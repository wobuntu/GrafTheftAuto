﻿using Core;
using Core.Classes;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace MapEditor
{
  /// <summary>
  /// Interaktionslogik für TileControl.xaml
  /// </summary>
  public partial class TileControl : UserControl
  {
    public TileControl(Tile tile)
    {
      InitializeComponent();

      this.tile = tile;
      this.tile.PropertyChanged += Tile_PropertyChanged;
      this.DataContext = tile;

      drawLines();
    }

    ~TileControl()
    {
      this.TileChanged = null;
    }

    private bool showWeights = false;
    public bool ShowWeights
    {
      get => (bool)GetValue(ShowWeightsProperty);
      set
      {
        showWeights = value;
        drawLines();
        SetValue(ShowWeightsProperty, value);
      }
    }


    #region Private variables along with some fixed brushes, sizes, etc.
    private Tile tile = null;

    private static readonly SolidColorBrush brushForward = MapColors.LaneForward;
    private static readonly SolidColorBrush brushBackward = MapColors.LaneBackward;
    private const double strokeThickness = 2;
    #endregion


    public event EventHandler TileChanged;


    #region Private helper methods
    private void Tile_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
      drawLines();
      TileChanged?.Invoke(this, null);
    }
    
    private void drawLines()
    {
      C.Children.Clear();
      var lanes = buildLines(tile.TileType, tile.NumLanes);

      foreach (var lane in lanes)
      {
        C.Children.Add(lane);
      }
    }

    private static SolidColorBrush getWeightBrush(double zeroToOne)
    {
      if (zeroToOne > 1)
      {
        zeroToOne = 1;
      }
      if (zeroToOne < 0)
      {
        zeroToOne = 0;
      }

      Color c = Color.FromRgb(
          (byte)(255 * (1 - zeroToOne)),
          (byte)(0),
          (byte)(255 * zeroToOne / 2));

      return new SolidColorBrush(c);
    }

    private IEnumerable<Line> buildLines(TileType type, int numLanes)
    {
      if (tile.LanesForward == null || tile.LanesBackward == null)
      {
        yield break;
      }

      Point offset = new Point(Config.UI.TileSize * tile.MapColumn, Config.UI.TileSize * tile.MapRow);
      foreach (var lane in tile.LanesBackward)
      {
        yield return new Line()
        {
          X1 = lane.Start.X - offset.X - Config.UI.TileBorderWidth,
          X2 = lane.End.X - offset.X - Config.UI.TileBorderWidth,
          Y1 = lane.Start.Y - offset.Y - Config.UI.TileBorderWidth,
          Y2 = lane.End.Y - offset.Y - Config.UI.TileBorderWidth,
          StrokeThickness = strokeThickness,
          Stroke = (showWeights ? getWeightBrush(1 - lane.Weight) : brushBackward)
        };
      }

      foreach (var lane in tile.LanesForward)
      {
        yield return new Line()
        {
          X1 = lane.Start.X - offset.X - Config.UI.TileBorderWidth,
          X2 = lane.End.X - offset.X - Config.UI.TileBorderWidth,
          Y1 = lane.Start.Y - offset.Y - Config.UI.TileBorderWidth,
          Y2 = lane.End.Y - offset.Y - Config.UI.TileBorderWidth,
          StrokeThickness = strokeThickness,
          Stroke = (showWeights ? getWeightBrush(1 - lane.Weight) : brushForward)
        };
      }
    }
    #endregion


    #region Dependency properties
    static readonly DependencyProperty ShowWeightsProperty
      = DependencyProperty.Register(
        nameof(ShowWeights),
        typeof(bool),
        typeof(TileControl),
        new PropertyMetadata() { DefaultValue = false });
    #endregion
  }
}
