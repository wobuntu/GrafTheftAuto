﻿using Core;
using Core.Classes;
using MapEditor.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;

namespace MapEditor
{
  /// <summary>
  /// Interaktionslogik für GridEditor.xaml
  /// </summary>
  public partial class GridEditor : Window, INotifyPropertyChanged
  {
    public GridEditor()
    {
      InitializeComponent();
      this.SaveMapCommand = new ActionCommand(doSave);
      this.LoadMapCommand = new ActionCommand(doLoad);
      this.SaveAndPublishMapCommand = new ActionCommand(doSaveAndPublish);
      this.ApplyNewDimensionsCommand = new ActionCommand((o)=>updateMapSize(MapCtrl.Map.GridHeight, MapCtrl.Map.GridWidth));

      var map = Map.TryDeserializeFrom("Map.json");
      if (map != null)
      {
        this.MapCtrl.Map = map;
      }
      else
      {
        this.MapCtrl.Map = new Map(4, 5);
      }

      this.mapHeightUi = this.MapCtrl.Map.GridHeight;
      this.mapWidthUi = this.MapCtrl.Map.GridWidth;

      this.DataContext = this;
    }

    #region Commands
    public ActionCommand LoadMapCommand { get; private set; }
    public ActionCommand SaveMapCommand { get; private set; }
    public ActionCommand SaveAndPublishMapCommand { get; private set; }
    public ActionCommand ApplyNewDimensionsCommand { get; private set; }

    public void doSave(object o)
    {
      // TODO: Uncomment if validity check works
      //if (!this.MapCtrl.Map.IsValid)
      //{
      //  MessageBox.Show("Cannot save the map, its connections are not valid.");
      //  return;
      //}
      if (Map.TrySerializeTo(this.MapCtrl.Map, "Map.json"))
      {
        MessageBox.Show("Map was saved.");
      }
      else
      {
        MessageBox.Show("Could not serialize the map.");
      }
    }

    public void doSaveAndPublish(object o)
    {
      SaveAndPublishDialog dialog = new SaveAndPublishDialog(this.MapCtrl.Map);
      dialog.ShowDialog();
    }

    public void doLoad(object o)
    {
      var map = Map.TryDeserializeFrom("Map.json");
      if (map != null)
      {
        this.MapCtrl.Map = map;
        MessageBox.Show("Map was loaded.");
        return;
      }

      MessageBox.Show("Could not load the map.");
    }
    #endregion


    void updateMapSize(int height, int width)
    {
      var result = MessageBox.Show("Reset grid? All data will be lost.", "Reset grid?", MessageBoxButton.YesNo);
      if (result == MessageBoxResult.Yes)
      {
        MapCtrl.Map = new Core.Classes.Map(MapHeightUi, MapWidthUi);
        this.MapHeightUi = MapCtrl.Map.GridHeight;
        this.MapWidthUi = MapCtrl.Map.GridWidth;
      }
    }


    #region Properties and their fields
    public bool ShowWeights
    {
      get => this.MapCtrl?.ShowWeights ?? false;
      set
      {
        if (this.MapCtrl != null)
        {
          this.MapCtrl.ShowWeights = value;
        }
        OnPropertyChanged();
      }
    }

    Tile selectedTile;
    public Tile SelectedTile
    {
      get => selectedTile;
      set
      {
        selectedTile = value;
        OnPropertyChanged(nameof(DebugMsg));
        OnPropertyChanged();
        OnPropertyChanged(nameof(TrafficLightIntervalId));
      }
    }

    public string DebugMsg
    {
      get
      {
        // Please leave this property, it will allow us to display data in the ui if problems occur
        //if (selectedTile == null)
        //{
        //  return "No tile selected";
        //}

        //string msg = $"lanes={selectedTile.NumLanes};in={selectedTile.LanesForward?.Count ?? 0};out={selectedTile.LanesBackward?.Count ?? 0}";
        //return msg;
        return "";
      }
    }

    public int MapWidth
    {
      get => MapCtrl.Map?.GridWidth ?? 0;
      set
      {
        if (value < 1)
        {
          value = 1;
        }
        else if (value > Config.UI.MaxTilesX)
        {
          value = Config.UI.MaxTilesX;
        }

        updateMapSize(MapCtrl.Map.GridHeight, value);
      }
    }

    private int mapHeightUi = 1;
    public int MapHeightUi
    {
      get => mapHeightUi;
      set
      {
        if (value > 15)
        {
          value = 15;
        }
        else if (value < 1)
        {
          value = 1;
        }
        mapHeightUi = value;
        OnPropertyChanged();
      }
    }

    private int mapWidthUi = 1;
    public int MapWidthUi
    {
      get => mapWidthUi;
      set
      {
        if (value > 15)
        {
          value = 15;
        }
        else if (value < 1)
        {
          value = 1;
        }
        mapWidthUi = value;
        OnPropertyChanged();
      }
    }

    public int MapHeight
    {
      get => MapCtrl.Map?.GridHeight ?? 0;
      set
      {
        if (value < 1)
        {
          value = 1;
        }
        else if (value > Config.UI.MaxTilesY)
        {
          value = Config.UI.MaxTilesY;
        }

        updateMapSize(MapCtrl.Map.GridHeight, value);
      }
    }

    string trafficLightIntervalId = "None";
    public string TrafficLightIntervalId
    {
      get
      {
        if (selectedTile == null
          || !selectedTile.TrafficLightEnabled
          || string.IsNullOrEmpty(trafficLightIntervalId))
        {
          return "None";
        }

        var realId = AllowedTrafficIntervalIds.IndexOf(trafficLightIntervalId);
        if (realId < 0)
        {
          return "None";
        }

        var light = selectedTile.TrafficLights.FirstOrDefault();
        if (light == null)
        {
          return "None";
        }

        return AllowedTrafficIntervalIds[light.IntervalId];
      }
      set
      {
        if (value == null || selectedTile == null)
        {
          return;
        }

        if (value == "None")
        {
          selectedTile.TrafficLightEnabled = false;
          OnPropertyChanged();
          return;
        }

        var realId = AllowedTrafficIntervalIds.IndexOf(value);
        if (realId < 0)
        {
          return;
        }

        selectedTile.TrafficLightEnabled = true;
        selectedTile.TrafficLightIntervalId = realId;

        trafficLightIntervalId = value;
        OnPropertyChanged();
      }
    }

    private IEnumerable<TileType> tileTypes = Enum.GetValues(typeof(TileType)).OfType<TileType>();

    public IEnumerable<TileType> TileTypes => tileTypes;

    public IEnumerable<double> AllowedRotations => new[] { 0.0, 90.0, 180.0, 270.0 };

    public IEnumerable<int> AllowedNumLanes => new[] { 0, 1, 2, 3 };

    public IList<string> AllowedTrafficIntervalIds => new List<string>() { "Group 1", "Group 2", "None" };
    #endregion


    #region Interface implementation
    public event PropertyChangedEventHandler PropertyChanged;

    protected void OnPropertyChanged([CallerMemberName]string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
    #endregion
  }
}
