﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace MapEditor
{
  public static class MapColors
  {
    public static SolidColorBrush NotConnected { get; set; } = Brushes.Red;
    public static SolidColorBrush ConnectedOne { get; set; } = Brushes.LightSeaGreen;
    public static SolidColorBrush ConnectedTwo { get; set; } = Brushes.SeaGreen;
    public static SolidColorBrush ConnectedMore { get; set; } = Brushes.DarkGreen;
    public static SolidColorBrush TrafficLightGroupOne { get; set; } = new SolidColorBrush(Color.FromArgb(100, 255, 128, 20));
    public static SolidColorBrush TrafficLightGroupTwo { get; set; } = new SolidColorBrush(Color.FromArgb(100, 128, 255, 20));
    public static SolidColorBrush TrafficLightGroupOther { get; set; } = Brushes.HotPink;
    public static SolidColorBrush SelectedTileBorder { get; set; } = Brushes.LightGoldenrodYellow;
    public static SolidColorBrush LaneForward { get; set; } = Brushes.LawnGreen;
    public static SolidColorBrush LaneBackward { get; set; } = Brushes.DarkMagenta;
  }
}
