﻿using Core;
using Core.Classes;
using Core.Interfaces;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace MapEditor
{
  /// <summary>
  /// Interaktionslogik für MapControl.xaml
  /// </summary>
  public partial class MapControl : UserControl
  {
    public MapControl()
    {
      InitializeComponent();
      C.PreviewMouseLeftButtonDown += C_MouseLeftButtonDown;
    }

    private const double strokeThickness = 3;
    private const double startIndicatorWidth = 12;
    private const double lightIndicatorWidth = 20;

    private TileControl lastSelectedTileCtrl = null;
    private void C_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      // Get x, y coordinates where the user clicked
      var position = e.GetPosition(C);

      // Finding the correct tile is easy, it can be calculated without an expensive hit test
      int left = (int)(position.X / Config.UI.TileSize);
      int top = (int)(position.Y / Config.UI.TileSize);

      this.SelectedItem = this.Map[top, left];

      // Adjust the color for displaying the item as selected
      var tileCtrl = C.Children[top * this.Map.GridWidth + left] as TileControl;
      if (lastSelectedTileCtrl != null)
      {
        lastSelectedTileCtrl.BorderBrush = Brushes.Transparent;
      }
      tileCtrl.BorderBrush = MapColors.SelectedTileBorder;
      lastSelectedTileCtrl = tileCtrl;
    }

    private void redrawConnections()
    {
      // Remove old connection and light indicators
      foreach (var toRemove in C.Children.OfType<Ellipse>().ToList())
      {
        C.Children.Remove(toRemove);
      }
      foreach (var toRemove in C.Children.OfType<Rectangle>().ToList())
      {
        C.Children.Remove(toRemove);
      }

      Map map = this.Map; // only require the cast in the getter once

      // Draw connection points on top of the tiles
      var allLanesBackward = map.Tiles.SelectMany(x => x).Where(x => x.LanesBackward != null).SelectMany(x => x.LanesBackward);
      var allLanesForward = map.Tiles.SelectMany(x => x).Where(x => x.LanesForward != null).SelectMany(x => x.LanesForward);
      foreach (var lane in allLanesBackward.Concat(allLanesForward))
      {
        var startIndicator = new Ellipse()
        {
          Fill = getIndicatorColor(lane),
          Width = startIndicatorWidth,
          Height = startIndicatorWidth
        };
        Canvas.SetLeft(startIndicator, lane.End.X - startIndicatorWidth / 2);
        Canvas.SetTop(startIndicator, lane.End.Y - startIndicatorWidth / 2);
        C.Children.Add(startIndicator);
      }

      // Draw Traffic Lights as rectangles
      foreach (var light in map.Tiles.SelectMany(x=>x).SelectMany(x=>x.TrafficLights))
      {
        foreach (var lightPosition in light.Positions)
        {
          var lightIndicator = new Rectangle()
          {
            Fill = Brushes.Transparent,
            Stroke = getLightGroupColor(light),
            Width = lightIndicatorWidth,
            Height = lightIndicatorWidth,
            StrokeThickness = strokeThickness,
          };

          Canvas.SetLeft(lightIndicator, lightPosition.X - lightIndicatorWidth / 2);
          Canvas.SetTop(lightIndicator, lightPosition.Y - lightIndicatorWidth / 2);
          C.Children.Add(lightIndicator);
        }
      }
    }

    private void redrawGrid()
    {
      Map map = this.Map; // only require the cast in the getter once
      C.Children.Clear();

      if (map == null)
      {
        return;
      }

      // Adjust the canvas width / height to the elements
      C.Width = Config.UI.TileSize * map.GridWidth;
      C.Height = Config.UI.TileSize * map.GridHeight;

      // Draw all tiles on the Canvas
      for (int h = 0; h < map.GridHeight; h++)
      {
        double offsetTop = h * Config.UI.TileSize;

        for (int w = 0; w < map.GridWidth; w++)
        {
          double offsetLeft = w * Config.UI.TileSize;

          var curTile = map[h, w] as Tile;
          TileControl ctrl = new TileControl(curTile)
          {
            Width = Config.UI.TileSize,
            Height = Config.UI.TileSize
          };
          ctrl.TileChanged += tileChangedHandler;

          Canvas.SetTop(ctrl, offsetTop);
          Canvas.SetLeft(ctrl, offsetLeft);

          C.Children.Add(ctrl);
        }
      }

      redrawConnections();
    }

    private void tileChangedHandler(object sender, System.EventArgs e)
    {
      redrawConnections();
    }

    private Brush getIndicatorColor(ILaneEx lane)
    {
      if (lane.NextLanes == null || lane.NextLanes.Count < 1)
        return MapColors.NotConnected;
      else if (lane.NextLanes.Count == 1)
        return MapColors.ConnectedOne;
      else if (lane.NextLanes.Count == 2)
        return MapColors.ConnectedTwo;
      else
        return MapColors.ConnectedMore;
    }

    private Brush getLightGroupColor(ITrafficLight light)
    {
      switch (light.IntervalId)
      {
        case 0: return MapColors.TrafficLightGroupOne;
        case 1: return MapColors.TrafficLightGroupTwo;
        default: return MapColors.TrafficLightGroupOther;
      }
    }

    #region Properties
    public Map Map
    {
      get => (Map)GetValue(MapProperty);
      set
      {
        SetValue(MapProperty, value);
        redrawGrid();
      }
    }

    public ITile SelectedItem
    {
      get => (ITile)GetValue(SelectedItemProperty);
      set
      {
        SetValue(SelectedItemProperty, value);
      }
    }

    private bool showWeights = false;
    public bool ShowWeights
    {
      get => showWeights;
      set
      {
        foreach (var tile in this.C.Children.OfType<TileControl>())
        {
          tile.ShowWeights = value;
        }
        this.showWeights = value;
      }
    }
    #endregion

    #region DependencyProperties
    private static readonly DependencyProperty MapProperty = DependencyProperty.Register(nameof(Map), typeof(Map), typeof(MapControl));
    private static readonly DependencyProperty SelectedItemProperty = DependencyProperty.Register(nameof(SelectedItem), typeof(Tile), typeof(MapControl));
    #endregion
  }
}
