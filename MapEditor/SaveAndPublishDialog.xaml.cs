﻿using Core.Classes;
using Core.ComponentDiscovery;
using Core.Interfaces;
using Core.Net.RemoteInvocation;
using Core.Net.Stubs;
using Core.Net.Websockets;
using MapEditor.Commands;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;

namespace MapEditor
{
  /// <summary>
  /// Interaktionslogik für SaveAndPublishDialog.xaml
  /// </summary>
  public partial class SaveAndPublishDialog : Window, INotifyPropertyChanged
  {
    public SaveAndPublishDialog(Map map)
    {
      InitializeComponent();
      this.LastMap = map;
      this.OkCommand = new ActionCommand(doSaveAndPublish);
      this.ReloadCommand = new ActionCommand(doReload);

      this.DataContext = this;

      this.doReload(null);
    }

    public Map LastMap { get; private set; }
    private IMapProviderService lastStub = null;
    private StubRemoteInvocationBehaviorProvider lastBehavior = null;

    List<IPAddress> foundServices;
    public List<IPAddress> FoundServices
    {
      get => foundServices;
      set
      {
        foundServices = value;
        OnPropertyChanged();
      }
    }

    private IPAddress selectedItem = null;
    public IPAddress SelectedItem
    {
      get => selectedItem;
      set
      {
        selectedItem = value;
        OnPropertyChanged();
      }
    }

    private string status = null;
    public string Status
    {
      get => status;
      set
      {
        status = value;
        OnPropertyChanged();
      }
    }

    public ActionCommand OkCommand { get; set; }
    public ActionCommand ReloadCommand { get; set; }
    
    void doSaveAndPublish(object o)
    {
      // Save the map locally
      try
      {
        bool saved = Map.TrySerializeTo(this.LastMap, "Map.json");
        if (!saved)
        {
          this.Status = "Failed to save the map locally. Aborting.";
        }

        this.Status = "Connecting...";
        Client client = new Client($"ws://{SelectedItem}:80/GrafTheftAuto/{nameof(IMapProviderService)}/");

        if (lastStub == null)
        {
          lastBehavior = new StubRemoteInvocationBehaviorProvider(client);
          lastStub = StubBuilder.CompileStubFor<IMapProviderService>(lastBehavior);
        }
        else
        {
          lastBehavior.TransportClient.TryDisconnect();
          lastBehavior.TransportClient = client;
        }

        this.Status = "Connected, sending request...";
        bool success = Task.Run(()=>lastStub.PushMap(LastMap)).Result;
        if (success)
        {
          this.Status = "Success, you can close this window now.";
        }
        else
        {
          this.Status = "Failed to transmit Map data.";
        }
      }
      catch
      {
        this.Status = "Failed to transmit Map data.";
      }
    }

    void doReload(object o)
    {
      Task.Run(() =>
      {
        this.Status = "Searching...";
        this.FoundServices = DiscoveryService.Find(
          nameof(IMapProviderService),
          Core.Config.NetworkDiscovery.Ports[nameof(IMapProviderService)],
          Core.Config.NetworkDiscovery.ReceiveTimeout).Select(x=>x.Address).ToList();

        this.SelectedItem = this.foundServices.FirstOrDefault();

        if (this.SelectedItem == null)
        {
          this.Status = "No service found.";
        }
        else
        {
          this.Status = $"Connect to the service by clicking OK.";
        }
      });
    }

    public event PropertyChangedEventHandler PropertyChanged;

    public void OnPropertyChanged([CallerMemberName]string propertyName = null)
    {
      this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
