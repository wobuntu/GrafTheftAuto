﻿using Core;
using System;
using System.Globalization;
using System.Windows.Data;

namespace MapEditor.Converters
{
  public class ImageRotationCenterConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return Config.UI.TileSize * 0.5 - Config.UI.TileBorderWidth;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
