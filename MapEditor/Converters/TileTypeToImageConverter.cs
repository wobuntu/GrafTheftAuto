﻿using Core.Classes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace MapEditor.Converters
{
  public class TileTypeToImageConverter : IMultiValueConverter
  {
    static TileTypeToImageConverter()
    {
      images = new Dictionary<(TileType, int), BitmapImage>();

      foreach (TileType entity in Enum.GetValues(typeof(TileType)))
      {
        try
        {
          if (entity == TileType.None)
          {
            string resourcePath = "pack://application:,,,/Pics/" + entity.ToString() + ".jpg";
            images.Add((entity, 0), new BitmapImage(new Uri(resourcePath)));
          }
          else
          {
            for (int i = 3; i >= 1; i--)
            {
              string resourcePath = "pack://application:,,,/Pics/" + entity.ToString() + "_" + i + ".png";
              images.Add((entity, i), new BitmapImage(new Uri(resourcePath)));
            }
          }
        }
        catch
        {
          // Do not import any picture
        }
      }
    }

    static Dictionary<(TileType, int), BitmapImage> images;


    public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
    {
      if (values.Length != 2)
      {
        return null;
      }

      BitmapImage img = null;
      if (values[0] is TileType type && values[1] is int numLanes)
      {
        if (images.ContainsKey((type, numLanes)))
        {
          img = images[(type, numLanes)];
        }
        else
        {
          img = null;
        }
      }

      return img;
    }

    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
    {
      return null;
    }
  }
}
