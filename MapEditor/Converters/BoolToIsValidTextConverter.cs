﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace MapEditor.Converters
{
  public class BoolToIsValidTextConverter : IMultiValueConverter
  {
    public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
    {
      if (values.Length != 2)
      {
        return "Unknown map status";
      }

      if (values[0] is bool b && b)
      {
        return "Map is valid.";
      }

      if (values[1] is Point p)
      {
        return $"Missing connections! See tile at row {p.Y} and column {p.X}.";
      }
      else
      {
        return "Map is invalid.";
      }
    }

    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
