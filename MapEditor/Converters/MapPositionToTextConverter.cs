﻿using Core.Classes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace MapEditor.Converters
{
  public class MapPositionToTextConverter : IMultiValueConverter
  {
    public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
    {
      if (values.Length == 3
        && values[0] is Tile tile
        && values[1] is int col
        && values[2] is int row)
      {
        return $"Row: {row}, Col: {col}";
      }

      return "-";
    }

    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
    {
      // Not required
      throw new NotImplementedException();
    }
  }
}
