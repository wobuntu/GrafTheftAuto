﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace MapEditor.Converters
{
  public class NoItemsConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is IEnumerable<object> infos && infos.Count() > 0)
      {
        if (targetType.IsAssignableFrom(typeof(bool)))
        {
          return true;
        }

        return infos;
      }
      else
      {
        if (targetType.IsAssignableFrom(typeof(bool)))
        {
          return false;
        }

        return new List<string>()
        {
          "None found"
        };
      }
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      // Not required
      throw new NotImplementedException();
    }
  }
}