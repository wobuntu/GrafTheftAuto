﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Interfaces;

namespace NavigationService
{
    internal static class LookupService
    {
        static List<ILaneEx> shortestRoute = new List<ILaneEx>();

        internal static IEnumerable<IEnumerable<ILaneEx>> Route(ILaneEx From, ILaneEx To)
        {

            List<List<ILaneEx>> resultingRoute = new List<List<ILaneEx>>();

            // Add startpoint
            List<ILaneEx> route = new List<ILaneEx>();
            route.Add(From);
            
            getRouteRecursive(From.NextLanes, To, route);

            foreach(var lane in shortestRoute)
            {
                // include the neighbour lanes to the shortest route
                resultingRoute.Add(addNeigbours(lane));
            }

            return resultingRoute;
        }

        private static List<ILaneEx> getRouteRecursive(IEnumerable<ILaneEx> lanes, ILaneEx To, List<ILaneEx> route)
        {
            var tmpSavedRouteBiggerScope = route;

            foreach (var lane in lanes)
            {
                if(lane.NextLanes != null && !route.Contains(lane))
                {
                    // just a add a elem to the current route if we are sure to continue
                    route.Add(lane);

                    if (lane.NextLanes.Contains(To))
                    {
                        // Add endpoint
                        route.Add(To);

                        if(shortestRoute.Count() == 0)
                        {
                            // store shortest route
                            shortestRoute = route;
                            // clear route
                            return new List<ILaneEx>();
                        }
                        if (shortestRoute.Count() >= route.Count())
                        {
                            // store shortest
                            shortestRoute = route;
                            // clear route
                            return new List<ILaneEx>();
                        }
                        else
                        {
                            // no shortest route found
                            return new List<ILaneEx>();
                        }
                    }
                    else
                    {
                        var tmpSavedRoute = route;
                        route = getRouteRecursive(lane.NextLanes, To, route);
                        return (route.Count() == 0) ? tmpSavedRoute : route;
                    }

                }
                else
                {
                    // at a dead end
                    // or a loop happend
                    // do nothing just leave 
                    // and start with another lane
                }
            }
            return tmpSavedRouteBiggerScope;
        }

        private static List<ILaneEx> addNeigbours(ILaneEx lane)
        {
            List<ILaneEx> list = new List<ILaneEx>();

            (list as List<ILaneEx>).Add(lane);

            if(lane.LeftLane != null)
            {
                list = checkLaneToTheLeft(lane, list);
            }

            if (lane.RightLane != null)
            {
                list = checkLaneToTheRight(lane, list);
            }

            return list;
        }

        private static List<ILaneEx> checkLaneToTheRight(ILaneEx laneToTheRight, List<ILaneEx> lanes)
        {

            (lanes as List<ILaneEx>).Add(laneToTheRight);

            if (laneToTheRight.RightLane != null)
            {
                lanes = checkLaneToTheRight(laneToTheRight.RightLane, lanes);
            }

            return lanes;
        }

        static List<ILaneEx> checkLaneToTheLeft(ILaneEx laneToTheLeft, List<ILaneEx> lanes)
        {
            (lanes as List<ILaneEx>).Add(laneToTheLeft);

            if (laneToTheLeft.LeftLane != null)
            {
                lanes = checkLaneToTheLeft(laneToTheLeft.RightLane, lanes);
            }

            return lanes;
        }
    }
}
