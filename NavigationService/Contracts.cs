﻿using Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NavigationService
{
    public static class Contracts
    {
        /// <summary>
        /// Resolves the fastes path from a lane to another lane.
        /// </summary>
        /// <param name="From">Starting lane.</param>
        /// <param name="To">Destination lane.</param>
        /// <returns>Returns a list of lists which hold lanes that lead to the
        /// destianation.</returns>
        public static IEnumerable<IEnumerable<ILaneEx>> FindRoute(ILaneEx From, ILaneEx To)
        {
            return LookupService.Route(From,To);
        }
    }
}
