﻿using Core;
using Core.Logging;
using Core.Net.RemoteInvocation;
using Core.Net.Websockets;
using System;
using System.Threading.Tasks;

namespace TesterReceiverOnly
{
    class Program
    {
        static void Main(string[] args)
        {
            // See the ClientSample project for a description of the logging stuff
            Feed.Subscribe(new ConsoleFeedPrinter(), LogLvl.All);
            Feed.Subscribe(new NLogFeedPrinter(true), LogLvl.All);

            // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            // SAMPLE FOR REGISTERING A COMPONENT IN THE NETWORK SO THAT IT CAN BE FOUND BY THE DISCOVERY SERVICE
            // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            // Attention: The returntype is of type IDisposable, meaning that it will unregister itself if the object is deleted,
            // eg. if it is out of scope. Store it in a static variable to avoid that!
            // However, this allows you to just register the component temporarely with:
            // using(DiscoveryRegistration.Create("ISampleInterface", 12345)
            // {
            //   .. Code ..
            // } <--- If this is reached, the component performs an auto unsubscribe, same if you call returnValue.Dispose()
            var discovery = Core.ComponentDiscovery.DiscoveryRegistration.Create(nameof(ISampleInterfaceFromCore), 12345);
            // From now on, the component can be found if others search for "ISampleInterface"


            // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            // SAMPLE FOR SETTING UP A COMPONENT SERVER AND REGISTERING FOR REMOTE INVOCATION
            // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            // Totally loose coupled, we could exchange the Websocket stuff here without problems (But I only implemented a websocket version)
            RemoteInvocationHandler rmiHandler = new RemoteInvocationHandler(new Server($"http://+:80/GrafTheftAuto/{nameof(ISampleInterfaceFromCore)}/"));
            rmiHandler.RegisterLocalTarget(nameof(ISampleInterfaceFromCore), new SampleComponent());

            while (true)
            {
                Task.Delay(100).Wait();
            }
        }

        public class SampleComponent : ISampleInterfaceFromCore
        {
            private int answer = 42;
            public int TestProperty
            {
                get => answer;
                set => answer = value;
            }

            public string TestMethod(string someTestString)
            {
                return $"My answer to '{someTestString}' is: {answer}.";
            }
        }
    }
}
