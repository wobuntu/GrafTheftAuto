﻿using System;
using Core;
using Core.Interfaces;
using Core.Logging;
using Core.Net.RemoteInvocation;
using Core.Net.Websockets;

namespace MapProviderService
{
  class MapProviderServiceStartup
  {
    static void Main(string[] args)
    {
      const string componentName = nameof(IMapProviderService);
      Console.Title = componentName;
      var component = new MapProviderService();
      UInt16 port = Core.Config.NetworkDiscovery.Ports[nameof(IMapProviderService)];
      string serverUri = $"http://+:80/GrafTheftAuto/{componentName}/";

      // Register for logging outputs
      Feed.Subscribe(new ConsoleFeedPrinter(), Config.FeedOptions.DefaultLogLvl);
      //Feed.Subscribe(new NLogFeedPrinter(true), LogLvl.All);

      Feed.Report($"{componentName} starting up...");

      try
      {
        // Register the component to be found in the network
        var discovery = Core.ComponentDiscovery.DiscoveryRegistration.Create(componentName, port);
        Feed.Report($"{componentName} was registered for network discovery on port {port}.");

        // Register the RMI handler, so that other foreign components can call methods from this component
        RemoteInvocationHandler rmiHandler = new RemoteInvocationHandler(new Server(serverUri));
        rmiHandler.RegisterLocalTarget(nameof(IMapProviderService), component);
        Feed.Report($"{componentName} was registered as an RMI server on {serverUri}");
      }
      catch (Exception ex)
      {
        Feed.Report($"An exception occurred: {ex}", LogLvl.Error);
      }

      Feed.Report($"Setup done. Press enter to quit.", LogLvl.InfoHighlighted);
      Console.ReadLine();
    }
  }
}