﻿using System.IO;

using Core.Classes;
using Core.Interfaces;
using Core.Logging;


namespace MapProviderService
{
  public class MapProviderService : IMapProviderService
  {
    const string path = "Map.json";
    public MapProviderService()
    {
      Map.TryDeserializeFrom(path);
    }

    public Map GetMap()
    {
      Feed.Report($"Map requested received ('{nameof(GetMap)}')");
      if (!File.Exists(path))
      {
        Feed.Report($"Could not find file '{path}', returning null.", LogLvl.Warn);
        return null;
      }

      Map map = Map.TryDeserializeFrom(path);
      if (map == null)
      {
        Feed.Report($"Could not deserialize map from file '{path}', returning null.", LogLvl.Error);
        return null;
      }

      return map;
    }

    public bool PushMap(Map map)
    {
      Feed.Report($"A new map was pushed ('{nameof(PushMap)}')");
      return Map.TrySerializeTo(map, path);
    }
  }
}
