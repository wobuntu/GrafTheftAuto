﻿using Core.AsyncUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Logging
{
  public class Feed
  {
    object _lock = new object();
    TaskScheduler entryThreadContext = MainThreadContext.Initialize();
    static Feed instance = new Feed();

    private Feed() { }

    /// <summary>
    /// This private variable holds the last percentage which was reported to the feed component.
    /// </summary>
    private static double lastPercentage = 0.0;

    /// <summary>The event which is responsible for firing on debug messages.</summary>
    private event FeedChanged OnFeedDebugMsg;
    /// <summary>The event which is responsible for firing on info messages.</summary>
    private event FeedChanged OnFeedInfoMsg;
    /// <summary>The event which is responsible for firing on info messages.</summary>
    private event FeedChanged OnFeedInfoHighlightedMsg;
    /// <summary>The event which is responsible for firing on warn messages.</summary>
    private event FeedChanged OnFeedWarnMsg;
    /// <summary>The event which is responsible for firing on error messages.</summary>
    private event FeedChanged OnFeedErrorMsg;
    /// <summary>The event which is responsible for firing on changing the progress.</summary>
    private event FeedChanged OnOnlyProgressChanged;


    /// <summary>
    /// Subscribes to the current <see cref="Feed"/> instance with the given event handler. It will only be invoked, if the
    /// reporting log level matches the given <paramref name="logLevel"/>. Note that the log level is a flagged value, which therefore
    /// allows to pass multiple log levels to subscribe to. Moreover, progress changes will be automatically reported to the given
    /// event handler, regardless of the <see cref="LogLvl"/>. To only register an event handler for progress changes, pass
    /// <see cref="LogLvl.None"/> to the parameter <paramref name="logLevel"/>.
    /// </summary>
    /// <param name="onFeedChanged">The event handler which is executed on <see cref="FeedChanged"/> events.</param>
    /// <param name="logLevel">The log level flags for which the event handler shall be invoked.</param>
    public static void Subscribe(FeedChanged onFeedChanged, LogLvl logLevel)
    {
      lock (instance._lock)
      {
        if ((logLevel & LogLvl.Debug) != 0)
          instance.OnFeedDebugMsg += onFeedChanged;
        if ((logLevel & LogLvl.Error) != 0)
          instance.OnFeedErrorMsg += onFeedChanged;
        if ((logLevel & LogLvl.Warn) != 0)
          instance.OnFeedWarnMsg += onFeedChanged;
        if ((logLevel & LogLvl.InfoHighlighted) != 0)
          instance.OnFeedInfoHighlightedMsg += onFeedChanged;
        if ((logLevel & LogLvl.Info) != 0)
          instance.OnFeedInfoMsg += onFeedChanged;

        instance.OnOnlyProgressChanged += onFeedChanged;
      }
    }

    public static void Subscribe(FeedPrinter printer, LogLvl logLevel)
    {
      lock (instance._lock)
      {
        if ((logLevel & LogLvl.Debug) != 0)
          instance.OnFeedDebugMsg += printer.PrintFeedChanged;
        if ((logLevel & LogLvl.Error) != 0)
          instance.OnFeedErrorMsg += printer.PrintFeedChanged;
        if ((logLevel & LogLvl.Warn) != 0)
          instance.OnFeedWarnMsg += printer.PrintFeedChanged;
        if ((logLevel & LogLvl.InfoHighlighted) != 0)
          instance.OnFeedInfoHighlightedMsg += printer.PrintFeedChanged;
        if ((logLevel & LogLvl.Info) != 0)
          instance.OnFeedInfoMsg += printer.PrintFeedChanged;

        instance.OnOnlyProgressChanged += printer.PrintFeedChanged;
      }
    }


    /// <summary>
    /// Removes the given <see cref="FeedChanged"/> event handler from the subscription list of the event. Note that it will be removed
    /// for all log levels.
    /// </summary>
    /// <param name="onFeedChanged">The event handler to remove.</param>
    public static void Unsubscribe(FeedChanged onFeedChanged)
    {
      lock (instance._lock)
      {
        if (instance.OnFeedDebugMsg != null)
          instance.OnFeedDebugMsg -= onFeedChanged;
        if (instance.OnFeedErrorMsg != null)
          instance.OnFeedErrorMsg -= onFeedChanged;
        if (instance.OnFeedWarnMsg != null)
          instance.OnFeedWarnMsg -= onFeedChanged;
        if (instance.OnFeedInfoHighlightedMsg != null)
          instance.OnFeedInfoHighlightedMsg -= onFeedChanged;
        if (instance.OnFeedInfoMsg != null)
          instance.OnFeedInfoMsg -= onFeedChanged;
        if (instance.OnOnlyProgressChanged != null)
          instance.OnOnlyProgressChanged -= onFeedChanged;
      }
    }


    public static void Unsubscribe(FeedPrinter printer)
    {
      lock (instance._lock)
      {
        if (instance.OnFeedDebugMsg != null)
          instance.OnFeedDebugMsg -= printer.PrintFeedChanged;
        if (instance.OnFeedErrorMsg != null)
          instance.OnFeedErrorMsg -= printer.PrintFeedChanged;
        if (instance.OnFeedWarnMsg != null)
          instance.OnFeedWarnMsg -= printer.PrintFeedChanged;
        if (instance.OnFeedInfoHighlightedMsg != null)
          instance.OnFeedInfoHighlightedMsg -= printer.PrintFeedChanged;
        if (instance.OnFeedInfoMsg != null)
          instance.OnFeedInfoMsg -= printer.PrintFeedChanged;
        if (instance.OnOnlyProgressChanged != null)
          instance.OnOnlyProgressChanged -= printer.PrintFeedChanged;
      }
    }


    /// <summary>
    /// This private method invokes all event handlers which are subscribed for the given <see cref="FeedChangedArgs"/>.LogLvl.
    /// </summary>
    /// <param name="args">The <see cref="FeedChangedArgs"/> which contains informations about the event.</param>
    private void report(string callerMemberName, FeedChangedArgs args)
    {
      //Task.Factory.StartNew(() =>
      //{
      //  lock (_lock)
      //  {
          Delegate reportTo = null;

          if (args.LogLvl.HasFlag(LogLvl.Debug))
            reportTo = this.OnFeedDebugMsg;
          if (args.LogLvl.HasFlag(LogLvl.Error))
            reportTo = this.OnFeedErrorMsg;
          if (args.LogLvl.HasFlag(LogLvl.Warn))
            reportTo = this.OnFeedWarnMsg;
          if (args.LogLvl.HasFlag(LogLvl.InfoHighlighted))
            reportTo = this.OnFeedInfoHighlightedMsg;
          if (args.LogLvl.HasFlag(LogLvl.Info))
            reportTo = this.OnFeedInfoMsg;
          if (args.LogLvl == LogLvl.None)
            reportTo = this.OnOnlyProgressChanged;

          reportTo?.DynamicInvoke(new object[] { callerMemberName, args });
      //  }
      //}, CancellationToken.None, TaskCreationOptions.None, MainThreadContext.Current);
    }


    /// <summary>
    /// Reports a percentage change and invokes the subscribed event handlers. Note that all methods registered via
    /// <see cref="Subscribe(FeedChanged, LogLvl)"/> will be invoked, independently from the <see cref="LogLvl"/>.
    /// </summary>
    /// <param name="progressPercentage">The progress percentage to report.</param>
    /// <param name="threshold">The threshold for the percentage change. If the given percentage added to it is smaller than
    /// the last reported percentage, the method is left.</param>
    public static void Report(double progressPercentage, double threshold = 0.01, [CallerMemberName] string sender = null)
    {
      progressPercentage = Math.Round(progressPercentage, 2);
      if (lastPercentage <= progressPercentage + threshold)
        return; // No visible change for the user, only report if the progress changed by 0.1

      lastPercentage = progressPercentage;

      instance.report(sender, new FeedChangedArgs(progressPercentage, LogLvl.None));
    }


    /// <summary>
    /// Reports a message along with a progress change to the registered event handlers matching the given <see cref="LogLvl"/>.
    /// </summary>
    /// <param name="message">The message to report.</param>
    /// <param name="progressPercentage">The percentage change to report.</param>
    /// <param name="logLevel">The <see cref="LogLvl"/>.</param>
    public static void Report(String message, double progressPercentage, LogLvl logLevel = LogLvl.Info, [CallerMemberName] string sender = null)
    {
      progressPercentage = Math.Round(progressPercentage, 2);
      lastPercentage = progressPercentage;

      instance.report(sender, new FeedChangedArgs(message, progressPercentage, logLevel));
    }


    /// <summary>
    /// Reports a message to the registered event handlers matching the given <see cref="LogLvl"/>.
    /// </summary>
    /// <param name="message">The message to report.</param>
    /// <param name="logLevel">The <see cref="LogLvl"/>.</param>
    public static void Report(String message, LogLvl logLevel = LogLvl.Info, [CallerMemberName] string sender = null)
        => instance.report(sender, new FeedChangedArgs(message, logLevel));


    /// <summary>
    /// Reports a message along with an exception to the registered event handlers matching the given <see cref="LogLvl"/>.
    /// </summary>
    /// <param name="message">The message to report.</param>
    /// <param name="exception">The exception object to pass within the <see cref="FeedChangedArgs"/>.</param>
    /// <param name="logLevel">The <see cref="LogLvl"/>.</param>
    public static void Report(String message, Exception exception, LogLvl logLevel = LogLvl.Error, [CallerMemberName] string sender = null)
        => instance.report(sender, new FeedChangedArgs(message, logLevel, exception));


    /// <summary>
    /// Reports an exception to the registered event handlers matching the given <see cref="LogLvl"/>.
    /// </summary>
    /// <param name="exception">The exception object to pass within the <see cref="FeedChangedArgs"/>.</param>
    /// <param name="logLevel">The <see cref="LogLvl"/>.</param>
    public static void Report(Exception exception, LogLvl logLevel = LogLvl.Error, [CallerMemberName] string sender = null)
        => instance.report(sender, new FeedChangedArgs("An error occured.", logLevel, exception));


    /// <summary>
    /// The delegate which describes the method signature for event handlers compatible with the <see cref="Feed"/>.
    /// </summary>
    /// <param name="sender">The sender (or its name) which caused the invocation of the event handlers.</param>
    /// <param name="args">The arguments of the invocation, which holds data about it.</param>
    public delegate void FeedChanged(object sender, FeedChangedArgs args);
  }
}
