﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Logging
{
    /// <summary>This class is used to hold information about <see cref="FeedChanged"/> events.</summary>
    public class FeedChangedArgs : EventArgs
    {
        /// <summary>This value is not null if a progress percentage change was reported.</summary>
        public double? Progress { get; private set; } = null;
        /// <summary>This value is not null if a message was reported.</summary>
        public string Message { get; private set; } = null;
        /// <summary>This value specifies the <see cref="LogLvl"/> of the reported values.</summary>
        public LogLvl LogLvl { get; private set; } = LogLvl.Info;
        /// <summary>This value is not null if an exception object was reported.</summary>
        public Exception Exception { get; private set; } = null;

        /// <summary>Creates a new instance of <see cref="FeedChangedArgs"/>.</summary>
        /// <param name="message">A message to be included in the report.</param>
        /// <param name="progressPercentage">A progress percentage change to be included in the report.</param>
        /// <param name="logLevel">The <see cref="LogLvl"/> of the report.</param>
        public FeedChangedArgs(String message, double progressPercentage, LogLvl logLevel)
        {
            this.Message = message;
            this.Progress = progressPercentage;
            this.LogLvl = logLevel;
        }

        /// <summary>Creates a new instance of <see cref="FeedChangedArgs"/>.</summary>
        /// <param name="message">A message to be included in the report.</param>
        /// <param name="logLevel">The <see cref="LogLvl"/> of the report.</param>
        /// <param name="exception">An optional exception to be included in the report.</param>
        public FeedChangedArgs(String message, LogLvl logLevel, Exception exception = null)
        {
            this.Message = message;
            this.Exception = exception;
            this.LogLvl = logLevel;
        }


        /// <summary>Creates a new instance of <see cref="FeedChangedArgs"/>.</summary>
        /// <param name="progress">A progress change to be included in the report.</param>
        /// <param name="logLevel">The <see cref="LogLvl"/> of the report.</param>
        public FeedChangedArgs(double progress, LogLvl logLevel)
        {
            this.Progress = progress;
            this.LogLvl = logLevel;
        }
    }
}
