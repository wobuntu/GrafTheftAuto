﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Logging
{
    public abstract class FeedPrinter
    {
        public abstract void PrintFeedChanged(object sender, FeedChangedArgs args);
    }
}
