﻿using Core.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Logging
{
    public sealed class ConsoleFeedPrinter : FeedPrinter
    {
        public ConsoleFeedPrinter(bool printSender = false)
        {
            PrintSender = printSender;
        }

        public bool PrintSender { get; set; }

        public override void PrintFeedChanged(object sender, FeedChangedArgs args)
        {
            if (args.Message == null && args.Exception == null)
                return;

            ConsoleColor fg = Console.ForegroundColor;
            ConsoleColor bg = Console.BackgroundColor;
            string paragraphBegin = "    | ";
            string indentationStr = "    | ";

            // Use the highest LogLvl for the output
            StringBuilder sb = new StringBuilder();
            if ((args.LogLvl & LogLvl.Error) != 0)
            {
                paragraphBegin = "ERR | ";
                fg = ConsoleColor.White;
                bg = ConsoleColor.DarkRed;
            }
            else if ((args.LogLvl & LogLvl.Warn) != 0)
            {
                paragraphBegin = "WRN | ";
                fg = ConsoleColor.Yellow;
            }
            else if ((args.LogLvl & LogLvl.Debug) != 0)
            {
                paragraphBegin = "DBG | ";
                fg = ConsoleColor.White;
            }
            else if ((args.LogLvl & LogLvl.Info) != 0)
            {
                paragraphBegin = "NFO | ";
                fg = ConsoleColor.Gray;
            }
            else if ((args.LogLvl & LogLvl.InfoHighlighted) != 0)
            {
                paragraphBegin = "NFO | ";
                fg = ConsoleColor.Green;
            }
            else if (args.LogLvl == LogLvl.None)
            {
                // For progress changes, no output to nlog
                fg = ConsoleColor.DarkGray;
            }

            if (PrintSender)
            {
                sb.Append(sender);
                sb.Append(": ");
            }
            if (args.Message != null)
            {
                sb.Append(args.Message);
                sb.Append(Environment.NewLine);
            }
            if (args.Progress != null)
            {
                sb.Append(Math.Round(args.Progress.Value, 2));
                sb.Append(Environment.NewLine);
            }
            if (args.Exception != null)
            {
                sb.Append(Environment.NewLine);
                sb.Append("Exception: ");
                sb.Append(args.Exception);
                sb.Append(Environment.NewLine);
            }

            Windows.WriteConsoleIndented(fg, bg, sb.ToString(), paragraphBegin, indentationStr);
        }
    }
}
