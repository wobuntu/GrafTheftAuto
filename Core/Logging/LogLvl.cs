﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Logging
{
    /// <summary>A flagged enum value, which is used together with the <see cref="Feed"/> for listening to log messages.</summary>
    [Flags]
    public enum LogLvl
    {
        /// <summary>Register only for progress changes.</summary>
        None = 0,
        /// <summary>Register for error messages and progress changes.</summary>
        Error = 1,
        /// <summary>Register for warn messages and progress changes.</summary>
        Warn = 2,
        /// <summary>Register for info messages and progress changes.</summary>
        Info = 4,
        /// <summary>Register for debug messages and progress changes.</summary>
        Debug = 8,
        /// <summary>Register for debug messages and progress changes.</summary>
        InfoHighlighted = 16,
        /// <summary>Register for all message types.</summary>
        All = 31
    }
}
