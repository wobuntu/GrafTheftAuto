﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Logging
{
    public sealed class NLogFeedPrinter : FeedPrinter
    {
        public NLogFeedPrinter(bool printSender = false)
        {
            PrintSender = printSender;
        }

        public bool PrintSender { get; set; }

        public override void PrintFeedChanged(object sender, FeedChangedArgs args)
        {
            if (args.Message == null && args.Exception == null)
                return;

            var logger = NLog.LogManager.GetCurrentClassLogger();
            NLog.LogLevel nlogLvl = NLog.LogLevel.Off;

            // Use the highest LogLvl for the output
            StringBuilder sb = new StringBuilder();
            if ((args.LogLvl & LogLvl.Error) != 0)
            {
                nlogLvl = NLog.LogLevel.Error;
            }
            else if ((args.LogLvl & LogLvl.Warn) != 0)
            {
                nlogLvl = NLog.LogLevel.Warn;
            }
            else if ((args.LogLvl & LogLvl.Debug) != 0)
            {
                nlogLvl = NLog.LogLevel.Debug;
            }
            else if ((args.LogLvl & LogLvl.Info) != 0)
            {
                nlogLvl = NLog.LogLevel.Info;
            }
            else if ((args.LogLvl & LogLvl.InfoHighlighted) != 0)
            {
                nlogLvl = NLog.LogLevel.Info;
            }
            else if (args.LogLvl == LogLvl.None)
            {
                // Nothing to report
                return;
            }

            if (PrintSender)
            {
                sb.Append(sender);
                sb.Append(": ");
            }
            if (args.Message != null)
            {
                sb.Append(args.Message);
                sb.Append(Environment.NewLine);
            }
            if (args.Progress != null)
            {
                sb.Append(Math.Round(args.Progress.Value, 2));
                sb.Append(Environment.NewLine);
            }
            if (args.Exception != null)
            {
                sb.Append(Environment.NewLine);
                sb.Append("Exception: ");
                sb.Append(args.Exception);
                sb.Append(Environment.NewLine);
            }

            logger.Log(nlogLvl, args.Exception, args.Message);
        }
    }
}
