﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Extensions
{
  public static class ListExtensions
  {
    public static bool AddIfNotContained<T>(this IList<T> list, T item)
    {
      if (list.Contains(item))
      {
        if (list.Count > 5)
        {
          // TODO REMOVE ME, was for debugging breakpoint
        }
        return false;
      }

      list.Add(item);
      return true;
    }
  }
}
