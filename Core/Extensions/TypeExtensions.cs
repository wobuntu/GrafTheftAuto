﻿using System;
using System.Linq;

namespace Core.Extensions
{
    public static class TypeExtensions
    {
        public static string GetFriendlyTypeName(this Type type, bool addNamespace = true, bool showGenericParams = true)
        {
            if (type.IsGenericParameter)
            {
                return type.Name;
            }

            string friendlyName = (addNamespace ? type.FullName : type.Name);

            if (type == null)
            {
                return "null";
            }

            if (type == typeof(void))
            {
                return "void";
            }

            if (type.IsGenericType)
            {
                int index = friendlyName.IndexOf('`');
                if (index > 0)
                {
                    friendlyName = friendlyName.Remove(index);
                }
                if (showGenericParams)
                {
                    var typeParams = type.GetGenericArguments().Select((x) => x.GetFriendlyTypeName(addNamespace, showGenericParams));
                    friendlyName += $"<{string.Join(",", typeParams)}>";
                }
            }

            return friendlyName;
        }

        public static object GetDefaultValue(this Type type)
        {
            if (type == typeof(void))
            {
                return null;
            }

            if (type.IsValueType)
            {
                return Activator.CreateInstance(type);
            }

            return null;
        }
    }
}
