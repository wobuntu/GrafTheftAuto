﻿using System;
using System.Text;

namespace Core.Extensions
{
    public static class ByteExtensions
    {
        public static byte[] Part(this byte[] source, int startIndex, int lg)
        {
            byte[] result = new byte[lg];
            for (int i = 0; i < lg; i++)
            {
                result[i] = source[startIndex + i];
            }

            return result;
        }

        /// <summary>
        /// Reads the given amount of bits (max. 32 bit) from an array of bytes and returns an integer.
        /// Throws an ArgumentException if 'lengthInBit' is greater than 32.
        /// </summary>
        /// <param name="src"></param>
        /// <param name="startBitIndex">First bit index to read.</param>
        /// <param name="lengthInBit">Length in bits to read.</param>
        public static int ReadInt(this byte[] src, int startBitIndex, int lengthInBit)
        {
            if (lengthInBit > 32)
                throw new ArgumentException("Argument 'lengthInBit' must not be larger than 32.");

            int firstByteIndex, lastByteIndex;
            int val = 0;

            int curBitIndex = 0;
            int curBitOffset = 0;
            int bitRead = 0;

            int curBit = 0;

            int endBitIndex = startBitIndex + lengthInBit - 1;

            // get first and last byte to read from
            firstByteIndex = startBitIndex / 8;
            lastByteIndex = endBitIndex / 8;

            if (endBitIndex > src.Length * 8 - 1)
                throw new IndexOutOfRangeException("Length in Bit is too large.");

            // get index of first bit to read
            curBitOffset = startBitIndex - 8 * firstByteIndex;

            // read through all desired bytes
            for (int i = firstByteIndex; i <= lastByteIndex; i++)
            {
                // read all desired bits
                for (int j = curBitOffset; j < 8; j++)
                {
                    // current byte * 8 + current bit offset
                    curBitIndex = i * 8 + j;

                    if (curBitIndex > endBitIndex)
                        break;

                    curBit = src[i] & (1 << (7 - j));
                    if (curBit != 0)
                        curBit = 1;

                    val <<= 1;
                    val |= curBit;

                    bitRead++;
                }
                curBitOffset = 0;
            }

            return val;
        }

        public static int ReadInt(this byte[] src, ref int startBitIndex, int lengthInBit)
        {
            int result = src.ReadInt(startBitIndex, lengthInBit);
            startBitIndex += lengthInBit;
            return result;
        }


        /// <summary>
        /// Reads the given amount of bits (max. 32 bits) from an array of bytes and returns an unsigned integer.
        /// Throws an ArgumentException if 'lengthInBit' is greater than 32.
        /// </summary>
        /// <param name="src"></param>
        /// <param name="startBitIndex">First bit index to read.</param>
        /// <param name="lengthInBit">Length in bits to read.</param>
        public static UInt32 ReadUInt(this byte[] src, int startBitIndex, int lengthInBit)
        {
            int val = src.ReadInt(startBitIndex, lengthInBit);

            // first byte tells if the value is negativ, but for uint it stores if 2^31 is set
            if (val >= 0)
                // if the value is positive, just cast and return
                return (UInt32)val;
            else
            {
                // extract 31 oof 32 bit and add 2^31 to get the actual UInt32 value
                UInt32 uval = (UInt32)(val & 0x7FFFFFFF);   // 0x7FFFFFFF = 0b01111111111111111111111111111111
                return uval + (UInt32)0x80000000;           // 0x80000000 = 0b10000000000000000000000000000000 = 2^31
            }
        }


        public static UInt32 ReadUInt(this byte[] src, ref int startBitIndex, int lengthInBit)
        {
            UInt32 result = src.ReadUInt(startBitIndex, lengthInBit);
            startBitIndex += lengthInBit;
            return result;
        }

        /// <summary>
        /// Writes the given value into a byte array beginning at a given starting bit.
        /// No exception is thrown if the given value is too big for the array.
        /// If 'bitToWriteFromValue' is greater than 32, an ArgumentException is thrown.
        /// </summary>
        /// <param name="src"></param>
        /// <param name="startBitIndex">First bit position to write to.</param>
        /// <param name="value">Value which shall be written binary into the array.</param>
        /// <param name="bitToWriteFromValue">Number of bit to write from the 'value'. Note: Counting starts from the back!</param>
        public static void WriteInt(this byte[] src, int startBitIndex, int value, ushort bitToWriteFromValue)
        {
            if (bitToWriteFromValue == 0)
                throw new ArgumentException("Number of bit to write must not be 0.");

            if (bitToWriteFromValue > 32)
                throw new ArgumentException("Argument 'bitToWriteFromValue' must not be greater than 32.");

            int firstByteIndex, lastByteIndex, lastBitIndex, initialBitOffset, curBitIndex;

            int bitWritten = 0;

            int curBit = 0;

            // get first and last bit/byte to read from
            lastBitIndex = startBitIndex + bitToWriteFromValue - 1;
            firstByteIndex = startBitIndex / 8;
            lastByteIndex = lastBitIndex / 8;

            // get current position in current byte
            initialBitOffset = lastBitIndex - 8 * lastByteIndex;

            // get through the appropriate bits and write bit from 'value'
            for (int i = lastByteIndex; i >= firstByteIndex; i--)
            {
                for (int j = initialBitOffset; j >= 0; j--)
                {
                    // current byte index * 8 + current bit offset
                    curBitIndex = i * 8 + j;
                    if (curBitIndex < startBitIndex)
                        break;

                    curBit = value & (1 << bitWritten);
                    if (curBit != 0)
                        curBit = 1;

                    // Clear current bit in source (looks fancy, right?)
                    src[i] &= (byte)(~(1 << (7 - j)));

                    // Set current bit in source
                    src[i] |= (byte)(curBit << (7 - j));
                    bitWritten++;
                }
                initialBitOffset = 7;
            }

            // get index of first bit to read
            initialBitOffset = startBitIndex - 8 * firstByteIndex;
        }


        public static void WriteInt(this byte[] src, ref int startBitIndex, int value, ushort bitToWriteFromValue)
        {
            WriteInt(src, startBitIndex, value, bitToWriteFromValue);
            startBitIndex += bitToWriteFromValue;
        }

        public static void WriteString(this byte[] src, ref int startBitIndex, string value, ushort totalCharsToWrite)
        {
            if (value.Length > totalCharsToWrite)
                value = value.Substring(0, totalCharsToWrite);

            byte[] raw = Encoding.Unicode.GetBytes(value);
            for (int i = 0; i < raw.Length; i++)
                src.WriteInt(ref startBitIndex, raw[i], 8);

            // Fill the padding too (1 char = 2 byte)
            for (int i = 0; i < totalCharsToWrite * 2 - raw.Length; i++)
                src.WriteInt(ref startBitIndex, 0, 8);
        }

        public static string ReadString(this byte[] src, ref int startBitIndex, ushort maxBytesToRead)
        {
            byte[] result = new byte[maxBytesToRead];

            for (int i = 0; i < maxBytesToRead; i++)
                result[i] = (byte)src.ReadInt(ref startBitIndex, 8);

            return System.Text.Encoding.Unicode.GetString(result, 0, result.Length).TrimEnd('\0');
        }

        public static void WriteBytes(this byte[] src, ref int startBitIndex, byte[] value, ushort valueByteOffset, uint valueMaxBytesToWrite)
        {
            for (ushort i = valueByteOffset; i < Math.Min(value.Length, valueByteOffset + valueMaxBytesToWrite); i++)
            {
                src.WriteInt(ref startBitIndex, value[i], 8);
            }
        }

        public static void WriteBytes(this byte[] src, ref int startBitIndex, byte[] value)
        {
            src.WriteBytes(ref startBitIndex, value, 0, (uint)value.Length);
        }


        public static byte[] ReadBytes(this byte[] src, ref int startBitIndex, ushort bytesToRead)
        {
            byte[] result = new byte[bytesToRead];
            for (int i = 0; i < bytesToRead; i++)
                result[i] = (byte)src.ReadInt(ref startBitIndex, 8);

            return result;
        }

        /// <summary>
        /// Writes the given value into a byte array beginning at a given starting bit.
        /// No exception is thrown if the given value is too big for the array.
        /// If 'bitToWriteFromValue' is greater than 32, an ArgumentException is thrown.
        /// </summary>
        /// <param name="src"></param>
        /// <param name="startBitIndex">First bit position to write to.</param>
        /// <param name="value">Value which shall be written binary into the array.</param>
        /// <param name="bitToWriteFromValue">Number of bit to write from the 'value'. Note: Counting starts from the back!</param>
        public static void WriteUInt(this byte[] src, int startBitIndex, uint value, ushort bitToWriteFromValue)
        {
            // convert the value and use WriteInt method:
            if (value <= Int32.MaxValue)
            {
                // if the first bit isn't set, the value is positive in Int32 representation, just cast and use WriteInt:
                src.WriteInt(startBitIndex, (Int32)value, bitToWriteFromValue);
            }
            else
            {
                // the value is negative than, extract 31 of 32 bit, cast the now smaller value to int and add the bit again by subtracting (Int32.MaxValue - 1)
                int sval = (int)(value & 0x7FFFFFFF);   // 0x7FFFFFFF = 0b01111111111111111111111111111111
                src.WriteInt(startBitIndex, (sval - 0x7FFFFFFF) - 1, bitToWriteFromValue);
            }
        }

        public static void WriteUInt(this byte[] src, ref int startBitIndex, uint value, ushort bitToWriteFromValue)
        {
            WriteUInt(src, startBitIndex, value, bitToWriteFromValue);
            startBitIndex += bitToWriteFromValue;
        }

        /// <summary>
        /// Copies all values from the array to 'destination', while 'startindex' specifies the first index in the
        /// source array to copy from and 'amount' specifies the amount of bytes to copy.
        /// </summary>
        /// <param name="src"></param>
        /// <param name="destination">Array which shall be filled with data from the current array.</param>
        /// <param name="startindex">First index in the source array to be copied into the destination array.</param>
        /// <param name="amount">Amount of bytes to copy, beginning at 'startindex'.</param>
        public static void CopyTo(this byte[] src, byte[] destination, int startindex, int amount)
        {
            for (int i = 0; i < amount; i++)
                destination[i] = src[startindex + i];
        }


        /// <summary>
        /// Unfortunately, the IPAddress class is not accessible from a PCL. Therefore a conversion from a byte array to an IP-string
        /// is utilized with this extension method.
        /// </summary>
        /// <param name="ip">The byte array with exactly 4 bytes.</param>
        public static string ToIpString(this byte[] ip)
        {
            if (ip.Length != 4)
                throw new ArgumentException("A byte array must have an exact length of 4 to convert it to an IP-address.");

            return $"{ip[3]}.{ip[2]}.{ip[1]}.{ip[0]}";
        }
    }
}
