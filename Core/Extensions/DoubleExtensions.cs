﻿using System;

namespace Core.Extensions
{
  public static class DoubleExtensions
  {
    public static int RoundToStep(this double value, int stepsize = 1)
    {
      return (int)Math.Round(value / stepsize) * stepsize;
    }

    public static int FloorToStep(this double value, int stepsize = 1)
    {
      return (int)Math.Floor(value / stepsize) * stepsize;
    }

    public static int CeilingToStep(this double value, int stepsize = 1)
    {
      return (int)Math.Ceiling(value / stepsize) * stepsize;
    }
  }
}
