﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Extensions
{
    public static class StringExtensions
    {
        public static IEnumerable<string> SplitToLines(this String str, int maxLineLength)
        {
            int start = 0;
            int end = -Environment.NewLine.Length;
            int lg;
            while (end < str.Length)
            {
                start = end + Environment.NewLine.Length;
                end = str.IndexOf(Environment.NewLine, start);
                if (end == -1)
                    end = str.Length;

                if (start > end)
                    yield break;

                lg = end - start;
                while (lg > maxLineLength)
                {
                    string subLine = str.Substring(start, Math.Min(maxLineLength, lg));
                    start += subLine.Length;
                    lg -= subLine.Length;

                    yield return subLine;
                }

                if (lg == 0 && end == str.Length)
                    yield break;

                yield return str.Substring(start, lg);
            }
        }
    }
}
