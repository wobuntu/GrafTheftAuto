﻿using Core.Extensions;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Core.Utils
{
  public static class Windows
  {
    static object consoleLock = new object();

    public static void WriteConsoleIndented(ConsoleColor foreground, ConsoleColor background,
        String message, String paragraphBegin, String indentationStr)
    {
      if (string.IsNullOrWhiteSpace(message))
        return;

      lock (consoleLock)
      {
        // The lock is used to ensure the colors don't get mixed up
        ConsoleColor oldFg = Console.ForegroundColor;
        ConsoleColor oldBg = Console.BackgroundColor;

        Console.ForegroundColor = foreground;
        Console.BackgroundColor = background;

        WriteIndented(message, paragraphBegin, indentationStr);

        Console.ForegroundColor = oldFg;
        Console.BackgroundColor = oldBg;
      }
    }

    public static void WriteIndented(String message, String paragraphBegin, String indentationStr)
    {
      bool firstLinePrinted = false;
      int maxLineLength = Console.WindowWidth - 1 - Math.Max(paragraphBegin.Length, indentationStr.Length);

      foreach (var line in message.SplitToLines(maxLineLength))
      {
        if (!firstLinePrinted)
        {
          Console.Write(paragraphBegin);
          firstLinePrinted = true;
        }
        else
        {
          Console.Write(indentationStr);
        }

        Console.WriteLine(line);
      }
    }

    #region Flashing of windows in taskbar
    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    static extern bool FlashWindowEx(ref FLASHWINFO pwfi);

    const UInt32 FLASHW_ALL = 3;

    [StructLayout(LayoutKind.Sequential)]
    struct FLASHWINFO
    {
      public UInt32 cbSize;
      public IntPtr hwnd;
      public UInt32 dwFlags;
      public UInt32 uCount;
      public Int32 dwTimeout;
    }

    public static void FlashWindow(IntPtr hWnd)
    {
      FLASHWINFO fInfo = new FLASHWINFO();

      fInfo.cbSize = Convert.ToUInt32(Marshal.SizeOf(fInfo));
      fInfo.hwnd = hWnd;
      fInfo.dwFlags = FLASHW_ALL;
      fInfo.uCount = UInt32.MaxValue;
      fInfo.dwTimeout = 0;

      FlashWindowEx(ref fInfo);
    }

    public static void FlashWindow()
    {
      FlashWindow(Process.GetCurrentProcess().MainWindowHandle);
    }
    #endregion


    #region Find window
    public static void BringProcessToFront(Process process)
    {
      IntPtr handle = process.MainWindowHandle;
      if (IsIconic(handle))
      {
        ShowWindow(handle, SW_RESTORE);
      }

      SetForegroundWindow(handle);
    }

    const int SW_RESTORE = 9;

    [System.Runtime.InteropServices.DllImport("User32.dll")]
    private static extern bool SetForegroundWindow(IntPtr handle);
    [System.Runtime.InteropServices.DllImport("User32.dll")]
    private static extern bool ShowWindow(IntPtr handle, int nCmdShow);
    [System.Runtime.InteropServices.DllImport("User32.dll")]
    private static extern bool IsIconic(IntPtr handle);
    #endregion
  }
}