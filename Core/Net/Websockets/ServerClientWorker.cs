﻿using System;
using System.Net;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Core.Logging;

namespace Core.Net.Websockets
{
  internal class WebSocketClientWorker
  {
    public WebSocketClientWorker(HttpListenerContext context, Server serverReference)
    {
      this.serverReference = serverReference;
      Task.Run(() => processMessages(context));
    }

    public HttpListenerWebSocketContext Context { get; private set; } = null;

    Server serverReference;
    const int wsBufferSize = 65536; // = max allowed buffersize
    byte[] buffer = new byte[wsBufferSize];

    public async void processMessages(HttpListenerContext httpContext)
    {
      // This method is called solely from the WebGateway class, httpContext will never be null
      IPEndPoint ep = httpContext.Request.RemoteEndPoint;

      try
      {
        Context = await httpContext.AcceptWebSocketAsync(
            null, // don't require a nested protocol
            wsBufferSize, // default buffer size in byte for data to be sent at once
            TimeSpan.FromMilliseconds(1000));


      }
      catch (Exception ex)
      {
        Feed.Report($"Client {ep.Address}:{ep.Port} was refused, see the exception for details: ", ex);
        httpContext.Response.StatusCode = 403;  // "Forbidden"
        httpContext.Response.Close();

        return;
      }

      Feed.Report($"Client {ep.Address}:{ep.Port} was accepted.", LogLvl.InfoHighlighted);
      WebSocketReceiveResult result = null;
      CancellationToken tokenNone = CancellationToken.None;
      StringBuilder sb = null;

      try
      {
        // Don't stop to read until the client closes the connection
        while (Context.WebSocket.State == WebSocketState.Open)
        {
          result = await Context.WebSocket.ReceiveAsync(new ArraySegment<byte>(this.buffer), tokenNone);

          if (result.MessageType == WebSocketMessageType.Close)
          {
            // client closed the connection normally
            Context.WebSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, string.Empty, tokenNone).Wait();
            Feed.Report($"Client {ep.Address}:{ep.Port} closed the connection properly.", LogLvl.InfoHighlighted);
            return;
          }
          else if (result.MessageType == WebSocketMessageType.Binary)
          {
            // We only allow pure text data
            await Context.WebSocket.CloseAsync(WebSocketCloseStatus.InvalidMessageType, "Not permitted data received", tokenNone);
            Feed.Report($"Client {ep.Address}:{ep.Port} tried to send invalid data, cutting the connection.", LogLvl.Error);
            return;
          }
          else if (result.MessageType == WebSocketMessageType.Text)
          {
            // If this is the first part of the message, create the string builder to concat the upcoming parts
            if (sb == null)
              sb = new StringBuilder();

            // Get the raw data which was received from the client
            string msg = Encoding.UTF8.GetString(buffer, 0, result.Count);
            sb.Append(msg);

            Feed.Report($"Received message part: '{msg}'", LogLvl.Debug);

            if (!result.EndOfMessage)
              // Not everything received until now
              continue;

            String fullMsg = sb.ToString();
            sb.Clear();

            // If this line is reached, the whole message was received.
            if (serverReference.ProcessDataAction == null)
            {
              Feed.Report("Data was received, but no action is registered for processing the message.", LogLvl.Warn);
              continue;
            }

            var responseString = serverReference.ProcessDataAction(fullMsg);

            if (responseString != null)
            {
              byte[] payload = Encoding.Unicode.GetBytes(responseString);
              payload = Encoding.Convert(Encoding.Unicode, Encoding.UTF8, payload);
              await Context.WebSocket.SendAsync(new ArraySegment<byte>(payload), WebSocketMessageType.Text, true, tokenNone);
            }

            /*
            int remainingNumBytes = payload.Length;
            int offset = 0;
            bool endOfMsg = false;
            while (remainingNumBytes > 0)
            {
                int lg = Math.Min(remainingNumBytes, wsBufferSize);
                var segment = new ArraySegment<byte>(payload, offset, lg);

                if (remainingNumBytes == lg)
                    endOfMsg = true;

                remainingNumBytes -= lg;
                offset += lg;

                App.Feed.Report($"WebSocket: Sending {segment.Count} bytes, then {remainingNumBytes} bytes will remain (EOM:{endOfMsg})...", LogLvl.Debug);

                context.WebSocket.SendAsync(segment, WebSocketMessageType.Text, endOfMsg, tokenNone);
            }
            App.Feed.Report($"Websocket: Sent all segments", LogLvl.Debug);*/
          }
        }
      }
      catch (AggregateException)
      {
        Feed.Report($"Client connection for {ep.Address}:{ep.Port} canceled.", LogLvl.InfoHighlighted);
      }
      catch (Exception ex)
      {
        // This will occur also on closing the web app, just output a warning
        Feed.Report("Closing the connection due to an exception:", ex, LogLvl.Warn);
      }
      finally
      {
        if (Context != null)
        {
          if (Context.WebSocket.State == WebSocketState.Open)
          {
            // The connection is still open, close it properly for the client
            await Context.WebSocket.CloseAsync(WebSocketCloseStatus.InternalServerError,
                "Connection is closed due to an internal server error.", tokenNone);
          }
          Context = null;
        }
      }
    }
  }
}
