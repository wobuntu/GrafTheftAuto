﻿using System;
using Core.Logging;
using System.Security.Principal;
using System.IO;

namespace Core.Net.Websockets
{
    public static class Util
    {
        /// <summary>
        /// Prompts the user to allow listening to a port (calls a netsh command).
        /// </summary>
        /// <param name="address">The address to add. (Eg. 'http://+:8080/' or 'https://+:8080/' to allow listening for port 8080.</param>
        public static void AllowHttpListening(string address)
        {
            // TODO: Add method to allow this only for the current user

            // Check for already registered entries (we have to use the full system path to the file, since we cannot use
            // UseShellExecute in combination with RedirectStandardOutput
            string args = $"http show urlacl";
            string netsh = Path.Combine(Environment.SystemDirectory, "netsh.exe");
            System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo(netsh, args);
            psi.CreateNoWindow = true;
            psi.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            psi.UseShellExecute = false;
            psi.RedirectStandardOutput = true;
            var process = System.Diagnostics.Process.Start(psi);

            string output = process.StandardOutput.ReadToEnd();
            process.WaitForExit();

            // TODO: How to do this on linux?
            // found on stackoverflow + lots of research to find out that user=everyone must be typed in the machine language...

            if (output.Contains(address))
            {
                Feed.Report($"Address {address} seems to be already reserved for the app.");
                return;
            }

            Feed.Report($"Promping user to allow listening to address {address}...");
            System.Globalization.CultureInfo culture = System.Globalization.CultureInfo.CurrentCulture;
            var sid = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
            var account = (NTAccount)sid.Translate(typeof(NTAccount));
            string user = account.Value;

            args = $"http add urlacl url={address} user={user} listen=yes";
            psi = new System.Diagnostics.ProcessStartInfo("netsh", args);
            psi.Verb = "runas";
            psi.CreateNoWindow = true;
            psi.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            psi.UseShellExecute = true;

            System.Diagnostics.Process.Start(psi).WaitForExit();
        }
    }
}
