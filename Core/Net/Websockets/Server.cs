﻿using System;
using System.Collections.Concurrent;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Core.AsyncUtils;
using Core.Logging;

namespace Core.Net.Websockets
{
  public class Server : ITransportServer<string, string>
  {
    public Server(string socketUrl)
    {
      this.socketUrl = socketUrl;
    }

    CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
    Task websocketListenerTask = null;
    bool shuttingDown = false;
    string socketUrl;

    AsyncLock _lock = new AsyncLock();


    #region ITransportServer implementation
    public void StartAcceptingClients()
    {
      using (_lock.LockAsync().Result)
      {
        if (websocketListenerTask == null)
        {
          websocketListenerTask = new Task(() => websocketListenerWorker());
          websocketListenerTask.Start();
        }
        else
        {
          Feed.Report("The websocket listener is already running.", LogLvl.Warn);
        }
      }
    }

    public async Task StopAcceptingClients()
    {
      using (await _lock.LockAsync())
      {
        if (websocketListenerTask == null)
        {
          Feed.Report("No websocket listener to stop available.", LogLvl.Warn);
        }
        else
        {
          cancellationTokenSource.Cancel();
        }

        // we need to wait for all threads to properly exit
        await websocketListenerTask;
        websocketListenerTask = null;
      }
    }


    public Func<string, string> ProcessDataAction { get; set; }
    #endregion


    ConcurrentBag<WebSocketClientWorker> Workers = new ConcurrentBag<WebSocketClientWorker>();

    #region Helper methods
    private async void websocketListenerWorker()
    {
      HttpListener listener = null;

      try
      {
        Core.Net.Websockets.Util.AllowHttpListening(socketUrl);
        listener = new HttpListener();
        listener.Prefixes.Add(socketUrl);
        listener.Start();

        Feed.Report("Starting web socket listener, waiting for new clients to connect...");
        while (true)
        {
          var context = await listener.GetContextAsync();
          IPEndPoint ip = context.Request.RemoteEndPoint;

          if (context.Request.IsWebSocketRequest)
          {
            Feed.Report($"New client connection pending from {ip.Address}:{ip.Port}...");
            Workers.Add(new WebSocketClientWorker(context, this));
          }
          else
          {
            Feed.Report($"Illegal connection attempt received from {ip.Address}:{ip.Port}");

            // client sides awaits a proper error response, so send it back to the
            // client and close the connection:
            context.Response.StatusCode = 400; // Bad request
            context.Response.Close();
          }
        }
      }
      catch (TaskCanceledException)
      {
        Feed.Report("The websocket listener was successfully disposed.", LogLvl.Info);
      }
      catch (Exception ex)
      {
        Feed.Report("An unexpected exception occurred, see the exception for details:", ex, LogLvl.Error);

        if (shuttingDown)
        {
          Feed.Report("The websocket listener won't be restarted, since the app is shutting down.");
          return;
        }

        // Allow some time for not utilizing all of the computing power,
        // e.g. if a firewall is blocking the socket
        Feed.Report("Restarting the websocket listener, please wait...");
        Thread.Sleep(500);

        lock (_lock)
        {
          websocketListenerTask = null;
        }

        StartAcceptingClients();
      }
      finally
      {
        if (listener != null)
        {
          listener.Close();
          while (!Workers.IsEmpty)
          {
            Workers.TryTake(out var worker);
          }
        }
      }
    }
    #endregion
  }
}
