﻿using Core.AsyncUtils;
using Core.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Net.Websockets
{
    public class Client : ITransportClient<string, string>
    {
        // TODO: Add cancellation tokens to all async methods
        #region Ctor
        public Client(string uriString)
            : this(new Uri(uriString)) { }

        public Client(Uri uri)
        {
            this.Uri = uri;
        }
        #endregion


        #region Properties
        public Uri Uri { get; private set; }
        #endregion


        #region Private Fields
        const int wsBufferSize = 65536; // = max allowed buffersize
        byte[] buffer = new byte[wsBufferSize];
        private ClientWebSocket socket;
        private bool disconnectScheduled = false;
        StringBuilder receiveStringBuilder = null;

        AsyncLock _lock = new AsyncLock();
        #endregion


        #region ITransportClient implementation
        public bool IsConnected => socket != null && socket.State == WebSocketState.Open;


        public async Task<bool> TryDisconnect()
        {
            using (await _lock.LockAsync())
            {
                if (!IsConnected)
                {
                    Feed.Report($"A disconnect attempt was made, but no connection to {this.Uri} is currently active.", LogLvl.Debug);
                    return false;
                }

                Feed.Report($"Disconnecting from Uri {this.Uri}...");
                await socket.CloseAsync(WebSocketCloseStatus.InternalServerError,
                        "Connection is closed due to an internal server error.", CancellationToken.None);

                socket = null;
                Feed.Report($"Connection closed to Uri {this.Uri}");
                return true;
            }
        }


        public async Task<string> SendReceiveAsync(string data)
        {
            using (await _lock.LockAsync())
            {
                if (!IsConnected)
                {
                    // Not yet connected
                    Feed.Report("Sending data was cancelled, no connection is established.", LogLvl.Warn);
                    return null;
                }

                try
                {
                    // Send data
                    var raw = Encoding.Unicode.GetBytes(data);
                    raw = Encoding.Convert(Encoding.Unicode, Encoding.UTF8, raw);

                    // TODO: Support bigger buffer sizes by splitting the results (like it is currently proposed in the comments of the serverclientworker)
                    await socket.SendAsync(new ArraySegment<byte>(raw), WebSocketMessageType.Text, true, CancellationToken.None);

                    // Receive the response
                    while (socket.State == WebSocketState.Open)
                    {
                        ArraySegment<byte> segm = new ArraySegment<byte>(buffer);
                        var result = await socket.ReceiveAsync(segm, CancellationToken.None);

                        if (result.MessageType == WebSocketMessageType.Close)
                        {
                            // client closed the connection normally
                            await socket.CloseAsync(WebSocketCloseStatus.NormalClosure, string.Empty, CancellationToken.None);
                            Feed.Report($"The server closed the connection (Uri={this.Uri}).", LogLvl.Info);
                            return null;
                        }
                        else if (result.MessageType == WebSocketMessageType.Binary)
                        {
                            // We only allow pure text data
                            await socket.CloseAsync(WebSocketCloseStatus.InvalidMessageType, "Not permitted data received", CancellationToken.None);
                            Feed.Report($"The server tried to send invalid data, cutting the connection.", LogLvl.Warn);
                            return null;
                        }
                        else if (result.MessageType == WebSocketMessageType.Text)
                        {
                            // If this is the first part of the message, create the string builder to concat the upcoming parts
                            if (receiveStringBuilder == null)
                                receiveStringBuilder = new StringBuilder();

                            // Get the raw data which was received from the client
                            string msg = Encoding.UTF8.GetString(buffer, 0, result.Count);
                            receiveStringBuilder.Append(msg);

                            Feed.Report($"Received message part: '{msg}'", LogLvl.Debug);

                            if (!result.EndOfMessage)
                                // Not everything received until now
                                continue;

                            String fullMsg = receiveStringBuilder.ToString();
                            receiveStringBuilder.Clear();

                            return fullMsg;
                        }
                    }
                }
                catch (AggregateException)
                {
                    Feed.Report($"Client connection for Uri {this.Uri} cancelled.", LogLvl.Info);
                }
                catch (Exception ex)
                {
                    // This will occur also on closing the web app, just output a warning
                    Feed.Report("Closing the connection due to an exception:", ex, LogLvl.Error);
                }
            }

            // If this line is entered, the connection was cancelled
            Feed.Report($"Lost connection to Uri {this.Uri}, cancelling send/receive...", LogLvl.Error);
            await TryDisconnect();
            return null;
        }


        public async Task<bool> TryConnectAsync(byte maxAttempts = 3, UInt16 retryTimeout = 200)
        {
            using (await _lock.LockAsync())
            {
                if (IsConnected)
                {
                    return false;
                }

                byte attempts = 0;
                Exception lastException = null;

                while (socket == null
                    || socket.State != WebSocketState.Connecting
                    || socket.State != WebSocketState.Open)
                {
                    try
                    {
                        if (disconnectScheduled)
                        {
                            string errorMsg = $"Connection attempt aborted due to a call to method {nameof(TryDisconnect)}.";
                            Feed.Report(errorMsg, LogLvl.Info);

                            return false;
                        }

                        if (++attempts >= maxAttempts)
                        {
                            string errMsg = $"The connection could not be established after {maxAttempts} attempts (Uri={this.Uri}).";
                            Feed.Report(errMsg, lastException, LogLvl.Warn);

                            return false;
                        }

                        socket = new ClientWebSocket();
                        await socket.ConnectAsync(this.Uri, CancellationToken.None);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        lastException = ex;
                        Feed.Report($"Could not connect to {this.Uri}, retrying after {retryTimeout}ms.", LogLvl.Warn);
                        await Task.Delay(retryTimeout);
                    }
                }

                return true;
            }
        }
        #endregion
    }
}
