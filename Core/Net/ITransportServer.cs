﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Net
{
    public interface ITransportServer<InType, OutType>
    {
        void StartAcceptingClients();

        Task StopAcceptingClients();

        Func<InType, OutType> ProcessDataAction { get; set; }
    }
}
