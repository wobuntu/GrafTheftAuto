﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Net
{
    public interface ITransportClient<InType, OutType>
    {
        Task<bool> TryConnectAsync(byte maxAttempts = 3, UInt16 retryTimeout = 200);

        bool IsConnected { get; }

        Task<bool> TryDisconnect();

        Task<OutType> SendReceiveAsync(InType data);
    }
}
