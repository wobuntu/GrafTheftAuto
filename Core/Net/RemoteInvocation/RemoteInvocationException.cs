﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Net.RemoteInvocation
{
    public class RemoteInvocationException : Exception
    {
        const string err = "Server tells the following error: ";

        public RemoteInvocationException(string message) : base(message) { }

        public RemoteInvocationException(RemoteInvocationInfo source) : base(err + extractMessage(source))
        {
            this.CausingRemoteInvocationInfo = source;
        }

        public RemoteInvocationException(string message, RemoteInvocationInfo source) : base(err + message + " - " + extractMessage(source))
        {
            this.CausingRemoteInvocationInfo = source;
        }

        public RemoteInvocationInfo CausingRemoteInvocationInfo { get; protected set; } = null;

        private static string extractMessage(RemoteInvocationInfo source)
        {
            if (source != null
                && source.Params != null
                && source.Params.Length > 0
                && source.Params[0] != null)
            {
                return source.Params[0].ToString();
            }

            return "Unknown invocation error.";
        }
    }
}
