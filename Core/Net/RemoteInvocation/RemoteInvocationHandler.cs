﻿using Core.ComponentDiscovery;
using Core.Logging;
using Core.Net;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;

namespace Core.Net.RemoteInvocation
{
    public class RemoteInvocationHandler
    {
        const string commandIdentifier = "___command___";
        const string responseIdentifier = "___response___";
        const string valueIdentifier = "___value___";
        const string getRegisteredInvocationTargetsCommand = "GetRegisteredInvocationTargets";

        public RemoteInvocationHandler(ITransportServer<string, string> serverToBeConfigured)
        {
            serverToBeConfigured.ProcessDataAction = (request) =>
            {
                RemoteInvocationInfo received = RemoteInvocationInfo.FromJson(request);

                if (received == null)
                {
                    return RemoteInvocationInfo.BuildErrorResponse("Unexpected data received from client", received.Id).ToJson();
                }

                RemoteInvocationInfo response = InvokeLocal(received);
                return response.ToJson();
            };

            serverToBeConfigured.StartAcceptingClients();
        }

        public RemoteInvocationInfo InvokeLocal(RemoteInvocationInfo info)
        {
            return InvokeLocal(info.Id, info.ComponentName, info.TargetName, info.TypeParams, info.Params);
        }

        public RemoteInvocationInfo InvokeLocal(long id,
            string componentString,
            string targetString,
            Type[] genericTypeParams,
            params object[] parameters)
        {
            if (componentString == responseIdentifier
                && targetString == "___error___")
            {
                // This is an error response, indicating that an error occured during parsing, send it back as it is
                return new RemoteInvocationInfo(id, componentString, targetString, genericTypeParams, parameters);
            }

            if (string.IsNullOrWhiteSpace(componentString) || string.IsNullOrWhiteSpace(targetString))
                return RemoteInvocationInfo.BuildErrorResponse("Invalid component or target specified.", id);

            if (parameters == null)
                parameters = new object[0]; // will be easier for the following code

            if (componentString == commandIdentifier)
            {
                switch (targetString)
                {
                    case getRegisteredInvocationTargetsCommand:
                        Feed.Report("Client asks for available invocation targets", LogLvl.Debug);
                        var targets = registeredComponents.Select((x) => x.Key).ToArray();

                        Feed.Report($"Returning {targets.Length} found targets.", LogLvl.Debug);
                        return new RemoteInvocationInfo(id, responseIdentifier, valueIdentifier, null, new [] { targets });
                }

                Feed.Report($"Client asked for an invalid command '{targetString}', returning a error response");
                return RemoteInvocationInfo.BuildErrorResponse("Invalid command.", id);
            }
            else
            {
                // Not a control command received, should be a command to invoke a method of a registered component
                var componentInstance = GetRegisteredLocalTargets()
                    .Where((p) => p.Key == componentString)
                    .Select((p) => p.Value)
                    .FirstOrDefault();

                if (componentInstance == null)
                {
                    Feed.Report($"Client requested a non-existend component named '{componentString}'", LogLvl.Warn);
                    return RemoteInvocationInfo.BuildErrorResponse($"Component '{componentString}' not found", id);
                }

                try
                {
                    // I ♥♥♥♥♥ reflection, I ♥♥♥♥♥ Linq
                    IEnumerable<MethodInfo> candidates = componentInstance.GetType().GetMethods().Where((x) => x.Name == targetString);
                    Feed.Report($"Client requests execution of '{targetString}' from component '{componentString}'", LogLvl.Debug);

                    // Found matching methods, now find the correct overload
                    MethodInfo matchingMethodInfo = null;
                    ParameterInfo[] methodParams = null;

                    foreach (var curMethodInfo in candidates)
                    {
                        methodParams = curMethodInfo.GetParameters();
                        if (methodParams.Length != parameters.Length)
                            continue;

                        bool found = true;
                        for (int i = 0; i < methodParams.Length; i++)
                        {
                            // TODO: Future: Multiple equally named and equally 
                            if (parameters[i] != null && methodParams[i].ParameterType.IsAssignableFrom(parameters[i].GetType()))
                                continue;
                            else if (parameters[i] == null && !methodParams[i].ParameterType.IsValueType)
                                // may be ok if a param is just null
                                continue;

                            found = false;
                            break;
                        }

                        if (found)
                        {
                            Feed.Report("Found matching method using Reflection", LogLvl.Debug);
                            matchingMethodInfo = curMethodInfo;
                            break;
                        }
                    }

                    if (matchingMethodInfo == null)
                    {
                        Feed.Report($"A matching overload for the requested method '{targetString}' in component '{componentString}' "
                            + $"could not be found.", LogLvl.Warn);
                        return RemoteInvocationInfo.BuildErrorResponse(
                            $"Did not find a matching method named '{targetString}' in '{componentString}'", id);
                    }

                    if (matchingMethodInfo.IsGenericMethod)
                    {
                        Feed.Report("Requested method is generic, transforming it to a concrete type...", LogLvl.Debug);
                        try
                        {
                            matchingMethodInfo = matchingMethodInfo.MakeGenericMethod(genericTypeParams);
                            Feed.Report("Generic method successfully transformed to a concrete type.", LogLvl.Debug);
                        }
                        catch
                        {
                            if (genericTypeParams == null || genericTypeParams.Length == 0)
                            {
                                Feed.Report($"A generic method named '{targetString}' was found in component '{componentString}', "
                                    + $"but no generic type params for selecting the concrete one was given", LogLvl.Warn);

                                return RemoteInvocationInfo.BuildErrorResponse("Request is missing type params.", id);
                            }
                            else
                            {
                                Feed.Report($"Could not find a generic method named '{targetString}' in component '{componentString}', "
                                    + $"which supports the following type parameters: {string.Join<Type>(", ", genericTypeParams)}",
                                    LogLvl.Warn);

                                return RemoteInvocationInfo.BuildErrorResponse(
                                    "Could not find matching concrete type of generic method.", id);
                            }
                        }
                    }

                    Object result = null;
                    if (methodParams.Length == 0)
                        result = matchingMethodInfo.Invoke(componentInstance, null);
                    else
                        result = matchingMethodInfo.Invoke(componentInstance, parameters);

                    Feed.Report("Dynamic invocation successful. Sending a response back to the client.", LogLvl.Debug);
                    return RemoteInvocationInfo.BuildResponseFromObject(result, id);
                }
                catch (Exception ex)
                {
                    Feed.Report($"An unexpected exception occurred during the remote invocation: {ex}", LogLvl.Error);
                    return RemoteInvocationInfo.BuildErrorResponse($"Unexpected exception occurred: {ex}", id);
                }
            }
        }


        ConcurrentDictionary<string, object> registeredComponents = new ConcurrentDictionary<string, object>();


        public void RegisterLocalTarget(string componentName, object component, bool throwExceptionIfAlreadyAdded = false)
        {
            if (registeredComponents.ContainsKey(componentName))
            {
                if (throwExceptionIfAlreadyAdded)
                {
                    throw new ArgumentException($"A target with name {componentName} was already registered.");
                }
            }
            else
            {
                registeredComponents.TryAdd(componentName, component);
            }
        }


        public void UnregisterLocalTarget(string componentName, bool throwExceptionIfUnknown = false)
        {
            if (!registeredComponents.ContainsKey(componentName))
            {
                if (throwExceptionIfUnknown)
                {
                    throw new ArgumentException($"A target with name {componentName} does not exist, thus cannot be unregistered.");
                }
            }
            else
            {
                registeredComponents.TryRemove(componentName, out var val);
            }
        }


        public IReadOnlyDictionary<string, object> GetRegisteredLocalTargets()
        {
            return registeredComponents;
        }
    }
}
