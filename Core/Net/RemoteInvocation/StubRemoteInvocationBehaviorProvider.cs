﻿using Core.Logging;
using Core.Net.Stubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Net.RemoteInvocation
{
  // TODO: Add proper error handling here!
  public class StubRemoteInvocationBehaviorProvider : StubBehaviorProvider
  {
    const string behaviorActionsMustNotBeModified = "Remote invocation stub behavior actions cannot be modified";

    public ITransportClient<string, string> TransportClient { get; set; }

    public StubRemoteInvocationBehaviorProvider(ITransportClient<string, string> clientToUse)
        : base() // Base does the registering for so that the behavior can be found later on by stubs
    {
      this.TransportClient = clientToUse;

      void connect()
      {
        if (TransportClient.IsConnected)
        {
          return;
        }

        if (!TransportClient.TryConnectAsync().Result)
        {
          throw new RemoteInvocationException("Server is not reachable.");
        }
      }

      base.MethodInvokeAction = (invokeArgs) =>
      {
        connect();
        RemoteInvocationInfo info = new RemoteInvocationInfo(
                  invokeArgs.ComponentName,
                  invokeArgs.MethodName,
                  invokeArgs.GenericTypeParameters.ToArray(),
                  invokeArgs.Arguments.Select((x) => x.Value).ToArray());

        string json = info.ToJson();
        string response = TransportClient.SendReceiveAsync(json).Result;

        info = RemoteInvocationInfo.FromJson(response);
        info.EnsureNoRemoteError();

        return info.ToResponseValue<object>();
      };

      base.PropertySetAction = (invokeArgs) =>
      {
        connect();
        RemoteInvocationInfo info = new RemoteInvocationInfo(
                  invokeArgs.ComponentName,
                  "set_" + invokeArgs.PropertyName,
                  null,
                  invokeArgs.Value);

        string json = info.ToJson();
        string response = TransportClient.SendReceiveAsync(json).Result;

        info = RemoteInvocationInfo.FromJson(response);
        info.EnsureNoRemoteError();
      };

      base.PropertyGetAction = (invokeArgs) =>
      {
        connect();
        RemoteInvocationInfo info = new RemoteInvocationInfo(
                  invokeArgs.ComponentName,
                  "get_" + invokeArgs.PropertyName,
                  null,
                  null);

        string json = info.ToJson();
        string response = TransportClient.SendReceiveAsync(json).Result;

        info = RemoteInvocationInfo.FromJson(response);
        info.EnsureNoRemoteError();

        return info.ToResponseValue<object>();
      };
    }

    public override Func<StubMethodInvokeArgs, object> MethodInvokeAction
    {
      get => base.MethodInvokeAction;
      set => throw new Exception(behaviorActionsMustNotBeModified);
    }

    public override Func<StubPropertyGetArgs, object> PropertyGetAction
    {
      get => base.PropertyGetAction;
      set => throw new Exception(behaviorActionsMustNotBeModified);
    }

    public override Action<StubPropertySetArgs> PropertySetAction
    {
      get => base.PropertySetAction;
      set => throw new Exception(behaviorActionsMustNotBeModified);
    }
  }
}
