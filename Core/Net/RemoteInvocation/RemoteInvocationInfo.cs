﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;

using Newtonsoft.Json;
using Core.Logging;

namespace Core.Net.RemoteInvocation
{
    public class RemoteInvocationInfo
    {
        #region Ctor
        [JsonConstructor]
        private RemoteInvocationInfo() { }

        public RemoteInvocationInfo(string componentName, string targetName, Type[] genericTypeParams, params object[] arguments)
            : this (NextId, componentName, targetName, genericTypeParams, arguments) { }

        internal RemoteInvocationInfo(long id, string componentName, string targetName, Type[] genericTypeParams, params object[] arguments)
        {
            this.ComponentName = componentName;
            this.TargetName = targetName;
            this.Params = arguments;
            this.TypeParams = genericTypeParams;
            this.Id = id;

            // Also store the types of the params for later deserialisation
            if (Params == null)
            {
                ParamTypes = null;
            }
            else
            {
                ParamTypes = new Type[Params.Length];
                for (int i = 0; i < Params.Length; i++)
                {
                    if (Params[i] == null)
                        ParamTypes[i] = null;
                    else
                        ParamTypes[i] = Params[i].GetType();
                }
            }
        }
        #endregion


        #region Fields and properties
        public string ComponentName { get; private set; }
        public string TargetName { get; private set; }
        public object[] Params { get; private set; }
        public Type[] TypeParams { get; private set; }
        public Type[] ParamTypes { get; private set; }
        public long Id { get; private set; }

        private static long nextId = 0;
        protected static long NextId
        {
            get
            {
                if (nextId == long.MaxValue)
                {
                    nextId = 1;
                }
                else
                {
                    nextId++;
                }

                return nextId;
            }
        }

        static JsonSerializerSettings jsonSettings = new JsonSerializerSettings
        {
            ContractResolver = new Serialization.Helpers.JsonHiddenPropResolver(),
            Formatting = Formatting.Indented, // <-- easier for debugging
            TypeNameHandling = TypeNameHandling.All,
            ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
            PreserveReferencesHandling = PreserveReferencesHandling.Objects
        };

        #endregion


        #region Response helpers
        public static RemoteInvocationInfo BuildResponseFromObject(object obj, long id)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo();
            info.ComponentName = "___response___";
            info.TargetName = "___value___";
            info.Id = id;

            info.Params = new object[] { obj };
            info.ParamTypes = new Type[] { obj?.GetType() };

            return info;
        }

        public RemoteInvocationInfo BuildResponseFromObject(object obj)
        {
            return BuildResponseFromObject(obj, this.Id);
        }

        public static RemoteInvocationInfo BuildErrorResponse(String message, long id)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo();
            info.ComponentName = "___response___";
            info.TargetName = "___error___";
            info.Id = id;
            info.Params = new object[] { message };
            info.ParamTypes = new Type[] { message?.GetType() };

            return info;
        }

        public RemoteInvocationInfo BuildErrorResponse(String message)
        {
            return BuildErrorResponse(message, this.Id);
        }

        public T ToResponseValue<T>()
        {
            if (this.ComponentName != "___response___"
                || this.TargetName != "___value___"
                || this.Params == null
                || this.Params.Length != 1)
            {
                throw new Exception($"Cannot extract a response value from a {nameof(RemoteInvocationInfo)}, which is not a response.");
            }

            // Methods of this types are just for convenience and to hide the implementation details...
            return (T)this.Params[0];
        }
        #endregion


        #region Json conversion
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, this.GetType(), jsonSettings);
        }

        public static RemoteInvocationInfo FromJson(String json)
        {
            try
            {
                var info = JsonConvert.DeserializeObject<RemoteInvocationInfo>(json, jsonSettings);

                // Restore faulty types which were not correctly deserialized by json
                if (info.Params == null)
                    return info;

                for (int i = 0; i < info.Params.Length; i++)
                {
                    if (info.Params[i] == null)
                        continue;

                    if (info.ParamTypes[i] == typeof(Guid) && info.Params[i] is string guidStr)
                        info.Params[i] = Guid.Parse(guidStr);
                    else if (info.ParamTypes[i] != null && info.ParamTypes[i].IsEnum)
                        info.Params[i] = Enum.Parse(info.ParamTypes[i], info.Params[i].ToString());
                    else
                        info.Params[i] = Convert.ChangeType(info.Params[i], info.ParamTypes[i]);
                }

                return info;
            }
            catch (Exception ex)
            {
                Feed.Report($"Could not properly deserialize a {nameof(RemoteInvocationInfo)}: {Environment.NewLine}{json}", ex,
                    LogLvl.Warn);
                return RemoteInvocationInfo.BuildErrorResponse("Message not interpretable: " + ex, 0);
            }
        }
        #endregion


        #region Further helpers and convinience methods
        public void EnsureNoRemoteError()
        {
            if (this.ComponentName == "___response___"
                && this.TargetName == "___error___")
            {
                throw new RemoteInvocationException(this);
            }
        }
        #endregion
    }
}