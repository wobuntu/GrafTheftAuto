﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Net.Stubs
{
    public class StubMethodArgument
    {
        public StubMethodArgument(Type declaredType, object value, string name)
        {
            this.ArgumentType = declaredType;
            this.Value = value;
            this.Name = name;
        }

        public Type ArgumentType { get; protected set; }
        public object Value { get; protected set; }
        public string Name { get; protected set; }
    }
}
