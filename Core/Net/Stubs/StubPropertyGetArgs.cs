﻿using System;
using System.Collections.Generic;

namespace Core.Net.Stubs
{
    public class StubPropertyGetArgs : EventArgs
    {
        public StubPropertyGetArgs(Type propertyType, string propertyName, string componentName)
        {
            this.PropertyType = propertyType;
            this.PropertyName = propertyName;
            this.ComponentName = componentName;
        }
        public Type PropertyType { get; protected set; }
        public string PropertyName { get; protected set; }
        public string ComponentName { get; protected set; }
    }
}
