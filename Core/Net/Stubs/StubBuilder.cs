﻿using System;
using System.Linq;
using System.Text;
using Core.Extensions;
using Microsoft.CSharp;
using System.Reflection;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using Core.Logging;

namespace Core.Net.Stubs
{
  public class StubBuilder
  {
    /// <summary>
    /// Generates a stub at runtime for the given interface. A <see cref="StubBehaviorProvider"/> must be provided which specifies
    /// the behavior of it.
    /// </summary>
    /// <typeparam name="T">The interface for which a stub shall be created.</typeparam>
    /// <param name="invocationProvider">A provider which specifies the behavior of the generated stub.</param>
    public static T CompileStubFor<T>(StubBehaviorProvider invocationProvider)
    {
      // Check if we really got an interface
      Type interfaceType = typeof(T);
      if (!interfaceType.IsInterface)
      {
        throw new Exception("The type parameter must be an interface.");
      }
      if (invocationProvider == null)
      {
        throw new ArgumentNullException(nameof(invocationProvider),
            $"A {nameof(StubBehaviorProvider)} is required to compile a stub (null was given).");
      }

      // Check for assemblies which must be referenced in the compilation
      List<string> assemblies = new List<string>() { interfaceType.Assembly.Location };
      string handlerLocation = typeof(StubBehaviorProvider).Assembly.Location;
      if (!assemblies.Contains(handlerLocation))
      {
        assemblies.Add(handlerLocation);
      }
      // TODO: Quickfix, should be automatically detected
      if (!assemblies.Contains(typeof(INotifyPropertyChanged).Assembly.Location))
      {
        assemblies.Add(typeof(INotifyPropertyChanged).Assembly.Location);
      }

      String componentName = interfaceType.GetFriendlyTypeName(false, false);

      // Start to build the stub's code file
      StringBuilder sb = new StringBuilder();
      sb.AppendLine($"using System;{Environment.NewLine}using {typeof(StubBehaviorProvider).Namespace};");
      sb.AppendLine("namespace GeneratedStubs {");
      sb.Append("  public class ");
      sb.Append(componentName);
      sb.Append("Impl : ");
      sb.AppendLine(interfaceType.GetFriendlyTypeName());
      sb.AppendLine("  {");

      var interfacePropInfos = interfaceType.GetRuntimeProperties();
      var propGetSetMethods = interfacePropInfos.SelectMany((x) => new[] { x.GetMethod, x.SetMethod });

      // Create the methods
      foreach (var interfaceMethod in interfaceType.GetMethods().Except(propGetSetMethods))
      {
        sb.Append(describeInterfaceMethod(componentName, interfaceMethod, invocationProvider));
      }

      // Create properties
      foreach (var interfaceProperty in interfaceType.GetRuntimeProperties())
      {
        sb.Append(describeInterfaceProperty(componentName, interfaceProperty, invocationProvider));
      }

      sb.Append($"  }}{Environment.NewLine}}}");

      // Set up the compiler and compile the file
      CompilerParameters compilerParameters = new CompilerParameters(assemblies.ToArray())
      {
        GenerateInMemory = true,
        GenerateExecutable = false,
        OutputAssembly = "GeneratedStubs",
      };
      CSharpCodeProvider provider = new CSharpCodeProvider();
      CompilerResults results = provider.CompileAssemblyFromSource(
          compilerParameters, new[] { sb.ToString() });

      if (results.Errors.Count > 0)
      {
        Feed.Report($"Could not compile stub for interface {interfaceType.Name} ({results.Errors.Count} Compilation errors).", LogLvl.Error);
        // TODO: Own exception type
        throw new Exception($"Could not compile stub for interface {interfaceType.Name} ({results.Errors.Count} Compilation errors).");
      }

      // Now pull the generated stub from the compiled assembly and return it
      Type generatedType = results.CompiledAssembly.GetType("GeneratedStubs." + interfaceType.GetFriendlyTypeName(false, false) + "Impl");
      T instance = (T)Activator.CreateInstance(generatedType);

      return instance;
    }


    /// <summary>
    /// Private method which assembles a code string containing the calls to the <see cref="StubBehaviorProvider"/> for the stub.
    /// It simply forwards the properties to the provider, which is responsible for handling the call.
    /// </summary>
    /// <param name="propMirror">The property info, coming from the interface.</param>
    /// <param name="invocationProvider">The invocation provider which shall be called.</param>
    static String describeInterfaceProperty(String componentName, PropertyInfo propMirror, StubBehaviorProvider invocationProvider)
    {
      // We don't need to check for private or protected get/set methods,
      // since we only check interfaces in which this cannot be specified
      StringBuilder sb = new StringBuilder();

      // Write the property body
      sb.AppendLine($"    public {propMirror.PropertyType.GetFriendlyTypeName()} {propMirror.Name}");
      sb.AppendLine("    {");

      if (propMirror.GetMethod != null)
      {
        sb.AppendLine($"      get{Environment.NewLine}      {{");
        sb.AppendLine($"        var provider = {nameof(StubBehaviorProvider)}.{nameof(StubBehaviorProvider.TryGet)}(" +
            $"Guid.Parse(\"{invocationProvider.ID}\"));");
        sb.AppendLine($"        if (provider == null || provider.{nameof(StubBehaviorProvider.PropertyGetAction)} == null)");
        if (typeof(System.Guid).IsAssignableFrom(propMirror.PropertyType))
        {
          sb.AppendLine($"          return System.Guid.Empty;");
        }
        else if (typeof(bool).IsAssignableFrom(propMirror.PropertyType))
        {
          // Default value would result in "False"
          sb.AppendLine($"          return false");
        }
        else
        {
          var defaultVal = propMirror.PropertyType.GetDefaultValue();
          if (defaultVal == null)
          {
            sb.AppendLine($"          return null;");
          }
          else
          {
            // TODO: Double.NaN etc won't not be catched here anyways, this still does need a fix
            sb.AppendLine($"          return {propMirror.PropertyType.GetDefaultValue()};");
          }
        }
        sb.Append($"        return ({propMirror.PropertyType.GetFriendlyTypeName()}) ");
        sb.AppendLine($"provider.{nameof(StubBehaviorProvider.PropertyGetAction)}" +
            $".Invoke(new {nameof(StubPropertyGetArgs)}(" +
            $"typeof({propMirror.PropertyType.GetFriendlyTypeName()}), " +
            $"\"{propMirror.Name}\", " +
            $"\"{componentName}\"));{Environment.NewLine}}}");
      }
      if (propMirror.SetMethod != null)
      {
        sb.AppendLine($"      set{Environment.NewLine}      {{");
        sb.AppendLine($"        var provider = {nameof(StubBehaviorProvider)}.{nameof(StubBehaviorProvider.TryGet)}(" +
            $"Guid.Parse(\"{invocationProvider.ID}\"));");
        sb.AppendLine($"        if (provider == null || provider.{nameof(StubBehaviorProvider.PropertySetAction)} == null)");
        sb.AppendLine($"          return;");
        sb.AppendLine($"provider.{nameof(StubBehaviorProvider.PropertySetAction)}" +
            $".Invoke(new {nameof(StubPropertySetArgs)}(" +
            $"typeof({propMirror.PropertyType.GetFriendlyTypeName()}), value, " +
            $"\"{propMirror.Name}\", " +
            $"\"{componentName}\"));{Environment.NewLine}}}");
      }

      sb.AppendLine("    }");
      return sb.ToString();
    }


    /// <summary>
    /// Private method which assembles a code string containing the calls to the <see cref="StubBehaviorProvider"/> for the stub.
    /// It simply forwards all arguments to the provider, which is responsible for handling the call.
    /// </summary>
    /// <param name="methodMirror">The method info, coming from the interface.</param>
    /// <param name="invocationProvider">The invocation provider which shall be called.</param>
    static String describeInterfaceMethod(String componentName, MethodInfo methodMirror, StubBehaviorProvider invocationProvider)
    {
      // Extract the required information which is required to assemble the proxy method properly
      ParameterInfo[] paramInfos = methodMirror.GetParameters();
      Type[] genericTypeParams = methodMirror.GetGenericArguments();
      Type retType = methodMirror.ReturnType;

      // Prepare variables for building the code string
      StringBuilder sb = new StringBuilder();
      List<StubMethodArgument> stubMethodArguments = new List<StubMethodArgument>();
      var genericTypesStrings = genericTypeParams.Select((t) => t.GetFriendlyTypeName());

      string[] paramStrings = new string[paramInfos.Length];
      for (int i = 0; i < paramStrings.Length; i++)
      {
        Type t = paramInfos[i].ParameterType;

        paramStrings[i] = t.GetFriendlyTypeName() + " " + paramInfos[i].Name;
        stubMethodArguments.Add(new StubMethodArgument(t, t.GetDefaultValue(), paramInfos[i].Name));
      }

      // Write the method signature including the parameters
      sb.Append("    public ");
      if (methodMirror.IsVirtual)
      {
        sb.Append("virtual ");
      }
      sb.Append(retType == typeof(void) ? "void" : retType.GetFriendlyTypeName());
      sb.Append($" {methodMirror.Name}");

      if (genericTypesStrings.Count() > 0)
      {
        sb.Append($"<{string.Join(", ", genericTypesStrings)}>");
      }

      sb.Append("(");

      if (paramStrings.Length > 0)
      {
        sb.Append(string.Join(", ", paramStrings));
      }

      sb.AppendLine(") {");

      // Write the method body: Get the StubInvocationProvider for handling the call behaviour
      sb.AppendLine($"      var provider = {nameof(StubBehaviorProvider)}.{nameof(StubBehaviorProvider.TryGet)}(" +
          $"Guid.Parse(\"{invocationProvider.ID}\"));");
      sb.AppendLine($"      if (provider == null || provider.{nameof(StubBehaviorProvider.MethodInvokeAction)} == null)");

      if (retType == typeof(void))
      {
        sb.AppendLine("        return;");
        sb.Append("      ");
      }
      else
      {
        var defaultVal = retType.GetDefaultValue();
        if (defaultVal == null)
        {
          defaultVal = "null";
        }
        else if (defaultVal is String s)
        {
          defaultVal = $"\"{s}\"";
        }
        else if (defaultVal is Guid g)
        {
          defaultVal = "Guid.Empty";
        }
        else if (defaultVal is bool b)
        {
          defaultVal = "false";
        }
        sb.AppendLine($"        return {defaultVal};");
        sb.Append($"      return ({retType.GetFriendlyTypeName()})");
      }

      sb.AppendLine($"provider.{nameof(StubBehaviorProvider.MethodInvokeAction)}" +
          $".Invoke(new {nameof(StubMethodInvokeArgs)}(" +
          $"new Type[]{{ {string.Join(", ", genericTypesStrings.Select((x) => $"typeof({x})"))} }}, " +
          $"{describeStubMethodArguments(stubMethodArguments)}, " +
          $"typeof({retType.GetFriendlyTypeName()}), " +
          $"\"{methodMirror.Name}\", " +
          $"\"{componentName}\"));{Environment.NewLine}    }}");

      return sb.ToString();
    }


    /// <summary>
    /// Private helper method for <see cref="describeInterfaceMethod(MethodInfo, StubBehaviorProvider)"/>. It assembles a code string
    /// for the <see cref="StubMethodArgument"/>s for calls to the <see cref="StubBehaviorProvider"/>.
    /// </summary>
    /// <param name="arguments">The arguments to write as an initialization array.</param>
    static string describeStubMethodArguments(List<StubMethodArgument> arguments)
    {
      StringBuilder sb = new StringBuilder();
      sb.Append($"new {nameof(StubMethodArgument)}[] {{ ");

      string[] argStrings = new string[arguments.Count];
      for (int i = 0; i < arguments.Count; i++)
      {
        argStrings[i] = $"new {nameof(StubMethodArgument)}(typeof({arguments[i].ArgumentType.GetFriendlyTypeName()}), " +
            $"{arguments[i].Name}, \"{arguments[i].Name}\")";
      }

      if (argStrings.Length > 0)
      {
        sb.Append(string.Join(", ", argStrings));
      }
      sb.Append(" }");

      return sb.ToString();
    }
  }
}
