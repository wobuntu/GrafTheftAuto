﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Net.Stubs
{
    public class StubMethodInvokeArgs : EventArgs
    {
        public StubMethodInvokeArgs(IEnumerable<Type> genericTypeParameters, IEnumerable<StubMethodArgument> arguments,
            Type returnType, string methodName, string componentName)
        {
            if (genericTypeParameters == null)
            {
                genericTypeParameters = new Type[0];
            }

            if (arguments == null)
            {
                arguments = new StubMethodArgument[0];
            }

            if (returnType == null)
            {
                returnType = typeof(void);
            }

            this.GenericTypeParameters = genericTypeParameters;
            this.Arguments = arguments;
            this.ReturnType = returnType;
            this.MethodName = methodName;
            this.ComponentName = componentName;
        }

        public IEnumerable<Type> GenericTypeParameters { get; protected set; }

        public IEnumerable<StubMethodArgument> Arguments { get; protected set; }

        public Type ReturnType { get; protected set; }

        public string MethodName { get; protected set; }

        public string ComponentName { get; protected set; }
    }
}
