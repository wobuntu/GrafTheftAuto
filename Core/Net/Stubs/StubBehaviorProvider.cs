﻿using System;
using System.Collections.Generic;

namespace Core.Net.Stubs
{
    public class StubBehaviorProvider
    {
        #region Static members
        protected static Dictionary<Guid, StubBehaviorProvider> providers = new Dictionary<Guid, StubBehaviorProvider>();
        #endregion


        #region Ctor, indexer and properties
        public StubBehaviorProvider()
        {
            this.ID = Guid.NewGuid();
            providers.Add(this.ID, this);
        }

        public static StubBehaviorProvider Get(Guid index) => providers[index];

        public static StubBehaviorProvider TryGet(Guid index)
        {
            if (providers.ContainsKey(index))
            {
                return providers[index];
            }

            return null;
        }

        public Guid ID { get; protected set; }
        #endregion


        #region Actions to invoke on property get/set and method invocation
        public virtual Action<StubPropertySetArgs> PropertySetAction { get; set; }

        public virtual Func<StubPropertyGetArgs, object> PropertyGetAction { get; set; }

        public virtual Func<StubMethodInvokeArgs, object> MethodInvokeAction { get; set; }
        #endregion
    }
}
