﻿using System.Net;
using System.Net.Sockets;

namespace Core.Net
{
  public static class Utils
  {
    public static IPAddress GetLocalIPAddress()
    {
      try
      {
        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (var ip in host.AddressList)
        {
          if (ip.AddressFamily == AddressFamily.InterNetwork)
          {
            return ip;
          }
        }
      }
      catch { }

      return null;
    }
  }
}
