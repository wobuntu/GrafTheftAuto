﻿using Core.Classes;
using System;
using System.Collections.Generic;

namespace Core.Interfaces
{
  /// <summary>
  /// This message contains a list with Guids (to identify the traffic light) and the state it should transition to.
  /// </summary>
  public interface IRegulate
  {
    IEnumerable<Tuple<Guid, TrafficLightState>> NewStates { get; }  
  }
}
