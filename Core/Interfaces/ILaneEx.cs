﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Core.Interfaces
{
  public interface ILaneEx
  {
    double Weight { get; }
    Point Start { get; set; }
    Point End { get; set; }

    IList<ILaneEx> NextLanes { get; }
    ILaneEx LeftLane { get; }
    ILaneEx RightLane { get; }

    bool IsPartOfCrossing { get; set; }
  }
}
