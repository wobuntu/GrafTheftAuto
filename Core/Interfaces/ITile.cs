﻿using Core.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
  public interface ITile
  {
    TileType TileType { get; }

    IList<ILaneEx> LanesForward { get; }
    IList<ILaneEx> LanesBackward { get; }

    int MapRow { get; }
    int MapColumn { get; }

    double Rotation { get; }
    int NumLanes { get; }
    bool IsCrossing { get; }

    IList<ITrafficLight> TrafficLights { get; }
    
    void TryConnectWith(ITile other);
  }
}
