﻿using Core.Classes;
using System.Collections.Generic;

namespace Core.Interfaces
{
  public interface IVehiclePool
  {
    /// <summary>
    /// Call to update Vehicle list within AutoAi.
    /// </summary>
    /// <param name="vehicleAiUpdate"></param>
    void UpdateVehicle(IVehicleAiUpdate vehicleAiUpdate);

    /// <summary>
    /// Set a maximum amount of vehicles crusining arround.
    /// </summary>
    /// <param name="vehicleMaximum"></param>
    void SetVehicleMaximum(uint vehicleMaximum);
    uint GetVehicleMaximum();

    /// <summary>
    /// Set the inveral, which it takes the AutoAI to init a new vehicle.
    /// </summary>
    /// <param name="initInterval">Interval in miliseconds.</param>
    void SetVehicleInitInterval(uint initInterval);
    uint GetVehicleInitInterval();

    /// <summary>
    /// Returns a list of all Vehicles on the grid.
    /// The vehicles expose their position and angle.
    /// </summary>
    List<IVehicle> CurrentVehicles { get; }

    List<VehicleWrapperDto> CurrentVehiclesAsDto { get; }

    void PauseSimulation();
    void ResumeSimulation();

    bool TryRegisterSimulation(string ip);
  }
}
