﻿using Core.Classes;

namespace Core.Interfaces
{
  public interface IMapProviderService
  {
    Map GetMap();
    bool PushMap(Map map);
  }
}
