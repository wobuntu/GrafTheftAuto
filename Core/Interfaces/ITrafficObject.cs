﻿using Core.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
  /// <summary>
  /// A traffic object is anything that might trigger a reaction by the vehicles.
  /// </summary>
  public interface ITrafficObject
  {
    Guid Id { get; }
    /// <summary>
    /// Total length in meters
    /// </summary>
    double Length { get; }
    /// <summary>
    /// Total width in meters
    /// </summary>
    double Width { get; }
    /// <summary>
    /// The direction to which the object is pointing 
    /// </summary>
    (double, double) Direction { get; }
    /// <summary>
    /// The position on the grid 
    /// </summary>
    (double, double) PositionOnGrid { get; set; }
    /// <summary>
    /// Rotation of the Vehicle from 0 to 360 degrees.
    /// </summary>
    double Angle { get; }
    /// <summary>
    /// The relative position to the middle of the road
    /// 0 means centered on the lane
    /// Negative offset means the object is on the left side of the lane
    /// Posotive offset means the object is on the right side of the lane
    /// </summary>
    double RelativePosition { get; }
    /// <summary>
    /// The lane where the object is currently located on
    /// </summary>
    ILaneEx CurrentLane { get; }
    /// <summary>
    /// The distance from the starting point of the lane to the object in meters
    /// </summary>
    double PositionOnLane { get; }
  }
}
