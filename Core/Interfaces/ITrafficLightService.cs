﻿using Core.Classes;
using System;
using System.Collections.Generic;

namespace Core.Interfaces
{
  public interface ITrafficLightService
  {
    Guid Register(IEnumerable<ITrafficLight> lights);
    IDictionary<int, TrafficLightState> GetStates(Guid simulationId);
    int Interval { get; set; }
    void PauseSimulation();
    void ResumeSimulation();
  }
}