﻿using System;
using System.Collections.Generic;

namespace Core.Interfaces
{
  /// <summary>
  /// Updates a specific vehicle with the TrafficObjects in range
  /// </summary>
  public interface IVehicleAiUpdate
  {
    Guid VehicleId { get; }
    /// <summary>
    /// Traffic related objects
    /// </summary>
    IEnumerable<ITrafficObject> TrafficObjects { get; }
  }
}
