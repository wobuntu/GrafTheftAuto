﻿using System;

namespace Core.Interfaces
{
  /// <summary>
  /// Updates a car based on its decisions
  /// </summary>
  public interface IVehicleReaction
  {
    /// <summary>
    /// Id and instance of a Vehicle with updated properties
    /// </summary>
    Tuple<Guid, IVehicle> UpdateInfo { get; }
  }
}
