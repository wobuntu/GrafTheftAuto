﻿using Core.Classes;
using System;
using System.Collections.Generic;

namespace Core.Interfaces
{
    /// <summary>
    /// Vehicles that can actively participate in the traffic flow
    /// </summary>
    public interface IVehicle : ITrafficObject
  {
    /// <summary>
    /// Lane with no succesesor, destination of Vehicle where it leaves the map again.
    /// </summary>
    ILaneEx Destination { get; } 
    
    /// <summary>
    /// Shortest path to the goal. 
    /// </summary>
    IEnumerable<IEnumerable<ILaneEx> > Route { get; }

    /// <summary>
    /// Velocity in km/h
    /// </summary>
    double Velocity { get; }

    /// <summary>
    /// Maximum velocity in km/h
    /// </summary>
    double MaxVelocity { get; }
    /// <summary>
    /// The change of velocity
    /// Positive values mean acceleration
    /// Negative values mean breaking
    /// Zero means that the velocity is maintained
    /// </summary>
    double Delta { get; }

    /// <summary>
    /// Double betwenn 0 and 1 which indicated the will to change.
    /// </summary>
    double DecisionBoundary { get; set; }

    /// <summary>
    /// Calls the vehicles Drive routine in which it basically drives based on its own decsions
    /// </summary>
    void Move(DateTime currTime); 
    
    /// <summary>
    /// Update the cars information about its invironment.
    /// </summary>
    /// <param name="vehicleAiUpdateObject">List of all <see cref="IVehicleAiUpdate"/> whitin sight of the current vehicle.</param>
    void Update(IVehicleAiUpdate vehicleAiUpdateObject);

    VehicelType VehicleType { get; }
  }
}
