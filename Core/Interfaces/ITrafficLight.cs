﻿using System.Windows;
using System.Collections.Generic;

using Core.Classes;


namespace Core.Interfaces
{
  public interface ITrafficLight
  {
    IList<Point> Positions { get; }

    int IntervalId { get; set; }

    TrafficLightState State { get; set; }
  }
}
