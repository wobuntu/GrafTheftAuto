﻿using Core.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
  public interface ISimulation
  {
    Map GetCurrentMap();

    List<TrafficLight> GetCurrentTrafficLights();
    IDictionary<int, TrafficLightState> GetCurrentTrafficLightStates();

    List<VehicleWrapperDto> GetVehicles();
    bool PauseSimulation();
    bool ResumeSimulation();

    void SetMaximumVehicles(uint maxVehicles);
    void SetInitIntervalForVehicles(uint initInterval);
    void SetTrafficLightInterval(int interval);

    uint GetMaximumVehicles();
    uint GetInitIntervalForVehicles();
    int GetTrafficLightInterval();
  }
}
