﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
  /// <summary>
  /// Is sent back by the navigation service
  /// It contains the best route to the requested destination
  /// </summary>
  public interface INavigationRoute
  {
    /// <summary>
    /// A list of lanes which should be taken to reach the destination
    /// The inner list contains the parallel lanes which lead to the same next segment.
    /// The lane can then be chosen based on the weights.
    /// </summary>
    IEnumerable<IEnumerable<ILaneEx>> Route { get; set; }
  }
}
