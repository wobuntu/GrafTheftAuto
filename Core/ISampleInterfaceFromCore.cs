﻿namespace Core
{
  public interface ISampleInterfaceFromCore
  {
    string TestMethod(string someTestString);
    int TestProperty { get; set; }
  }
}
