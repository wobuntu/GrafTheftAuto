﻿using System.Threading.Tasks;
using System.Collections.Generic;

// Thanks to: https://blogs.msdn.microsoft.com/pfxteam/2012/02/12/building-async-coordination-primitives-part-5-asyncsemaphore/
namespace Core.AsyncUtils
{
    public class AsyncSemaphore
    {
        private readonly static Task s_completed = Task.FromResult(true);
        private readonly Queue<TaskCompletionSource<bool>> waiterQueue = new Queue<TaskCompletionSource<bool>>();
        private uint currentCount;

        public AsyncSemaphore(uint initialCount)
        {
            currentCount = initialCount;
        }

        public Task WaitAsync()
        {
            lock (waiterQueue)
            {
                if (currentCount > 0)
                { 
                    --currentCount;
                    return s_completed;
                }
                else
                {
                    var waiter = new TaskCompletionSource<bool>();
                    waiterQueue.Enqueue(waiter);
                    return waiter.Task;
                }
            }
        }

        public void Release()
        {
            TaskCompletionSource<bool> toRelease = null;
            lock (waiterQueue)
            {
                if (waiterQueue.Count > 0)
                    toRelease = waiterQueue.Dequeue();
                else
                    ++currentCount;
            }
            if (toRelease != null)
                toRelease.SetResult(true);
        }
    }
}
