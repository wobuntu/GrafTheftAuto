﻿using System;
using System.Threading;
using System.Threading.Tasks;

// Thanks to: https://blogs.msdn.microsoft.com/pfxteam/2012/02/12/building-async-coordination-primitives-part-6-asynclock/
namespace Core.AsyncUtils
{
    public class AsyncLock
    {
        public AsyncLock()
        {
            asyncSemaphoreHelper = new AsyncSemaphore(1);
            releaser = Task.FromResult(new Releaser(this));
        }

        private readonly AsyncSemaphore asyncSemaphoreHelper;
        private readonly Task<Releaser> releaser;

        public Task<Releaser> LockAsync()
        {
            var wait = asyncSemaphoreHelper.WaitAsync();
            return wait.IsCompleted ?
                releaser :
                wait.ContinueWith((_, state) => new Releaser((AsyncLock)state),
                    this, CancellationToken.None,
                    TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Default);
        }

        public struct Releaser : IDisposable
        {
            private readonly AsyncLock toRelease;

            internal Releaser(AsyncLock lockToRelease) { toRelease = lockToRelease; }

            public void Dispose()
            {
                if (toRelease != null)
                    toRelease.asyncSemaphoreHelper.Release();
            }
        }
    }
}
