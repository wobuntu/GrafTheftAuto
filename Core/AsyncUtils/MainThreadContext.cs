﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Core.AsyncUtils
{
    public class MainThreadContext
    {
        public static TaskScheduler Current { get; private set; }

        public static TaskScheduler Initialize()
        {
            if (Current != null)
                return Current;

            if (SynchronizationContext.Current == null)
                SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

            return Current = TaskScheduler.FromCurrentSynchronizationContext();
        }
    }
}
