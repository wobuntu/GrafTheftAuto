﻿namespace Core.Classes
{
  public enum VehicelType
  {
    Car = 0,
    Truck = 1,
    Bus = 2,
    Motorbike = 3,
    Tractor = 4
  }
}
