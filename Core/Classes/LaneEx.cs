﻿using Core.Interfaces;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace Core.Classes
{
  public class LaneEx : ILaneEx, INotifyPropertyChanged
  {
    public LaneEx(double weight, bool isPartOfCrossing = false)
    {
      this.weight = weight;
      this.IsPartOfCrossing = isPartOfCrossing;
    }

    #region Interface implementation
    public event PropertyChangedEventHandler PropertyChanged;

    protected void OnPropertyChanged([CallerMemberName]string propertyName = null)
    {
      this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    private double weight = 0;
    public double Weight
    {
      get => weight;
      set
      {
        weight = value;
        OnPropertyChanged();
      }
    }

    private Point start;
    public Point Start
    {
      get => start;
      set
      {
        start = value;
        OnPropertyChanged();
      }
    }

    private Point end;
    public Point End
    {
      get => end;
      set
      {
        end = value;
        OnPropertyChanged();
      }
    }

    private List<ILaneEx> nextLanes = new List<ILaneEx>();
    public IList<ILaneEx> NextLanes => nextLanes;

    public ILaneEx LeftLane { get; set; } = null;
    public ILaneEx RightLane { get; set; } = null;

    public bool IsPartOfCrossing { get; set; } = false;
    #endregion


    #region Overrides and operators
    public override bool Equals(object obj)
    {
      LaneEx other = obj as LaneEx;
      if (other == null)
      {
        return false;
      }
      if (ReferenceEquals(this, other))
      {
        return true;
      }
      if (this.Start == other.Start
        && this.End == other.End)
      {
        return true;
      }

      return false;
    }

    public override int GetHashCode()
    {
      return this.Start.GetHashCode() + this.End.GetHashCode();
    }

    public static bool operator==(LaneEx self, object obj)
    {
      if (ReferenceEquals(self, null) && ReferenceEquals(obj, null))
      {
        return true;
      }
      return (self as LaneEx)?.Equals(obj) ?? false;
    }

    public static bool operator!=(LaneEx self, object obj)
    {
      return !(self == obj);
    }
    #endregion
  }
}
