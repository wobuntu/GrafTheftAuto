﻿using Core.Extensions;
using Core.Interfaces;
using MapEditor.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;

namespace Core.Classes
{
  public class Tile : ITile, INotifyPropertyChanged
  {
    #region Constructors
    /// <summary>
    /// Private default constructor for serialization.
    /// </summary>
    [JsonConstructor]
    private Tile() { }

    public Tile(int mapRow, int mapColumn) : this(TileType.None, mapRow, mapColumn, 0, 0) { }

    public Tile(ITile t) : this(t.TileType, t.MapRow, t.MapColumn, t.Rotation, t.NumLanes)
    {
      bool isCrossing = t.TileType == TileType.CrossingFour;

      for (int i = 0; i < numLanes; i++)
      {
        this.LanesForward[i] = new LaneEx(t.LanesForward[i].Weight, isCrossing);
        this.LanesBackward[i] = new LaneEx(t.LanesBackward[i].Weight, isCrossing);
      }
    }

    public Tile(TileType tileType, int mapRow, int mapColumn, double rotation, int numLanes = 1)
    {
      this.TileType = tileType;
      if (tileType == TileType.None)
      {
        numLanes = 0;
        rotation = 0;
      }

      this.MapRow = mapRow;
      this.MapColumn = mapColumn;
      this.NumLanes = numLanes;
      this.Rotation = rotation;
    }
    #endregion


    #region Properties and their fields
    public int MapRow { get; private set; }
    public int MapColumn { get; private set; }

    private TileType tileType = TileType.None;
    public TileType TileType
    {
      get => tileType;
      set
      {
        tileType = value;
        if (tileType == TileType.None)
        {
          this.numLanes = 0;
        }
        else if (this.numLanes < 1)
        {
          this.numLanes = 1;
        }

        // Calling the setter for those properties again, to ensure values stay in range
        this.NumLanes = numLanes;
        this.Rotation = rotation;
        this.TrafficLightIntervalId = trafficLightIntervalId;

        updateCoords();
        OnPropertyChanged();
      }
    }

    private bool trafficLightEnabled = false;
    public bool TrafficLightEnabled
    {
      get => trafficLightEnabled;
      set
      {
        trafficLightEnabled = value;
        updateCoords();
        OnPropertyChanged();
      }
    }

    private double rotation = 0;
    public double Rotation
    {
      get => rotation;
      set
      {
        this.rotation = value;
        // Calling the setter for those properties again, to ensure values stay in range
        this.NumLanes = numLanes;

        updateCoords();
        OnPropertyChanged();
      }
    }

    private bool isCrossing;
    public bool IsCrossing
    {
      get => isCrossing;
      private set
      {
        isCrossing = value;
        OnPropertyChanged();
      }
    }

    private int trafficLightIntervalId = 0;
    public int TrafficLightIntervalId
    {
      get => trafficLightIntervalId;
      set
      {
        trafficLightIntervalId = value;
        // Calling the setter for those properties again, to ensure values stay in range
        this.NumLanes = numLanes;
        this.Rotation = rotation;

        updateCoords();
        OnPropertyChanged();
      }
    }

    private int numLanes = 1;
    public int NumLanes
    {
      get => numLanes;
      set
      {
        if (this.TileType == TileType.None)
        {
          numLanes = 0;
          this.LanesForward = null;
          this.LanesBackward = null;
          this.IsCrossing = false;
        }
        else if (this.tileType == TileType.CrossingFour)
        {
          // Only 2 or 3 starting lanes are allowed
          if (value > 3)
          {
            value = 3;
          }
          else if (value < 2)
          {
            value = 2;
          }

          this.IsCrossing = true;
          numLanes = value;
          createLanesCrossings();
        }
        else if (this.tileType == TileType.Widen || this.tileType == TileType.Tighten)
        {
          // Only one or two starting lanes are allowed
          if (value > 3)
          {
            value = 3;
          }
          else if (value < 2)
          {
            value = 2;
          }

          this.IsCrossing = false;
          numLanes = value;
          createLanesSizeChange();
        }
        else
        {
          // Correct the number of lanes for the remaining possible TileTypes
          if (value > 3)
          {
            value = 3;
          }
          else if (value < 1)
          {
            value = 1;
          }

          this.IsCrossing = false;
          numLanes = value;
          createLanesSymetric();
        }

        updateCoords();
        OnPropertyChanged();
      }
    }
    #endregion


    #region Public methods
    public void TryConnectWith(ITile other)
    {
      if (this.LanesForward == null || other?.LanesForward == null)
      {
        return;
      }

      // Allowed: left (x-1,y), right(x+1,y), top(x,y+1), bottom(x,y-1)
      if ((other.MapColumn == this.MapColumn - 1 && other.MapRow == this.MapRow)   // left
        || (other.MapColumn == this.MapColumn + 1 && other.MapRow == this.MapRow)  // right
        || (other.MapColumn == this.MapColumn && other.MapRow == this.MapRow + 1)  // top
        || (other.MapColumn == this.MapColumn && other.MapRow == this.MapRow - 1)) // bottom
      {
        // Note: Connecting mixed lane directions is allowed for crossings
        for (int i = 0; i < this.LanesForward.Count; i++)
        {
          var curLane = this.LanesForward[i];
          for (int j = 0; j < other.LanesForward.Count; j++)
          {
            var otherLane = other.LanesForward[j];
            var diff = curLane.Start - otherLane.End;
            if (diff.Length < Config.Lanes.RoundingErrorThresholdPx)
            {
              // other comes before this one
              //curLane.PreviousLanes.AddIfNotContained(otherLane);
              otherLane.NextLanes.AddIfNotContained(curLane);
              continue;
            }

            diff = curLane.End - otherLane.Start;
            if (diff.Length < Config.Lanes.RoundingErrorThresholdPx)
            {
              // other comes after this one
              curLane.NextLanes.AddIfNotContained(otherLane);
              //otherLane.PreviousLanes.AddIfNotContained(curLane);
              continue;
            }
          }

          if (!this.IsCrossing && !other.IsCrossing)
          {
            continue;
          }

          // If one of the lanes is part of a crossing, allow them to connect mixed direction types
          for (int j = 0; j < other.LanesBackward.Count; j++)
          {
            var otherLane = other.LanesBackward[j];
            var diff = curLane.Start - otherLane.End;
            if (diff.Length < Config.Lanes.RoundingErrorThresholdPx)
            {
              // other comes before this one
              //curLane.PreviousLanes.AddIfNotContained(otherLane);
              otherLane.NextLanes.AddIfNotContained(curLane);
              continue;
            }

            diff = curLane.End - otherLane.Start;
            if (diff.Length < Config.Lanes.RoundingErrorThresholdPx)
            {
              // other comes after this one
              curLane.NextLanes.AddIfNotContained(otherLane);
              //otherLane.PreviousLanes.AddIfNotContained(curLane);
            }
          }
        }
        for (int i = 0; i < this.LanesBackward.Count; i++)
        {
          var curLane = this.LanesBackward[i];
          for (int j = 0; j < other.LanesBackward.Count; j++)
          {
            var otherLane = other.LanesBackward[j];
            var diff = curLane.Start - otherLane.End;
            if (diff.Length < Config.Lanes.RoundingErrorThresholdPx)
            {
              // other comes before this one
              //curLane.PreviousLanes.AddIfNotContained(otherLane);
              otherLane.NextLanes.AddIfNotContained(curLane);
              continue;
            }

            diff = curLane.End - otherLane.Start;
            if (diff.Length < Config.Lanes.RoundingErrorThresholdPx)
            {
              // other comes after this one
              curLane.NextLanes.AddIfNotContained(otherLane);
              //otherLane.PreviousLanes.AddIfNotContained(curLane);
              continue;
            }
          }

          if (!this.IsCrossing && !other.IsCrossing)
          {
            continue;
          }

          // If one of the lanes is part of a crossing, allow them to connect mixed direction types
          for (int j = 0; j < other.LanesForward.Count; j++)
          {
            var otherLane = other.LanesForward[j];
            var diff = curLane.Start - otherLane.End;
            if (diff.Length < Config.Lanes.RoundingErrorThresholdPx)
            {
              // other comes before this one
              //curLane.PreviousLanes.AddIfNotContained(otherLane);
              otherLane.NextLanes.AddIfNotContained(curLane);
              continue;
            }

            diff = curLane.End - otherLane.Start;
            if (diff.Length < Config.Lanes.RoundingErrorThresholdPx)
            {
              // other comes after this one
              curLane.NextLanes.AddIfNotContained(otherLane);
              //otherLane.PreviousLanes.AddIfNotContained(curLane);
            }
          }
        }
      }
    }
    #endregion

    #region Lane generation helper methods
    private void createLanesSymetric()
    {
      this.LanesForward = new List<ILaneEx>();
      this.LanesBackward = new List<ILaneEx>();

      LaneEx lastForward = null;
      LaneEx lastBackward = null;

      for (int i = 1; i <= numLanes; i++)
      {
        double weight = Config.Lanes.WeightMultiplicatorPerLane * i + Config.Lanes.WeightDefault;

        // Create new lanes, use the previous ones as the right lanes
        var nextForward = new LaneEx(weight) { RightLane = lastForward };
        var nextBackward = new LaneEx(weight) { RightLane = lastBackward };

        // Set the left lanes of the previous lanes
        if (lastForward != null)
        {
          lastForward.LeftLane = nextForward;
        }
        if (lastBackward != null)
        {
          lastBackward.LeftLane = nextBackward;
        }

        // Insert the lanes in the list
        this.LanesForward.Add(nextForward);
        this.LanesBackward.Add(nextBackward);

        lastForward = nextForward;
        lastBackward = nextBackward;
      }
    }

    private void createLanesCrossings()
    {
      this.LanesBackward = new List<ILaneEx>();
      this.LanesForward = new List<ILaneEx>();

      for (int i = 1; i <= numLanes * 2; i++)
      {
        // Changing lanes is not allowed on crossings, therefore no setting of right/left lanes.
        // Also, the lanes have just their default weight, no need to distinguish here
        this.LanesForward.Add(new LaneEx(Config.Lanes.WeightDefault));
        this.LanesBackward.Add(new LaneEx(Config.Lanes.WeightDefault));
      }
    }

    private void createLanesSizeChange()
    {
      this.LanesBackward = new List<ILaneEx>();
      this.LanesForward = new List<ILaneEx>();

      LaneEx lastForward = null;
      LaneEx lastBackward = null;

      // Tighten [2] to 1 => create 4 lanes in total
      // Tighten [3] to 2 => create 8 lanes in total
      // Widen 1 to [2] => create 4 lanes in total
      // Widen 2 to [3] => create 8 lanes in total
      for (int i = 1; i <= 2 * numLanes - 2; i++)
      {
        double weight = Config.Lanes.WeightMultiplicatorPerLane * i + Config.Lanes.WeightDefault;

        // Create new lanes, use the previous ones as the right lanes
        var nextForward = new LaneEx(weight) { RightLane = lastForward };
        var nextBackward = new LaneEx(weight) { RightLane = lastBackward };

        // Set the left lanes of the previous lanes
        if (lastForward != null)
        {
          lastForward.LeftLane = nextForward;
        }
        if (lastBackward != null)
        {
          lastBackward.LeftLane = nextBackward;
        }

        // Insert the lanes in the list
        this.LanesForward.Add(nextForward);
        this.LanesBackward.Add(nextBackward);

        lastForward = nextForward;
        lastBackward = nextBackward;
      } 
    }
    #endregion


    #region Helpers to get coordinates
    private Point getStartForward(double pos, int laneIdx)
    {
      switch (this.tileType)
      {
        case TileType.Straight:
        case TileType.Tighten:
        case TileType.Widen:
          return new Point(pos, Config.UI.TileSize);
        case TileType.CurveLeft:
          return new Point(pos, Config.UI.TileSize);
        case TileType.CurveRight:
          return new Point(pos, Config.UI.TileSize);
        case TileType.CrossingFour:
          laneIdx = laneIdx - numLanes * 2;

          if (laneIdx % numLanes != 0) // All except the last lane
          {
            if (laneIdx <= numLanes) // Lanes starting from the bottom
            {
              // Straight line up
              return new Point(pos, Config.UI.TileSize);
            }
            // Lanes starting from left, straight line from left to right
            return new Point(0, pos);
          }
          else // Last lane per direction
          {
            if (laneIdx <= numLanes) // Lanes starting from the bottom
            {
              // Curve from bottom to right
              return new Point(pos, Config.UI.TileSize);
            }

            // Lanes starting from left, curve from left to bottom
            return new Point(0, pos);
          }
      }

      return new Point();
    }

    private Point getStartBackward(double pos, int laneIdx)
    {
      switch (this.tileType)
      {
        case TileType.Straight:
        case TileType.Tighten:
        case TileType.Widen:
          return new Point(pos, 0);
        case TileType.CurveLeft:
          return new Point(0, Config.UI.TileSize - pos);
        case TileType.CurveRight:
          return new Point(Config.UI.TileSize, pos);
        case TileType.CrossingFour:
          if (laneIdx % numLanes != 0) // All except the last lane
          {
            if (laneIdx <= numLanes) // Lanes starting from the top
            {
              // Straight line down
              return new Point(Config.UI.TileSize - pos, 0);
            }
            // Lanes starting from right, straight line from right to left
            return new Point(Config.UI.TileSize, Config.UI.TileSize - pos);
          }
          else // Last lane per direction
          {
            if (laneIdx <= numLanes) // Lanes starting from the top
            {
              // Curve from bottom to right
              return new Point(Config.UI.TileSize - pos, 0);
            }

            // Lanes starting from left, curve from left to bottom
            return new Point(Config.UI.TileSize, Config.UI.TileSize - pos);
          }
      }

      return new Point();
    }

    private Point getEndForward(double pos, int laneIdx)
    {
      switch (this.tileType)
      {
        case TileType.Straight:
        case TileType.Tighten:
        case TileType.Widen:
          return new Point(pos, 0);
        case TileType.CurveLeft:
          return new Point(0, Config.UI.TileSize - pos);
        case TileType.CurveRight:
          return new Point(Config.UI.TileSize, pos);
        case TileType.CrossingFour:
          laneIdx = laneIdx - numLanes * 2;

          if (laneIdx % numLanes != 0) // All except the last lane
          {
            if (laneIdx <= numLanes) // Lanes starting from the bottom
            {
              // Straight line up
              return new Point(pos, 0);
            }
            // Lanes starting from left, straight line from left to right
            return new Point(Config.UI.TileSize, pos);
          }
          else // Last lane per direction
          {
            if (laneIdx <= numLanes) // Lanes starting from the bottom
            {
              // Curve from bottom to right
              return new Point(Config.UI.TileSize, pos);
            }

            // Lanes starting from left, curve from left to bottom
            return new Point(Config.UI.TileSize - pos, Config.UI.TileSize);
          }
      }

      return new Point();
    }

    private Point getEndBackward(double pos, int laneIdx)
    {
      switch (this.tileType)
      {
        case TileType.Straight:
        case TileType.Tighten:
        case TileType.Widen:
          return new Point(pos, Config.UI.TileSize);
        case TileType.CurveLeft:
          return new Point(pos, Config.UI.TileSize);
        case TileType.CurveRight:
          return new Point(pos, Config.UI.TileSize);
        case TileType.CrossingFour:
          if (laneIdx % numLanes != 0) // All except the last lane
          {
            if (laneIdx <= numLanes) // Lanes starting from the top
            {
              // Straight line down
              return new Point(Config.UI.TileSize - pos, Config.UI.TileSize);
            }
            // Lanes starting from right, straight line from right to left
            return new Point(0, Config.UI.TileSize - pos);
          }
          else // Last lane per direction
          {
            if (laneIdx <= numLanes) // Lanes starting from the top
            {
              // Curve from bottom to right
              return new Point(0, Config.UI.TileSize - pos);
            }

            // Lanes starting from left, curve from left to bottom
            return new Point(pos, 0);
          }
      }

      return new Point();
    }

    private void addTrafficLight(Point pos, int intervalId)
    {
      if (!trafficLightEnabled)
      {
        return;
      }

      intervalId = intervalId % 2;
      var existing = this.TrafficLights.Where(x => x.IntervalId == intervalId).FirstOrDefault();
      if (existing == null)
      {
        var light = new TrafficLight() { IntervalId = intervalId };
        light.Positions.Add(pos);
        this.TrafficLights.Add(light);
      }
      else
      {
        if (!existing.Positions.Contains(pos))
        {
          existing.Positions.Add(pos);
        }
      }
    }

    private void updateCoords()
    {
      this.TrafficLights = new List<ITrafficLight>();
      if (this.LanesBackward == null || this.LanesForward == null)
      {
        return;
      }
      
      double space = Config.UI.TileSize / (numLanes * 2 + 1);
      double widthHalf = Config.UI.TileSize / 2;
      Point tileCenter = new Point(widthHalf, widthHalf);
      Point tileOffset = new Point(Config.UI.TileSize * this.MapColumn, Config.UI.TileSize * this.MapRow);

      bool isPartOfCrossing = this.tileType == TileType.CrossingFour;

      for (int i = 1; i <= this.LanesBackward.Count; i++)
      {
        // Calculate points without rotation on the tile
        double x;
        Point start;
        Point end;
        Point lightPos;
        int intervalId = this.trafficLightIntervalId;

        if (tileType == TileType.CrossingFour)
        {
          var m = i % numLanes;
          m = (m == 0) ? numLanes : m;
          var k = numLanes + 1 - m;

          x = space * (m + numLanes);
          start = getStartBackward(x, i);
          end = getEndBackward(x, i);

          //intervalId = this.trafficLightIntervalId + 1;

          if (i > this.numLanes)
          {
            intervalId = intervalId + 1;
          }

          lightPos = start;
        }
        else if (tileType == TileType.Tighten)
        {
          x = ((int)Math.Ceiling((i + 1) / 2.0)) * space;
          double spaceJunction = Config.UI.TileSize / ((numLanes - 1) * 2 + 1);
          int idxJunction = i - i/2;

          var xJunction = spaceJunction * idxJunction;
          start = getStartBackward(xJunction, i);
          end = getEndBackward(x, i);

          lightPos = end;
        }
        else if (tileType == TileType.Widen)
        {
          double spaceX = Config.UI.TileSize / ((numLanes - 1) * 2 + 1);
          int idxX = i - i / 2;
          
          x = idxX * spaceX;
          var xJunction = ((int)Math.Ceiling((i + 1) / 2.0)) * space;
          start = getStartBackward(xJunction, i);
          end = getEndBackward(x, i);

          lightPos = end;
        }
        else
        {
          x = space * i;
          start = getStartBackward(x, i);
          end = getEndBackward(x, i);

          lightPos = end;
        }

        // Rotate the point on the tile
        start = PointTransformation.RotatePoint(start, tileCenter, this.rotation);
        end = PointTransformation.RotatePoint(end, tileCenter, this.rotation);
        lightPos = PointTransformation.RotatePoint(lightPos, tileCenter, this.rotation);

        // Add the offset for the tile itself
        start.Offset(tileOffset.X, tileOffset.Y);
        end.Offset(tileOffset.X, tileOffset.Y);
        lightPos.Offset(tileOffset.X, tileOffset.Y);

        this.LanesBackward[i - 1].Start = start;
        this.LanesBackward[i - 1].End = end;
        this.LanesBackward[i - 1].IsPartOfCrossing = isPartOfCrossing;

        addTrafficLight(lightPos, intervalId);
      }

      for (int i = this.LanesForward.Count + 1; i <= this.LanesForward.Count + this.LanesBackward.Count; i++)
      {
        // Same steps as above, but for the lanes in the other direction and with other coordinates etc
        double x;
        Point start;
        Point end;
        Point lightPos;

        int intervalId = this.trafficLightIntervalId;

        if (tileType == TileType.CrossingFour)
        {
          var m = i % numLanes;
          m = (m == 0) ? numLanes : m;
          var k = numLanes + 1 - m;

          x = space * (m + numLanes);
          start = getStartForward(x, i);
          end = getEndForward(x, i);

          lightPos = start;

          if (i > this.numLanes + this.LanesForward.Count)
          {
            intervalId = intervalId + 1;
          }
        }
        else if (tileType == TileType.Tighten)
        {
          x = ((int)Math.Ceiling((i + 1) / 2.0)) * space + space;
          double spaceJunction = Config.UI.TileSize / ((numLanes - 1) * 2 + 1);
          int idxJunction = i - i / 2;

          var xJunction = spaceJunction * idxJunction;
          start = getStartForward(x, i);
          end = getEndForward(xJunction, i);

          lightPos = start;
        }
        else if (tileType == TileType.Widen)
        {
          double spaceX = Config.UI.TileSize / ((numLanes - 1) * 2 + 1);
          int idxX = i - i / 2;

          x = idxX * spaceX;
          var xJunction = ((int)Math.Ceiling((i + 1) / 2.0)) * space + space;
          start = getStartForward(x, i);
          end = getEndForward(xJunction, i);

          lightPos = start;
        }
        else
        {
          x = space * i;
          start = getStartForward(x, i);
          end = getEndForward(x, i);

          lightPos = start;
        }

        start = PointTransformation.RotatePoint(start, tileCenter, this.rotation);
        end = PointTransformation.RotatePoint(end, tileCenter, this.rotation);
        lightPos = PointTransformation.RotatePoint(lightPos, tileCenter, this.rotation);

        start.Offset(tileOffset.X, tileOffset.Y);
        end.Offset(tileOffset.X, tileOffset.Y);
        lightPos.Offset(tileOffset.X, tileOffset.Y);

        int idx = (this.LanesForward.Count + this.LanesBackward.Count) - i;
        this.LanesForward[idx].Start = start;
        this.LanesForward[idx].End = end;
        this.LanesForward[idx].IsPartOfCrossing = isPartOfCrossing;
        
        addTrafficLight(lightPos, intervalId);
      }
    }
    #endregion


    #region Interface implementations
    public event PropertyChangedEventHandler PropertyChanged;

    protected void OnPropertyChanged([CallerMemberName]string propertyName = null)
    {
      this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    public IList<ILaneEx> LanesForward { get; private set; }

    public IList<ILaneEx> LanesBackward { get; private set; }

    public IList<ITrafficLight> TrafficLights { get; private set; } = new List<ITrafficLight>();
    #endregion
  }
}
