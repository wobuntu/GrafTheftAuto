﻿using System.IO;
using System.Linq;
using Core.Interfaces;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;
using Core.Extensions;
using System.Windows;
using System.Runtime.Serialization;

namespace Core.Classes
{
  public class Map : INotifyPropertyChanged
  {
    #region Constructors
    /// <summary>
    /// Private default constructor for serialization.
    /// </summary>
    [Newtonsoft.Json.JsonConstructor]
    private Map() { }

    public Map(int height, int width)
    {
      this.tiles = new List<List<ITile>>();
      for (int h = 0; h < height; h++)
      {
        List<ITile> row = new List<ITile>();
        tiles.Add(row);
        for (int w = 0; w < width; w++)
        {
          var tile = new Tile(h, w);
          tile.PropertyChanged += tileChanged;
          row.Add(tile);
        }
      }

      this.GridHeight = height;
      this.GridWidth = width;
    }
    #endregion

    #region PropertyChanged related
    private void removePropertyChangedHandlers()
    {
      if (tiles == null || tiles.Count == 0)
      {
        return;
      }

      // Remove old handlers if there are any
      for (int h = 0; h < this.gridHeight; h++)
      {
        var row = tiles[h];
        for (int w = 0; w < this.GridWidth; w++)
        {
          var tile = row[w] as Tile;
          tile.PropertyChanged -= tileChanged;
        }
      }
    }

    private void addPropertyChangedHandlers()
    {
      if (tiles == null || tiles.Count == 0)
      {
        return;
      }

      // Add new handlers
      for (int h = 0; h < this.gridHeight; h++)
      {
        var row = tiles[h];
        for (int w = 0; w < this.GridWidth; w++)
        {
          var tile = row[w] as Tile;
          tile.PropertyChanged += tileChanged;
        }
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;
    protected void OnPropertyChanged([CallerMemberName]string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    private void tileChanged(object sender, PropertyChangedEventArgs args)
    {
      clearConnections();
      reconnectTiles();
    }
    #endregion

    #region Properties and their fields
    private List<List<ITile>> tiles;
    public List<List<ITile>> Tiles
    {
      get => tiles;
      private set
      {
        removePropertyChangedHandlers();
        this.tiles = value;

        if (value == null || value.Count == 0)
        {
          return;
        }

        this.gridHeight = this.tiles.Count;
        this.gridWidth = this.tiles[0].Count;
        addPropertyChangedHandlers();
        clearConnections();
        reconnectTiles();

        OnPropertyChanged(nameof(Tiles));
        OnPropertyChanged(nameof(GridWidth));
        OnPropertyChanged(nameof(GridHeight));
      }
    }

    private int gridWidth = 1;
    public int GridWidth
    {
      get => gridWidth;
      private set
      {
        if (value < 1)
        {
          value = 1;
        }

        gridWidth = value;
        OnPropertyChanged();
      }
    }

    private int gridHeight = 1;
    public int GridHeight
    {
      get => gridHeight;
      private set
      {
        if (value < 1)
        {
          value = 1;
        }

        gridHeight = value;
        OnPropertyChanged();
      }
    }

    private bool isValid = false;
    [JsonIgnore]
    public bool IsValid
    {
      get => isValid;
      private set
      {
        isValid = value;
        OnPropertyChanged();
      }
    }

    private Point invalidTileIndex = new Point(-1, -1);
    [JsonIgnore]
    public Point InvalidTileIndex
    {
      get => invalidTileIndex;
      private set
      {
        if (value == invalidTileIndex)
        {
          return;
        }

        invalidTileIndex = value;
        OnPropertyChanged();
      }
    }

    [JsonIgnore]
    public ITile this[int h, int w]
    {
      get
      {
        if (h < 0 || h >= this.GridHeight)
        {
          return null;
        }
        if (w < 0 || w >= this.GridWidth)
        {
          return null;
        }
        return this.Tiles[h][w];
      }
      set
      {
        Tile newTile;
        Tile oldTile = this.Tiles[h][w] as Tile;
        if (oldTile != null)
        {
          oldTile.PropertyChanged -= tileChanged;
        }

        if (value == null)
        {
          newTile = new Tile(h, w);
          newTile.PropertyChanged += tileChanged;
          this.Tiles[h][w] = newTile;
        }
        else
        {
          newTile = new Tile(value);
          newTile.PropertyChanged += tileChanged;
          this.Tiles[h][w] = newTile;
        }

        clearConnections();
        reconnectTiles();
      }
    }
    #endregion

    #region Helpers
    private void clearConnections()
    {
      var allTiles = this.Tiles.SelectMany(x => x);
      var toClear = allTiles.Where(x => x.LanesBackward != null).SelectMany(x => x.LanesBackward);
      toClear = toClear.Concat(allTiles.Where(x=>x.LanesForward != null).SelectMany(x => x.LanesForward));

      foreach (var item in toClear)
      {
        item.NextLanes.Clear();
      }
    }

    private void reconnectTiles()
    {
      double maxWidth = this.GridWidth * Config.UI.TileSize;
      double maxHeight = this.GridHeight * Config.UI.TileSize;
      
      for (int h = 0; h < this.GridHeight; h++)
      {
        for (int w = 0; w < this.GridWidth; w++)
        {
          ITile currentTile = this[h, w];

          ITile left = this[h, w - 1];
          ITile right = this[h, w + 1];
          ITile top = this[h - 1, w];
          ITile bottom = this[h + 1, w];

          left?.TryConnectWith(currentTile);
          right?.TryConnectWith(currentTile);
          top?.TryConnectWith(currentTile);
          bottom?.TryConnectWith(currentTile);
        }
      }

      // Check if the tiles are all properly connected
      foreach (var item in this.GetSnapshotOfLanes())
      {
        if (item.End.X == 0 || item.End.Y == 0 || item.End.X == maxWidth || item.End.Y == maxHeight)
        {
          // Ignore all lanes which end at the map borders
          continue;
        }

        if (item.NextLanes == null || item.NextLanes.Count == 0)
        {
          int xIdx = (int)((item.Start.X + item.End.X) / (2 * Config.UI.TileSize));
          int yIdx = (int)((item.Start.Y + item.End.Y) / (2 * Config.UI.TileSize));

          IsValid = false;
          this.InvalidTileIndex = new Point(xIdx, yIdx);

          return;
        }
      }

      this.IsValid = true;
      this.invalidTileIndex = new Point(-1, -1);
    }
    #endregion

    #region Public methods
    /// <summary>
    /// Returns all trafic lights of the <see cref="Map"/>.
    /// </summary>
    public List<TrafficLight> GetAllTrafficLights()
    {
      return this.Tiles.SelectMany(x => x).SelectMany(x => x.TrafficLights).OfType<TrafficLight>().ToList();
    }

    public IEnumerable<LaneEx> GetSnapshotOfLanes()
    {
      if (tiles == null)
        yield break;

      var allLanesBackward = this.Tiles.SelectMany(x => x).Where(x => x.LanesBackward != null).SelectMany(x => x.LanesBackward);
      var allLanesForward = this.Tiles.SelectMany(x => x).Where(x => x.LanesForward != null).SelectMany(x => x.LanesForward);
      foreach (var item in allLanesForward.Concat(allLanesBackward).OfType<LaneEx>())
      {
        yield return item;
      }
    }

    public IEnumerable<LaneEx> GetMapEntryLanes()
    {
      double maxWidth = this.GridWidth * Config.UI.TileSize;
      double maxHeight = this.GridHeight * Config.UI.TileSize;

      foreach (var item in GetSnapshotOfLanes())
      {
        // Round values to accuracy of the threshold pixels for comparison
        if ( item.Start.X.RoundToStep() == 0
          || item.Start.Y.RoundToStep() == 0
          || item.Start.X.RoundToStep() == maxWidth
          || item.Start.Y.RoundToStep() == maxHeight)
        {
          yield return item;
        }
      }
    }

    public IEnumerable<LaneEx> GetMapExitLanes()
    {
      double maxWidth = this.GridWidth * Config.UI.TileSize;
      double maxHeight = this.GridHeight * Config.UI.TileSize;

      foreach (var item in GetSnapshotOfLanes())
      {
        // Round values to accuracy of the threshold pixels for comparison
        if ( item.End.X.RoundToStep() == 0
          || item.End.Y.RoundToStep() == 0
          || item.End.X.RoundToStep() == maxWidth
          || item.End.Y.RoundToStep() == maxHeight)
        {
          yield return item;
        }
      }
    }
    #endregion


    #region Serialization/Deserialization
    [OnDeserialized]
    internal void OnDeserialized(StreamingContext context)
    {
      // There's a bug somewhere which causes the traffic lights to multiply after deserialization, the following is a quick and dirty for it
      // Remove Traffic Light Duplicates
      var allTilesWithTrafficLights = this.tiles.SelectMany(x => x).Where(x => x.TrafficLights != null);
      foreach (var tile in allTilesWithTrafficLights)
      {
        List<int> uniqueIds = new List<int>();
        for (int i = tile.TrafficLights.Count - 1; i >= 0; i--)
        {
          var item = tile.TrafficLights[i];
          if (uniqueIds.Contains(item.IntervalId))
          {
            tile.TrafficLights.RemoveAt(i);
          }
          else
          {
            uniqueIds.Add(item.IntervalId);
          }
        }
      }

      // Remove Lane duplicates
      var allTiles = this.tiles.SelectMany((x) => x);
      foreach (var tile in allTiles)
      {
        List<ILaneEx> uniqueLanes;
        if (tile.LanesForward != null)
        {
          uniqueLanes = new List<ILaneEx>();
          for (int i = tile.LanesForward.Count - 1; i >= 0; i--)
          {
            var lane = tile.LanesForward[i];
            if (uniqueLanes.Contains(lane))
            {
              tile.LanesForward.RemoveAt(i);
            }
            else
            {
              uniqueLanes.Add(lane);
            }
          }
        }

        if (tile.LanesBackward != null)
        {
          uniqueLanes = new List<ILaneEx>();
          for (int i = tile.LanesBackward.Count - 1; i >= 0; i--)
          {
            var lane = tile.LanesBackward[i];
            if (uniqueLanes.Contains(lane))
            {
              tile.LanesBackward.RemoveAt(i);
            }
            else
            {
              uniqueLanes.Add(lane);
            }
          }
        }
      }
    }

    static Newtonsoft.Json.JsonSerializerSettings serializerSettings = new Newtonsoft.Json.JsonSerializerSettings()
    {
      TypeNameHandling = Newtonsoft.Json.TypeNameHandling.All,
      Formatting = Newtonsoft.Json.Formatting.Indented,
      ContractResolver = new Core.Serialization.Helpers.JsonHiddenPropResolver(),
      ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Serialize,
      PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects
    };

    public static Map TryDeserializeFrom(string path)
    {
      try
      {
        string data = File.ReadAllText(path);
        Map map = Newtonsoft.Json.JsonConvert.DeserializeObject<Map>(data, serializerSettings);

        return map;
      }
      catch
      {
        return null;
      }
    }

    public static bool TrySerializeTo(Map map, string path)
    {
      try
      {
        string data = Newtonsoft.Json.JsonConvert.SerializeObject(map, serializerSettings);

        File.WriteAllText(path, data);
        return true;
      }
      catch
      {
        return false;
      }
    }
    #endregion
  }
}