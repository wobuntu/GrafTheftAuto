﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Classes
{
  public enum TileType
  {
    None,
    CrossingFour,
    // CrossingThree,
    Straight,
    CurveLeft,
    CurveRight,
    Widen,
    Tighten
  }
}
