﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Classes
{
  /// <summary>
  /// A state which a traffic light can have.
  /// </summary>
  public enum TrafficLightState
  {
    Error = 0,
    Red = 1,
    Green = 2
  };
}
