﻿using Core.Interfaces;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace Core.Classes
{
  public class TrafficLight : ITrafficLight, INotifyPropertyChanged
  {
    public IList<Point> Positions { get; set; } = new List<Point>();

    private TrafficLightState state;

    public event PropertyChangedEventHandler PropertyChanged;
    protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
    {
      this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    private int intervalId;
    public int IntervalId
    {
      get => intervalId;
      set
      {
        // Interval id may be 0 or 1
        intervalId = value % 2;
        OnPropertyChanged();
      }
    }

    public TrafficLightState State
    {
      get => state;
      set
      {
        state = value;
        OnPropertyChanged();
      }
    }
  }
}
