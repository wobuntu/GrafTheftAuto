﻿using Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Core.Classes
{
  public class VehicleWrapperDto
  {
    public VehicelType VehicleType { get; set; }
    public double Velocity { get; set; }
    public double Delta { get; set; }
    public double Length { get; set; }
    public double Width { get; set; }
    public double Rotation { get; set; }
    public Point PositionOnGrid { get; set; }
    public Guid Id { get; set; }
  }
}
