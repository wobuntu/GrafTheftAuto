﻿using System.Reflection;
using Newtonsoft.Json;

namespace Core.Serialization.Helpers
{
    public class JsonHiddenPropResolver : Newtonsoft.Json.Serialization.DefaultContractResolver
    {
        // With help of: https://stackoverflow.com/questions/4066947/private-setters-in-json-net
        protected override Newtonsoft.Json.Serialization.JsonProperty CreateProperty(
            MemberInfo member, MemberSerialization memberSerialization)
        {
            var prop = base.CreateProperty(member, memberSerialization);

            if (!prop.Writable)
            {
                var property = member as PropertyInfo;
                if (property != null)
                {
                    prop.Writable = property.GetSetMethod(true) != null;
                    prop.Readable = property.GetGetMethod(true) != null;
                }
            }

            return prop;
        }
    }
}
