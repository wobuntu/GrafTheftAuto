﻿using System;
using Core.Interfaces;
using System.Collections.Generic;
using Core.Logging;

namespace Core
{
  public static class Config
  {
    public static class FeedOptions
    {
      // By default, none of the programs show debug output anymore with this flag
      public const LogLvl DefaultLogLvl = LogLvl.All & ~LogLvl.Debug;
    }

    public static class NetworkDiscovery
    {
      public static Dictionary<string, UInt16> Ports = new Dictionary<string, ushort>()
      {
        { nameof(IMapProviderService), 23451 },
        { nameof(ITrafficLightService), 23452 },
        { nameof(ISimulation), 23453 },
        { nameof(IVehiclePool), 23454}
      };
      public static int BufferSizeDiscoveryResponse = 1024;
      public static int ReceiveTimeout = 200;
    }

    public static class Lanes
    {
      public const double WeightDefault = 0.1;
      public const double WeightMultiplicatorPerLane = 0.3;
      public const double RoundingErrorThresholdPx = 3;
    }

    public static class UI
    {
      public static double TileSize = 128;
      public static double TileBorderWidth = 2;
      public static int MaxTilesX = 20;
      public static int MaxTilesY = 10;
      public static int MillisecondsUntilAdd = 2500;
    }
  }
}