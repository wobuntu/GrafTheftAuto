﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Core.Logging;


namespace Core.ComponentDiscovery
{
  public class DiscoveryRegistration : IDisposable
  {
    #region Public creation methods
    public static DiscoveryRegistration Create(string registrationName, UInt16 listenerPort)
    {
      if (registrationName == null)
      {
        throw new ArgumentNullException(nameof(registrationName), "The registration name must not be null.");
      }
      if ((registrationName.Length + 36) * 2 > Config.NetworkDiscovery.BufferSizeDiscoveryResponse)
      {
        // 36 ... length of a guid string, *2 because of the encoding
        throw new Exception($"The registration name is too long, it must be representable by " +
            $"{Config.NetworkDiscovery.BufferSizeDiscoveryResponse - 36 * 2} bytes.");
      }

      return new DiscoveryRegistration(registrationName, listenerPort);
    }
    #endregion


    #region Private Ctor and fields
    private DiscoveryRegistration(string registrationName, UInt16 listenerPort)
    {
      this.registrationName = registrationName;
      this.listenerPort = listenerPort;
      startAsyncListener();
    }

    private string registrationName = null;
    private UInt16 listenerPort = 0;
    UdpClient serverInstance = null;
    Task listenerTask = null;
    CancellationTokenSource cancellationTokenSource = null;
    #endregion


    #region Listener
    private void startAsyncListener()
    {
      if (listenerTask != null && !listenerTask.IsCompleted)
      {
        return;
      }

      cancellationTokenSource = new CancellationTokenSource();

      listenerTask = new Task(async () =>
      {
        Feed.Report($"{nameof(DiscoveryRegistration)} was done (registration name = '{registrationName}', port = {listenerPort}), " +
                  $"accepting requests for it from now on.");

        while (true)
        {
          try
          {
            serverInstance = new UdpClient(listenerPort);
          }
          catch
          {
            Feed.Report($"Could not listen on the udp port {listenerPort}, it might be in use...", LogLvl.Warn);
            await Task.Delay(100);
            continue;
          }

          var clientEp = new IPEndPoint(IPAddress.Any, 0);

          var incomingRequestData = serverInstance.Receive(ref clientEp);
          var incomingRequestStr = Encoding.Unicode.GetString(incomingRequestData);

          Feed.Report($"Received discovery request from {clientEp.Address}: '{incomingRequestStr}'", LogLvl.Debug);

          if (incomingRequestStr.Length > 36 && incomingRequestStr.Substring(36) == registrationName)
          {
            Feed.Report($"Incoming discovery request matches the registered component, sending answer back.", LogLvl.Debug);

                  // We answer simply by sending our component name back, together with a guid to ensure we can remove responses
                  // which were send from ourself from the client side
                  string responseStr = Guid.NewGuid() + registrationName;

            byte[] response = Encoding.Unicode.GetBytes(responseStr);
            serverInstance.Send(response, response.Length, clientEp);
          }

          serverInstance.Close();
        }
      }, cancellationTokenSource.Token);

      listenerTask.ConfigureAwait(false);
      listenerTask.Start();
    }

    private void stopAsyncListener()
    {
      if (listenerTask == null || cancellationTokenSource == null)
      {
        return;
      }

      cancellationTokenSource.Cancel();
    }
    #endregion


    #region IDisposable implementation
    private bool disposed = false;
    public void Dispose()
    {
      if (disposed)
      {
        throw new Exception($"This {nameof(DiscoveryRegistration)} was already disposed.");
      }

      Feed.Report($"Disposing the listener for '{registrationName}' on port {listenerPort}.");
      if (listenerTask != null && cancellationTokenSource != null && !listenerTask.IsCompleted)
      {
        cancellationTokenSource.Cancel();
      }

      disposed = true;
    }
    #endregion
  }
}
