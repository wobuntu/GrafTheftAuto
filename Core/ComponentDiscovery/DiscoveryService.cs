﻿using System;
using System.Net;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using Core.Logging;

namespace Core.ComponentDiscovery
{
  public static class DiscoveryService
  {
    #region Discovery methods
    public static IEnumerable<DiscoveryResult> Find(String registrationName, UInt16 componentPort, int receiveTimeoutMillis)
    {
      // Thanks to: https://stackoverflow.com/questions/22852781/how-to-do-network-discovery-using-udp-broadcast
      IEnumerable<DiscoveryResult> found = new List<DiscoveryResult>();
      Guid ownSenderGuid = Guid.NewGuid();

      Feed.Report($"Searching for '{registrationName}' within the local network (port: {componentPort})...");

      // Target for sending the discovery broadcast
      IPEndPoint localEpBroadcast = new IPEndPoint(IPAddress.Broadcast, componentPort);

      NetworkInterface[] networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();

      foreach (var adapter in networkInterfaces)
      {
        if (/*adapter.NetworkInterfaceType == NetworkInterfaceType.Loopback
                    ||*/ !adapter.Supports(NetworkInterfaceComponent.IPv4)
            || adapter.OperationalStatus != OperationalStatus.Up)
        {
          // We only broadcast to all sorts of devices (ethernet, wireless, etc.) which support IPv4 and which are online
          continue;
        }

        found = found.Concat(findByAdapter(adapter, localEpBroadcast, ownSenderGuid, registrationName, receiveTimeoutMillis));
      }

      var deleteCached = CachedResults.Where((x) => x.ComponentName == registrationName);
      CachedResults = CachedResults.Except(deleteCached).Concat(found);

      Feed.Report($"Found {found.Count()} matches for '{registrationName}'.");

      return found;
    }

    private static List<DiscoveryResult> findByAdapter(
        NetworkInterface adapter, IPEndPoint localEpBroadcast,
        Guid ownSenderGuid, string registrationName, int timeoutMillis)
    {
      List<DiscoveryResult> found = new List<DiscoveryResult>();
      string requestStr = ownSenderGuid + registrationName;
      var request = Encoding.Unicode.GetBytes(requestStr);

      IPInterfaceProperties adapterProperties = adapter.GetIPProperties();
      foreach (var prop in adapterProperties.UnicastAddresses)
      {
        if (prop.Address.AddressFamily == AddressFamily.InterNetwork)
        {
          // Create the socket for sending the broadcast on the network interface
          Socket bcSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
          bcSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
          bcSocket.ReceiveTimeout = timeoutMillis;

          // Bind to the current adapter
          IPEndPoint myLocalEndPoint = new IPEndPoint(prop.Address, localEpBroadcast.Port);
          try
          {
            bcSocket.Bind(myLocalEndPoint);
          }
          catch
          {
            continue;
          }

          // Send the broadcast and receive the answer
          bcSocket.SendTo(request, localEpBroadcast);

          byte[] bufferAnswer = new byte[Config.NetworkDiscovery.BufferSizeDiscoveryResponse];
          do
          {
            try
            {
              EndPoint serverEp = new IPEndPoint(IPAddress.Any, 0);
              int lg = bcSocket.ReceiveFrom(bufferAnswer, ref serverEp);

              if (lg < 36 * 2)
              {
                // Guid has 36 chars, which are 72 bytes in unicode
                continue;
              }

              string received = Encoding.Unicode.GetString(bufferAnswer, 0, lg);
              Guid senderGuid = Guid.Parse(received.Substring(0, 36));

              if (senderGuid == ownSenderGuid)
              {
                // Filter the sent broadcast message away
                continue;
              }

              received = received.Substring(36);

              if (received == registrationName)
              {
                DiscoveryResult discoveryResult = new DiscoveryResult((serverEp as IPEndPoint)?.Address, registrationName);
                found.Add(discoveryResult);
              }
            }
            catch { break; }

          } while (bcSocket.ReceiveTimeout != 0); //fixed receive timeout for each adapter that supports our broadcast
          bcSocket.Close();
        }
      }

      return found;
    }

    public static IEnumerable<DiscoveryResult> CachedResults { get; private set; } = new List<DiscoveryResult>();
    #endregion
  }
}
