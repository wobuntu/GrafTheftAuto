﻿using System.Net;

namespace Core.ComponentDiscovery
{
    public class DiscoveryResult
    {
        public DiscoveryResult(IPAddress address, string componentName)
        {
            this.Address = address;
            this.ComponentName = componentName;
        }

        public IPAddress Address { get; set; }
        public string ComponentName { get; set; }
    }
}
