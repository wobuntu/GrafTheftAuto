const express = require('express');
const http = require('http');
const opn = require('opn');

let app = express();

app.use(express.static('dist'));
 
app.get('/', function(request, response){
    response.sendFile(__dirname + '/view.html');
});

app.listen(3000, function(){
    console.log('Server running at port 3000: http://127.0.0.1:3000');
});

opn('http://127.0.0.1:3000', {app: 'chrome'});
