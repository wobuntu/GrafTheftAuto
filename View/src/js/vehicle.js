const Utils = require('./utilities');

/*
Car = 0,
Truck = 1,
Bus = 2,
Motorbike = 3,
Tractor = 4
*/

module.exports = class Vehicle {

	constructor(id, params) {
		this.id = id;
		this._sprite = null;
		
		this.vehicleType = params.VehicleType;
		this.length = params.Length;
		this.width = params.width;

		let variantNum = id.match(/^\d+|\d+\b|\d+(?=\w)/g).map(function (v) {return +v;})[0];
		let vehicleTypeText = this.getTypeString();
		let vehicleVariant = this.getVariant(variantNum);

		let texture = this._getTexture(vehicleTypeText, vehicleVariant);

		if(texture) {
			let vehicleSprite = new PIXI.Sprite(texture);
			vehicleSprite.anchor.set(0.5);

			this._sprite = vehicleSprite;

			// Opt-in to interactivity
	        this._sprite.interactive = true;

	        // Shows hand cursor
	        this._sprite.buttonMode = true;

	        // Pointers normalize touch and mouse
	        this._sprite.on('pointerdown', () => this._onClick());
		}

		this.setParameters(params);
	}

	getTypeString() {
		switch(this.vehicleType) {
			case 0:
			case 2:
			case 4:
				return "car";
			case 1:
				return "truck";
			case 3:
				return "bike";
			default:
				return "car";
		}
	}

	getVariant(salt) {

		let saltInt = parseInt(salt, 10);

		if(isNaN(saltInt)) {
			return 1;
		}

		switch(this.getTypeString()) {
			case "car":
				return saltInt % 5 + 1;
			case "truck":
				return 1;
			case "bike":
				return saltInt % 3 + 1;
			default:
				return 1;
		}
	}

	setParameters(parameters) {

		this.delta = parameters.Delta;
		this.velocity = parameters.Velocity;

		this.setPosition(parameters.PositionOnGrid);
		this.setRotation(parameters.Rotation);
	}

	setPosition(posOnGrid) {

		let point = posOnGrid.split(',');

		this.x = Math.floor(point[0]);
		this.y = Math.floor(point[1]);

		this._sprite.position.x = this.x;
		this._sprite.position.y = this.y;
	}

	setRotation(rotation) {
		this.rotation = Math.round(rotation / 45) * 45;

		this._sprite.rotation = Utils.degToRad(this.rotation);
	}

	getSprite() {
		return this._sprite;
	}

	_getTexture(type, variant) {

		if(!["car", "truck", "bike"].includes(type)) {
			type = "car";
		}

		let textureIdentifier = type;
		textureIdentifier += "_";
		textureIdentifier += variant;

		if(PIXI.loader.resources[textureIdentifier]) {
			return PIXI.loader.resources[textureIdentifier].texture;			
		}

		return PIXI.loader.resources.car_one_black.texture;
	}

	_onClick() {

		let honkSound = 'honk_';

		let honkNum = Math.round(Math.random() + 1);

		honkSound += honkNum;

		PIXI.sound.play(honkSound);
	}
}