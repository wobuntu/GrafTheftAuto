const Config = require('./config');
const Utils = require('./utilities');
const MapTile = require('./mapTile');

module.exports = class Map {
	
	constructor(mapContainer, roadMap) {

		this.mapContainer = mapContainer;

    	this.tileSizeX = Config.tileSize;
		this.tileSizeY = Config.tileSize;

		let roadHeight = roadMap.GridHeight * this.tileSizeY;
		let roadWidth = roadMap.GridWidth * this.tileSizeX;

    	this._width = roadWidth + 1;
		this._height = roadHeight + 1;

		this.drawGroundTilingSprite();

		this.drawMapTiles(roadMap.Tiles);
	}

	drawGroundTilingSprite() {
		let texture = PIXI.loader.resources.grass.texture;

		var tilingSprite = new PIXI.extras.TilingSprite(texture, this._width, this._height);

		this.mapContainer.addChild(tilingSprite);
	}

	drawMapTiles(mapData) {

		let rows = mapData['$values'];

		rows.forEach((mapRow) => {
			this._drawRow(mapRow);
		});

	}

	_drawRow(mapRow) {
		let cols = mapRow['$values'];

		cols.forEach((mapCol) => {
			this._drawCol(mapCol);
		})
	}

	_drawCol(mapCol) {

		let tile = new MapTile(mapCol.TileType, mapCol);

		tile.draw(this.mapContainer);

	}

	_getSpriteForRoadTile(roadTileType) {
		if(roadTileType <= 0) {
			return null;
		}

		let texture = this._getTextureForRoadTile(roadTileType);
		let roadSprite = new PIXI.Sprite(texture);
		roadSprite.anchor.set(0.5);

		if(roadTileType === 5) {
			roadSprite.rotation = Utils.degToRad(270);
		}

		switch(roadTileType) {
			case 2:
				roadSprite.rotation = Utils.degToRad(90);
				break;
			case 3:
				roadSprite.rotation = Utils.degToRad(180);
				break;
			case 4:
				roadSprite.rotation = Utils.degToRad(270);
				break;
			case 5:
				roadSprite.rotation = Utils.degToRad(90);
				break;
			case 11:
				roadSprite.rotation = Utils.degToRad(270);
				break;
			case 13:
				roadSprite.rotation = Utils.degToRad(90);
				break;
			case 14:
				roadSprite.rotation = Utils.degToRad(180);
				break;
		}

		return roadSprite;
	}

	_getTextureForRoadTile(roadTileType) {
		if(roadTileType <= 0) {
			return null;
		}

		if(roadTileType >= 5 && roadTileType < 10) {
			return PIXI.Texture.fromImage("static/img/sprites/terrain/tiles/road_single_straight.png");
		}

		if(roadTileType >= 10 && roadTileType < 20) {
			return PIXI.Texture.fromImage("static/img/sprites/terrain/tiles/road_single_junction_one.png");
		}

		if(roadTileType >= 20) {
			return PIXI.Texture.fromImage("static/img/sprites/terrain/tiles/road_single_junction_all.png");
		}

		return PIXI.Texture.fromImage("static/img/sprites/terrain/tiles/road_single_turn.png");
	    
	}
}
