const Config = require('./config');

module.exports = class TrafficLightGroup {
	
	constructor(intervalId, positions, state) {
		
		this.intervalId = intervalId;
		this.positions = positions;
		this.state = state;
		this.gfxHolder = [];

		this._buildGraphics();
	}

	_buildGraphics() {

		const renderScaling = Config.tileSize / Config.serverTileSize;

		this.positions.forEach((positionTuple) => {
			let point = positionTuple.split(',');
			let x = (point[0] * renderScaling);
			let y = (point[1] * renderScaling);

			let graphics = new PIXI.Graphics();
			graphics = this._draw(x, y, this.state, graphics);

			let gfxObj = {
				'x': x,
				'y': y,
				'gfx': graphics
			}

			this.gfxHolder.push(gfxObj);
		});
	}

	_draw(x, y, state, gfx) {

		gfx.clear();

		if(state) {

			if(state === 1) {
				gfx.beginFill(0xFF0000);
			} else if (state === 2){
				gfx.beginFill(0x00FF00);
			} else {
				gfx.beginFill(0xf9eb1d);
			}

		} else {
			gfx.beginFill(0xf9eb1d);
		}

		gfx.lineStyle(1, 0x000000, 1);

		gfx.drawCircle(x, y, 10);

		gfx.endFill();

		return gfx;
	}

	setState(newState) {
		this.state = newState;

		this.getGfx().forEach((gfxObj) => {
			this._draw(gfxObj.x, gfxObj.y, newState, gfxObj.gfx)
		});
	}

	getGfx() {
		return this.gfxHolder;
	}


}