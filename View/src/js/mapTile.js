const Utils = require('./utilities');

class AbstractMapTile {
	constructor(tileType, tileOptions) {

		this._type = tileType;

		this._row = tileOptions.MapRow;
		this._col = tileOptions.MapColumn;

		this._numLanes = tileOptions.NumLanes;
		this._rotation = tileOptions.Rotation;

		this._buildTextureStore();
	}

	getSprite() {
		let texture = this._getTextureForRoadTile();

		if(texture) {
			let roadSprite = new PIXI.Sprite(texture);

			roadSprite.anchor.set(0.5);
			roadSprite.rotation = this._getRotation();

			return roadSprite;
		}

		return null;
	}

	_buildTextureStore() {
		let textures = {};

		textures['oneLane'] = {};
		textures.oneLane.straight = PIXI.loader.resources.oneLaneStraight;
		textures.oneLane.curve = PIXI.loader.resources.oneLaneCurve;
		textures.oneLane.widen = PIXI.loader.resources.oneToTwoLaneWiden;
		textures.oneLane.tighten = PIXI.loader.resources.oneToTwoLaneWiden;
		textures.oneLane.crossing = PIXI.loader.resources.oneLaneCrossing;

		textures['twoLanes'] = {};
		textures.twoLanes.straight = PIXI.loader.resources.twoLaneStraight;
		textures.twoLanes.curve = PIXI.loader.resources.twoLaneCurve;
		textures.twoLanes.widen = PIXI.loader.resources.oneToTwoLaneWiden;
		textures.twoLanes.tighten = PIXI.loader.resources.oneToTwoLaneWiden;
		textures.twoLanes.crossing = PIXI.loader.resources.twoLaneCrossing;

		textures['threeLanes'] = {};
		textures.threeLanes.straight = PIXI.loader.resources.threeLaneStraight;
		textures.threeLanes.curve = PIXI.loader.resources.threeLaneCurve;
		textures.threeLanes.widen = PIXI.loader.resources.twoToThreeLaneWiden;
		textures.threeLanes.tighten = PIXI.loader.resources.twoToThreeLaneWiden;
		textures.threeLanes.crossing = PIXI.loader.resources.threeLaneCrossing;

		this._textureStore = textures;
	}

	_getTextureForRoadTile() {

		let laneCountIdentifier = this._getLaneCountString();
		let tileTypeIdentifier = this._getTileTypeString();

		if(laneCountIdentifier && tileTypeIdentifier) {
			if(this._textureStore[laneCountIdentifier] && this._textureStore[laneCountIdentifier][tileTypeIdentifier]) {
				return this._textureStore[laneCountIdentifier][tileTypeIdentifier].texture;
			}
		}

		return null;
	}

	_getTileTypeString() {

		let tileType;

		switch(this._type) {
			case 0:
				tileType = null;
				break;
			case 2:
				tileType = 'straight';
				break;
			case 1: 
				tileType = 'crossing';
				break;
			case 3:
			case 4:
				tileType = 'curve';
				break;
			case 5:
				tileType = 'widen';
				break;
			case 6:
				tileType = 'tighten';
				break;
			default:
				tileType = 'straight';
				break;
		}

		return tileType;
	}

	_getLaneCountString() {

		let laneCount;

		laneCount = 'oneLane';
		switch(this._numLanes) {
			case 3:
				laneCount = 'threeLanes';
				break;
			case 2:
				laneCount = 'twoLanes';
				break;
			default:
				laneCount = 'oneLane';
				break;
		}

		return laneCount;
	}

	_getRotation() {

		let rotation;

		if(this._type === 3) {
			rotation = Utils.degToRad(this._rotation + 90);
		} else if ( this._type === 6) {
			 rotation = Utils.degToRad(this._rotation + 180);
		} else {
			rotation = Utils.degToRad(this._rotation);			
		}

		return rotation;
	}

	_onClick() {
		// console.log(this);
	}

	getXPosition() {
		return this.sprite.width * this._col + this.sprite.width / 2;
	}

	getYPosition() {
		return this.sprite.height * this._row + this.sprite.height / 2;
	}
}

module.exports = class MapTile extends AbstractMapTile {
	constructor(tileType, tileOptions) {
		super(tileType, tileOptions);

		this.sprite = this.getSprite();

		if(this.sprite) {
			this.sprite.interactive = true;

			this.sprite.on('pointerdown', () => this._onClick());
		}
		
	}

	draw(mapContainer) {
		if(this.sprite) {
			this.sprite.x = this.getXPosition();
			this.sprite.y = this.getYPosition();

			mapContainer.addChild(this.sprite);
		}
	}
}
