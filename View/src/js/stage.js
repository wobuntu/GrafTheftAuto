const Loader = require('./loader');
const Map = require('./map');
const TrafficLightHolder = require('./trafficLights');
const VehicleHolder = require('./vehicleHolder');
const Car = require('./car');

module.exports = class Stage {
	constructor(element, socketConnection, callback) {

		this.dragging = false;

		this.cars = [];
		this.tickListeners = {};


		let appWidth = window.innerWidth > 1024 ? window.innerWidth : 1024;
		let appHeight = window.innerHeight > 960 ? window.innerHeight : 960;

		this.app = new PIXI.Application(appWidth, appHeight, {
			transparent: true
		});

		element.appendChild(this.app.view);
		element.addEventListener('mousewheel', this._onMouseWheel.bind(this));

		this.mainStage = new PIXI.Container();
		this.mainStage.interactive = true;

		this.app.stage.addChild(this.mainStage);

		socketConnection.registerCallback('map', (event) => {

			let jsonData = JSON.parse(event.data);
			let mapData = jsonData.Params['$values'][0];

			this.mapData = mapData;

			if(!mapData || !mapData.GridHeight) {

				let errorInformation = {
					type: "error",
					error: "no-mapdata"
				};

				callback(errorInformation);
			} else {
				socketConnection.getTrafficLightPositions();
			}

		});

		socketConnection.registerCallback('trafficLightPositions', (event) => {

			let jsonData = JSON.parse(event.data);
			let trafficLightsList = jsonData.Params['$values'][0];

			let trafficLights = [];

			if(trafficLightsList && trafficLightsList['$values']) {
				trafficLights = trafficLightsList['$values'];
			}

			this.trafficLightData = trafficLights;

			this._setup(socketConnection, callback)
		});

		Loader.loadAssets(() => {
			socketConnection.getMap();
		});
	}

	_setup(socketConnection, cb) {

		this.mapContainer = new PIXI.Container();
    	this.mainStage.addChild(this.mapContainer);

		this.gameMap = new Map(
						this.mapContainer,
						this.mapData
					);

		this.trafficLightHolder = new TrafficLightHolder(
										this.mapContainer,
										socketConnection,
										this.trafficLightData
									);

		this.vehicleHolder = new VehicleHolder(
									this.mapContainer,
									socketConnection
								);

		this.mainStage.on('pointerdown', () => this._onClick());

		this.mainStage.on('pointerdown', this._onDragStart.bind(this))
			          .on('pointerup', this._onDragEnd.bind(this))
			          .on('pointerupoutside', this._onDragEnd.bind(this))
			          .on('pointermove', this._onDragMove.bind(this));

		this.app.ticker.add(() => this._tick());

		cb();
	}

	_tick() {
		for (let prop in this.tickListeners) {
			this.tickListeners[prop](this);
		}
	}

	_onClick() {
		// console.log("map click");
	}

	_onDragStart(event) {
		this.dragging = true;
	}

	_onDragEnd() {
		this.dragging = false;
	}

	_onDragMove(e) {
		if(this.dragging) {
	        this.mainStage.position.x += e.data.originalEvent.movementX;
      		this.mainStage.position.y += e.data.originalEvent.movementY;
		}
	}

	_onMouseWheel(e) {
		let currentScale = this.mainStage.scale;

		if(e.deltaY > 0 && currentScale.x >= 0.3) {
			this.mainStage.scale.set(currentScale.x -= 0.05, currentScale.y -= 0.05)	
		} else if (e.deltaY < 0 && currentScale.x <= 2) {
			this.mainStage.scale.set(currentScale.x += 0.05, currentScale.y += 0.05)	
		}
	}

	addTickListener(listenerId, listenerFn){

		if(this.tickListeners.hasOwnProperty(listenerId)) {
			throw new Exception("Listener with this ID already exists");
		}

		this.tickListeners[listenerId] = listenerFn.bind(this);
	}

	addCar(id, x, y, orientation) {

		if(!x){
			x = 20;
		}

		if(!y){
			y = 30;
		}

		if(!orientation) {
			orientation = 'e';
		}

		this.cars.push( 
			new Car(this.mainStage, this.app, id, x, y, orientation)
		);
	}
}
