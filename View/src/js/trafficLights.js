const TrafficLightGroup = require('./trafficLightGroup');

module.exports = class TrafficLights {
	
	constructor(mapContainer, socketConnection, trafficLightData) {
		
		this.mapContainer = mapContainer;
		this.intervalGroups = {};

		trafficLightData.forEach((trafficLightParams) => {

			let intervalId = trafficLightParams.IntervalId;

			if(!this.intervalGroups[intervalId]){
				this.intervalGroups[intervalId] = [];
			}

			let positions = trafficLightParams.Positions['$values'];
			let state = trafficLightParams.State;

			let trafficLightGroup = new TrafficLightGroup(intervalId, positions, state);

			this.intervalGroups[intervalId].push(trafficLightGroup);
		});

		this._drawTrafficLights();

		socketConnection.registerCallback('trafficLightStates', (event) => {

			let jsonData = JSON.parse(event.data);			
			let trafficLightStates = jsonData.Params['$values'][0];

			Object.keys(this.intervalGroups).forEach((intervalId) => {
				this.intervalGroups[intervalId].forEach((lightGroup) => {
					let newState = trafficLightStates[intervalId];

					lightGroup.setState(newState);
				});
			});

			setTimeout(() => {
				socketConnection.getTrafficLightStates();
			}, 1000);

		});

		socketConnection.getTrafficLightStates();

	}

	_drawTrafficLights() {

		Object.keys(this.intervalGroups).forEach((key) => {
			this.intervalGroups[key].forEach((lightGroup) => {
				let gfxHolder = lightGroup.getGfx();

				gfxHolder.forEach((gfxObj) => {
					this.mapContainer.addChild(gfxObj.gfx);
				});
			});
		});
	}

}