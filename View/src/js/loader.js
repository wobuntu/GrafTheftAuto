module.exports = {

	loadAssets: (callback) => {

		PIXI.loader
	        .add('grass', 'static/img/map/grass.png')
			.add('oneLaneStraight', 'static/img/map/Straight_1.png')
			.add('twoLaneStraight', 'static/img/map/Straight_2.png')
			.add('threeLaneStraight', 'static/img/map/Straight_3.png')
			.add('oneToTwoLaneWiden', 'static/img/map/widen_2.png')
			.add('twoToThreeLaneWiden', 'static/img/map/widen_3.png')
			.add('oneLaneCurve', 'static/img/map/CurveRight_straight_1.png')
			.add('twoLaneCurve', 'static/img/map/CurveRight_straight_2.png')
			.add('threeLaneCurve', 'static/img/map/CurveRight_straight_3.png')
			.add('oneLaneCrossing', 'static/img/map/CrossingFour_2.png')
			.add('twoLaneCrossing', 'static/img/map/CrossingFour_2.png')
			.add('threeLaneCrossing', 'static/img/map/CrossingFour_3.png')

			.add('car_1', 'static/img/vehicles/spr_car_1.png')
			.add('car_2', 'static/img/vehicles/spr_car_2.png')
			.add('car_3', 'static/img/vehicles/spr_car_3.png')
			.add('car_4', 'static/img/vehicles/spr_car_4.png')
			.add('car_5', 'static/img/vehicles/spr_car_5.png')
			.add('truck_1', 'static/img/vehicles/spr_truck_1.png')
			.add('bike_1', 'static/img/vehicles/spr_motorcycle_1.png')
			.add('bike_2', 'static/img/vehicles/spr_motorcycle_2.png')
			.add('bike_3', 'static/img/vehicles/spr_motorcycle_3.png')

	        .load(() => {
	        		callback();
	        	}
        	);

        	PIXI.sound.add('honk_1', 'static/sound/honk_1.mp3');
        	PIXI.sound.add('honk_2', 'static/sound/honk_2.mp3');
	}
}
