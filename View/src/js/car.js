const utils = require('./utilities');

module.exports = class Car {
    constructor(mainStage, game, id, x, y, orientation) {

        this.carId = id;

        this._mainStage = mainStage;

        this._game = game;
        this._body = PIXI.Sprite.fromImage("static/img/sprites/terrain/racingtiles01/PNG/Cars/car_black_small_1.png");

        this._body.position.x = x;
        this._body.position.y = y;
        this._body.anchor.x = 0.5;
        this._body.anchor.y = 0.5;

        this.speed = 5;

        if(orientation === 'north'){
            this.faceNorth()
        } else if(orientation === 'south') {
            this.faceSouth()
        } else if(orientation === 'west') {
            this.faceWest()
        } else {
            this.faceEast();    
        }

        this._mainStage.addChild(this._body);

        this._game.ticker.add(() => {
            this._update();
        });

        // Opt-in to interactivity
        this._body.interactive = true;

        // Shows hand cursor
        this._body.buttonMode = true;

        // Pointers normalize touch and mouse
        this._body.on('pointerdown', () => this._onClick());
    }

    _update() {

        this._navigate();
        this._drive();

    }

    _onClick() {
        this._body.scale.x *= 1.25;
        this._body.scale.y *= 1.25;     
    }


    _navigate() {
        let pos = this._body.position;
        let screen = this._game.screen;
        let direction = this.getDirection();

        if(pos.y <= screen.height / 2) {
            if(pos.x >= screen.width - 35 && direction === 'east') {
                this.faceSouth();
            }
        }

        if(pos.x <= 35 && pos.y <= 35 && direction === 'north') {
            this.faceEast();
        }

        if(pos.y > screen.height - 35) {
            if (pos.x >= screen.width - 35 && direction === 'south') {
                this.faceWest();
            } else if(pos.x <= 35 && direction === 'west') {
                this.faceNorth();
            }
        }
    }

    _drive() {
        switch(this.getDirection()) {
            case 'north':
                this._body.position.y -= this.speed;
                break;
            case 'east':
                this._body.position.x += this.speed;
                break;
            case 'south':
                this._body.position.y += this.speed;
                break;
            case 'west':
                this._body.position.x -= this.speed;
                break;
        }
    }

    _turn(rotation) {
    	this._body.rotation = rotation;
    }

    faceNorth() {
    	this._turn(utils.degToRad(0));
    }

    faceSouth() {
    	this._turn(utils.degToRad(180));
    }

    faceEast() {
    	this._turn(utils.degToRad(90));
    }

    faceWest() {
    	this._turn(utils.degToRad(270));
    }

    getDirection() {
        switch(utils.radToDeg(this._body.rotation)) {
            case 0:
                return 'north';
            case 90:
                return 'east';
            case 180:
                return 'south';
            case 270:
                return 'west';
        }
    }

    getCarData() {
        return {
            id: this.carId,
            x: this._body.position.x,
            y: this._body.position.y,
            orientation: this.getDirection()
        }
    }

}
