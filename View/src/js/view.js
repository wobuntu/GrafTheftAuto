PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;

const SocketConnector = require('./socketConnector');
const Stage = require( './stage' );

const viewHolder = document.getElementById('view-holder');
const errorHolder = document.getElementById('error-holder');
const socketConn = new SocketConnector();

const optionsPanel = document.getElementById('panel-options');
const btnPlayPause = document.getElementById('button-playpause');

const inputTfInterval = document.getElementById('input-tfinterval');
const btnTfInterval = document.getElementById('btn-tfinterval');

const inputMaxVehicles = document.getElementById('input-maxvehicles');
const btnMaxVehicles = document.getElementById('btn-maxvehicles');

const inputVehicleInitInterval = document.getElementById('input-vehicleinitinterval');
const btnVehicleInitInterval = document.getElementById('btn-vehicleinitinterval');

const notificationHolder = document.getElementById('notification-holder');

const notificationPaused = document.getElementById('notification-paused');
const notificationResumed = document.getElementById('notification-resumed');
const notificationTfInterval = document.getElementById('notification-tfinterval');
const notificationMaxVehicles = document.getElementById('notification-maxvehicles');
const notificationVehicleInterval = document.getElementById('notification-vehicleinitinterval');

let isPaused = false;	

socketConn.connect(() => {
	initMap();
}, () => {
	console.error("Connection Error");

	hideAllNotifications();
	let noConnErrorBox = errorHolder.querySelector('.error-noconnection');
	noConnErrorBox.style.display = 'block';
});


const initMap = function() {
	const trafficStage = new Stage(viewHolder, socketConn, (information) => {

		if(information) {

			console.error("Rendering error");

			if(information.type === "error") {
				
				hideAllNotifications();
				
				let errorType = information.error;

				switch(errorType) {
					case 'no-mapdata':
						let noMapData = errorHolder.querySelector('.error-nomapdata');
						noConnErrorBox.style.display = 'block';
						break;
				}
			} else {
				initUi();
			}
		} else {
			initUi();
		}

		document.getElementById('notification-loading').style.display = 'none';
	});

}

const initUi = function () {

	optionsPanel.classList.remove('is-invisible');
	
	registerUiSocketCallbacks();
	setupPlayPause();
	setupParameterBlocks();
};

const registerUiSocketCallbacks = function() {

	socketConn.registerCallback('pause', (event) => {

		let pauseResponse = JSON.parse(event.data);

		let callSuccessful = false;

		if(pauseResponse.Params['$values']) {
			callSuccessful = pauseResponse.Params['$values'][0];
		}

		if(callSuccessful) {
			btnPlayPause.textContent = "Resume";
			btnPlayPause.classList.add('is-active');
			isPaused = true;

			hideAllNotifications();
			notificationPaused.style.display = 'block';

			setTimeout(() => {
				notificationPaused.style.display = 'none';				
			}, 2000);
		}
	});

	socketConn.registerCallback('resume', (event) => {
		let resumeResponse = JSON.parse(event.data);

		let callSuccessful = false;

		if(resumeResponse.Params['$values']) {
			callSuccessful = resumeResponse.Params['$values'][0];
		}

		if(callSuccessful) {
			btnPlayPause.textContent = "Pause";
			btnPlayPause.classList.remove('is-active');
			isPaused = false;

			hideAllNotifications();
			notificationResumed.style.display = 'block';

			setTimeout(() => {
				notificationResumed.style.display = 'none';				
			}, 2000);
		}
	});

	socketConn.registerCallback('getTrafficLightInterval', (event) => {
		let respData = JSON.parse(event.data);
		let initialTrafficLightInterval = parseInt(respData.Params['$values'][0], 10);

		inputTfInterval.value = initialTrafficLightInterval;
		inputTfInterval.removeAttribute('disabled');
		btnTfInterval.removeAttribute('disabled');
	});

	socketConn.registerCallback('setTrafficLightInterval', (event) => {
		let respData = JSON.parse(event.data);
		
		let value = inputTfInterval.value;

		hideAllNotifications();
		notificationTfInterval.textContent = "Traffic Light Interval was set to " + value;
		notificationTfInterval.style.display = 'block';

		setTimeout(() => {
			notificationTfInterval.style.display = 'none';				
		}, 2000);
	});

	socketConn.registerCallback('getMaxVehicles', (event) => {
		let respData = JSON.parse(event.data);

		let initialMaxVehicles = parseInt(respData.Params['$values'][0]);

		inputMaxVehicles.value = initialMaxVehicles;
		inputMaxVehicles.removeAttribute('disabled');
		btnMaxVehicles.removeAttribute('disabled');
	});

	socketConn.registerCallback('setMaxVehicles', (event) => {
		let respData = JSON.parse(event.data);
		let value = inputMaxVehicles.value;

		hideAllNotifications();
		notificationMaxVehicles.textContent = "Max Vehicles was set to " + value;
		notificationMaxVehicles.style.display = 'block';

		setTimeout(() => {
			notificationMaxVehicles.style.display = 'none';				
		}, 2000);
	});

	socketConn.registerCallback('getVehicleInitInterval', (event) => {
		let respData = JSON.parse(event.data);
		let initialVehicleInitInterval = parseInt(respData.Params['$values'][0]);

		inputVehicleInitInterval.value = initialVehicleInitInterval;
		inputVehicleInitInterval.removeAttribute('disabled');
		btnVehicleInitInterval.removeAttribute('disabled');
	});

	socketConn.registerCallback('setVehicleInitInterval', (event) => {
		let respData = JSON.parse(event.data);
		
		let value = inputVehicleInitInterval.value;

		hideAllNotifications();
		notificationVehicleInterval.textContent = "Vehicle Init Interval was set to " + value;
		notificationVehicleInterval.style.display = 'block';

		setTimeout(() => {
			notificationVehicleInterval.style.display = 'none';				
		}, 2000);
	});
}

const setupPlayPause = function() {

	btnPlayPause.addEventListener('click', (event) => {
		if(isPaused) {
			socketConn.resumeSimulation();
		} else {
			socketConn.pauseSimulation();
		}
	});
};

const setupParameterBlocks = function() {

	socketConn.getTrafficLightInterval();
	socketConn.getMaximumVehicles();
	socketConn.getInitIntervalForVehicles();

	btnTfInterval.addEventListener('click', (event) => {
		let newVal = parseInt(inputTfInterval.value, 10);

		if(newVal) {
			socketConn.setTrafficLightInterval(newVal);
		} else {
			// bullshit value
		}
	});

	btnMaxVehicles.addEventListener('click', (event) => {
		let newVal = parseInt(inputMaxVehicles.value, 10);

		if(newVal) {
			socketConn.setMaximumVehicles(newVal);
		} else {
			// bullshit value
		}
	});

	btnVehicleInitInterval.addEventListener('click', (event) => {
		let newVal = parseInt(inputVehicleInitInterval.value, 10);

		if(newVal) {
			socketConn.setInitIntervalForVehicles(newVal);
		} else {
			// bullshit value
		}
	});
}


const hideAllNotifications = function() {
	let notifications = notificationHolder.querySelectorAll('.notification');

	notifications.forEach((element) => {
		element.style.display = 'none';
	});
}

