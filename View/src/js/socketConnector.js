module.exports = class SocketConnector {
	constructor() {
		this.socketUrl = 'ws://127.0.0.1/GrafTheftAuto/ISimulation';

		this.callbacks = {
			'map' : [],
			'trafficLightPositions': [],
			'trafficLightStates': [],
			'vehicles': [],
			'pause': [],
			'resume': [],
			'getTrafficLightInterval': [],
			'setTrafficLightInterval': [],
			'getMaxVehicles': [],
			'setMaxVehicles': [],
			'getVehicleInitInterval': [],
			'setVehicleInitInterval': []
		};
	}

	connect(cb, errCb) {
		this.socketConnection = new WebSocket(this.socketUrl);

		this.socketConnection.onopen = (event) => {

			this.setMessageListener();

			cb();
		};

		this.socketConnection.onerror = (event) => {
			errCb();
		};
	}

	setMessageListener() {
		this.socketConnection.onmessage = (event) => {
			this.messageHandler(event);
		}
	}

	getMap(cb) {
		let mapPayload = this._buildPayload("GetCurrentMap", 100);
		this.socketConnection.send(JSON.stringify(mapPayload));
	}

	getTrafficLightPositions(cb) {
		let trafficLightPayload = this._buildPayload("GetCurrentTrafficLights", 200);
		this.socketConnection.send(JSON.stringify(trafficLightPayload));
	}

	getTrafficLightStates(cb) {
		let trafficLightStatesPayload = this._buildPayload("GetCurrentTrafficLightStates", 300);
		this.socketConnection.send(JSON.stringify(trafficLightStatesPayload));
	}

	getVehicles(cb) {
		let vehiclesPayload = this._buildPayload("GetVehicles", 400);
		this.socketConnection.send(JSON.stringify(vehiclesPayload));
	}

	pauseSimulation() {

		let pausePayload = this._buildPayload("PauseSimulation", 10);
		this.socketConnection.send(JSON.stringify(pausePayload));
	}

	resumeSimulation() {
		let resumePayload = this._buildPayload("ResumeSimulation", 20);
		this.socketConnection.send(JSON.stringify(resumePayload));
	}

	setTrafficLightInterval(interval) {
		let setTrafficLightPayload = this._buildPayload("SetTrafficLightInterval", 30, [interval],
			["System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"]);
		this.socketConnection.send(JSON.stringify(setTrafficLightPayload));
	}

	getTrafficLightInterval() {
		let getTrafficLightPayload = this._buildPayload("GetTrafficLightInterval", 31);
		this.socketConnection.send(JSON.stringify(getTrafficLightPayload));
	}

	setMaximumVehicles(vehicleNum) {
		let setMaxVehiclesPayload = this._buildPayload("SetMaximumVehicles", 40, [vehicleNum], 
			["System.UInt32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"]);

		this.socketConnection.send(JSON.stringify(setMaxVehiclesPayload));	
	}

	getMaximumVehicles() {
		let getMaxVehiclesPayload = this._buildPayload("GetMaximumVehicles", 41);
		this.socketConnection.send(JSON.stringify(getMaxVehiclesPayload));	
	}

	setInitIntervalForVehicles(initInterval) {
		let setInitIntervalPayload = this._buildPayload("SetInitIntervalForVehicles", 50, [initInterval], 
			["System.UInt32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"]);
		this.socketConnection.send(JSON.stringify(setInitIntervalPayload));		
	}

	getInitIntervalForVehicles() {
		let getInitIntervalPayload = this._buildPayload("GetInitIntervalForVehicles", 51);
		this.socketConnection.send(JSON.stringify(getInitIntervalPayload));		
	}

	messageHandler(event) {

		let responseData = JSON.parse(event.data);

		let responseId = responseData.Id;
		let fnsToCall;

		switch(responseId) {
			case 10:
				fnsToCall = this.callbacks.pause;
				break;
			case 20:
				fnsToCall = this.callbacks.resume;
				break;
			case 100:
				fnsToCall = this.callbacks.map;
				break;
			case 200:
				fnsToCall = this.callbacks.trafficLightPositions;
				break;
			case 300:
				fnsToCall = this.callbacks.trafficLightStates;
				break;
			case 400:
				fnsToCall = this.callbacks.vehicles;
				break;
			case 30:
				fnsToCall = this.callbacks.setTrafficLightInterval;
				break;
			case 31:
				fnsToCall = this.callbacks.getTrafficLightInterval;
				break;
			case 40:
				fnsToCall = this.callbacks.setMaxVehicles;
				break;
			case 41:
				fnsToCall = this.callbacks.getMaxVehicles;
				break;
			case 50:
				fnsToCall = this.callbacks.setVehicleInitInterval;
				break;
			case 51:
				fnsToCall = this.callbacks.getVehicleInitInterval;
				break;
			default:
				// no valid param found, throw exception or do nothing
				console.error("[SocketConnector] ResponseId not recognized", responseId, responseData);
				break;
		}

		if(fnsToCall) {
			fnsToCall.forEach((fn) => {
				fn(event);
			});
		}	
	}

	registerCallback(key, fn) {
		this.callbacks[key].push(fn)
	}

	_buildPayload(target, requestId, values, paramTypes) {

		let paramTypeVals = paramTypes || [];
		let payloadValues = values || [];

		let payload = {
			"$id": "1",
		    "$type": "Core.Net.RemoteInvocation.RemoteInvocationInfo, Core",
		    "ComponentName": "ISimulation",
		    "Params": {
		      "$type": "System.Object[], mscorlib",
		      "$values": payloadValues
		    },
		    "TypeParams": {
		      "$type": "System.Type[], mscorlib",
		      "$values": []
		    },
		    "ParamTypes": {
		      "$type": "System.Type[], mscorlib",
		      "$values": paramTypeVals
		    }
		}

		payload["TargetName"] = target;
		payload["Id"] = requestId;

		return payload;

	}

};