const Vehicle = require('./vehicle');

module.exports = class VehicleHolder {
	constructor(mapContainer, socketConnection) {

		this.mapContainer = mapContainer;
		this.cars = {};

		socketConnection.registerCallback('vehicles', (event) => {
			let jsonData = JSON.parse(event.data);

			if(jsonData && jsonData.Params && jsonData.Params['$values']) {
				let carData = jsonData.Params['$values'][0];

				if(carData['$values']) {
					let carVals = carData['$values'];

					this.updateCars(carVals);
				}

				setTimeout(() => {
					socketConnection.getVehicles()
				}, 75)
			}
			
		});

		socketConnection.getVehicles();
	}

	updateCars(carValues) {

		let existingCars = {};

		Object.keys(this.cars).forEach((key) => {
			existingCars[key] = this.cars[key];
		});

		carValues.forEach((carParams) => {

			let carId = carParams.Id;
			let vehicle;

			if(!this.cars[carId]){

				vehicle = new Vehicle(carId, carParams);

				this.cars[carId] = vehicle;
				this.mapContainer.addChild(vehicle.getSprite());

			} else {
				vehicle = this.cars[carId];
				vehicle.setParameters(carParams);
			}

			delete existingCars[carId];
		});

		if(Object.keys(existingCars).length > 0) {

			// console.log("Purging orphan vehicles", existingCars);

			Object.keys(existingCars).forEach((key) => {
				let vehicle = existingCars[key];

				this.mapContainer.removeChild(vehicle.getSprite());

				delete this.cars[key];
			});
		}

		existingCars = {};
	}
}