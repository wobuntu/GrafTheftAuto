const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: "./src/js/view.js",
    watch: true,
    mode: "development",
    output: {
        filename: "./static/js/bundle.js"
    },
    node: {
		fs: 'empty'
	},
	module: {
	},
    plugins: [
        new CopyWebpackPlugin([
            {
                from: './src/img/',
                to: './static/img/'
            },
            {
                from: './src/ext/',
                to: './static/',
                toType: 'dir'
            },
            {
                from: './src/css/',
                to: './static/css/'
            }
        ])
    ]
};
