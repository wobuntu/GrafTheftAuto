﻿using System;
using System.Linq;

using Core.Classes;
using Core.Net.Stubs;
using Core.Interfaces;
using Core.Net.Websockets;
using Core.Net.RemoteInvocation;
using Core.Logging;

namespace ViewTester
{
  class Program
  {
    static ConsoleFeedPrinter feedPrinter = new ConsoleFeedPrinter();

    static bool exit = false;
    static void Main(string[] args)
    {
      Feed.Subscribe(feedPrinter, LogLvl.All);

      Console.Title = "Debug tool for View calls on localhost";
      Client client = new Client($"ws://127.0.0.1:80/GrafTheftAuto/{nameof(ISimulation)}/");
      StubBehaviorProvider behavior = new StubRemoteInvocationBehaviorProvider(client);

      stub = StubBuilder.CompileStubFor<ISimulation>(behavior);

      while (!exit)
      {
        try
        {
          promptNextAction();
        }
        catch (Exception ex)
        {
          Feed.Report("Error: " + ex, LogLvl.Error);
        }
      }
    }

    static ISimulation stub;

    static void promptNextAction()
    {
      Feed.Report("ViewTester");
      Feed.Report("Note: This is just a little debug program for calls from the view. " +
        "It also displays exceptions which is intended if a service is not running or " +
        "something else goes wrong.", LogLvl.InfoHighlighted);
      Feed.Report("Press one of the following options: ");
      Feed.Report($"[ 0] Exit");
      Feed.Report($"[ 1] {nameof(ISimulation.GetCurrentMap)}");
      Feed.Report($"[ 2] {nameof(ISimulation.GetCurrentTrafficLights)}");
      Feed.Report($"[ 3] {nameof(ISimulation.GetCurrentTrafficLightStates)}");
      Feed.Report($"[ 4] {nameof(ISimulation.GetVehicles)}");
      Feed.Report($"[ 5] {nameof(ISimulation.PauseSimulation)}");
      Feed.Report($"[ 6] {nameof(ISimulation.ResumeSimulation)}");
      Feed.Report($"[ 7] {nameof(ISimulation.SetTrafficLightInterval)}");
      Feed.Report($"[ 8] {nameof(ISimulation.GetTrafficLightInterval)}");
      Feed.Report($"[ 9] {nameof(ISimulation.SetMaximumVehicles)}");
      Feed.Report($"[10] {nameof(ISimulation.GetMaximumVehicles)}");
      Feed.Report($"[11] {nameof(ISimulation.SetInitIntervalForVehicles)}");
      Feed.Report($"[12] {nameof(ISimulation.GetInitIntervalForVehicles)}");
      Feed.Report($"[13] Stress test of fetching the vehicle list via my RMI implementation");
      var input = Console.ReadLine();

      if (int.TryParse(input, out int choice))
      {
        switch (choice)
        {
          case 0: exit = true; break;
          case 1:
            Feed.Report("Getting map:");
            Map m = stub.GetCurrentMap();
            if (m == null)
            {
              Feed.Report($"Null received", LogLvl.Warn);
            }
            else
            {
              Feed.Report($"Got map, width = {m.GridWidth}, height = {m.GridHeight}", LogLvl.InfoHighlighted);
            }
            break;
          case 2:
            Feed.Report("Getting traffic lights:");
            var l = stub.GetCurrentTrafficLights();
            if (l == null)
            {
              Feed.Report("Null received", LogLvl.Warn);
            }
            else
            {
              Feed.Report($"Got traffic lights: Amount: {l.Count()}", LogLvl.InfoHighlighted);
            }
            break;
          case 3:
            Feed.Report("Getting traffic light states:");
            var s = stub.GetCurrentTrafficLightStates();
            if (s == null)
            {
              Feed.Report("Null received", LogLvl.Warn);
            }
            else
            {
              Feed.Report("First found state: IntervalId=" + s.FirstOrDefault().Key + ", State=" + s.FirstOrDefault().Value, LogLvl.InfoHighlighted);
            }
            break;
          case 4:
            Feed.Report("Getting vehicles:");
            var v = stub.GetVehicles();
            if (v == null)
            {
              Feed.Report("Null received", LogLvl.Warn);
            }
            else
            {
              Feed.Report($"Got vehicles: amount={v.Count}, position of first: {v.First().PositionOnGrid.X}/{v.First().PositionOnGrid.Y}", LogLvl.InfoHighlighted);
            }
            break;
          case 5:
            Feed.Report("Pausing simulation:");
            var paused = stub.PauseSimulation();
            if (paused)
            {
              Feed.Report("Successfully paused", LogLvl.InfoHighlighted);
            }
            else
            {
              Feed.Report("Pausing failed", LogLvl.Warn);
            }
            break;
          case 6:
            Feed.Report("Pausing simulation:");
            var resumed = stub.ResumeSimulation();
            if (resumed)
            {
              Feed.Report("Successfully resumed", LogLvl.InfoHighlighted);
            }
            else
            {
              Feed.Report("Resuming failed", LogLvl.Warn);
            }
            break;
          case 7:
            Feed.Report("Setting traffic light interval: Which value shall be set?");
            var intervalInput = Console.ReadLine();
            if (int.TryParse(intervalInput, out int interval))
            {
              stub.SetTrafficLightInterval(interval);
              Feed.Report("Value for traffic light interval set.", LogLvl.InfoHighlighted);
            }
            else
            {
              Feed.Report("Invalid input", LogLvl.Warn);
            }
            break;
          case 8:
            Feed.Report("Getting traffic light interval:");
            var trafficLightInterval = stub.GetTrafficLightInterval();
            Feed.Report("Value for traffic light interval fetched: " + trafficLightInterval, LogLvl.InfoHighlighted);
            break;
          case 9:
            Feed.Report("Setting max amount of cars: Which value shall be set?");
            var carsInput = Console.ReadLine();
            if (uint.TryParse(carsInput, out uint numCars))
            {
              stub.SetMaximumVehicles(numCars);
              Feed.Report("Value for max amount of vehicles set.", LogLvl.InfoHighlighted);
            }
            else
            {
              Feed.Report("Invalid input", LogLvl.Warn);
            }
            break;
          case 10:
            Feed.Report("Getting max amount of cars:");
            var maxCars = stub.GetMaximumVehicles();
            Feed.Report("Value for max amount of vehicles fetched: " + maxCars, LogLvl.InfoHighlighted);
            break;
          case 11:
            Feed.Report("Setting car init interval: Which value shall be set?");
            var carsInitInput = Console.ReadLine();
            if (uint.TryParse(carsInitInput, out uint numInitCars))
            {
              stub.SetInitIntervalForVehicles(numInitCars);
              Feed.Report("Value for car init interval set.", LogLvl.InfoHighlighted);
            }
            else
            {
              Feed.Report("Invalid input", LogLvl.Warn);
            }
            break;
          case 12:
            Feed.Report("Getting car init interval:");
            var initInterval = stub.GetInitIntervalForVehicles();
            Feed.Report("Value for car init interval fetched: " + initInterval, LogLvl.InfoHighlighted);
            break;
          case 13:
            Feed.Unsubscribe(feedPrinter);
            Console.Clear();

            Console.WriteLine($"NOTE: this stress test gets the vehicle list sequentially without pausing inbetween requests.");
            Console.WriteLine($"This means, the autocompiled stub for {nameof(ISimulation)} is used to call method " +
              $"{nameof(stub.GetVehicles)} from the simulation server, which uses its autocompiled stub for " +
              $"{nameof(IVehiclePool)} to forward all data to the vehicle server. The displayed times of this test are " +
              $"those for the whole round trip back to this program.\r\n\r\nReally continue? [Y/n]");

            var doStressTest = Console.ReadLine();
            if (doStressTest == string.Empty || doStressTest.ToLower() == "y")
            {
              const int calls = 100;
              Console.Clear();
              Console.WriteLine($"Roundtrip time in milliseconds (Avg of {calls} calls):");
              Console.WriteLine("Press Ctrl+C to abort the program");
              DateTime startTime = DateTime.Now;

              int cnt = 0;
              double sum = 0;
              while (true)
              {
                Console.SetCursorPosition(55, 0);
                startTime = DateTime.Now;
                stub.GetVehicles();
                sum += (DateTime.Now - startTime).TotalMilliseconds;

                if (cnt == calls - 1)
                {
                  Console.Write(String.Format("{0,5:####.00}", sum / calls));
                  cnt = 0;
                  sum = 0;
                }
                else
                {
                  cnt++;
                }
              }
            }
            break;
          default:
            Feed.Report("Nope, input not correct", LogLvl.Warn);
            break;
        }
      }
      else
      {
        Feed.Report("Nope, input not correct", LogLvl.Warn);
      }
    }
  }
}
