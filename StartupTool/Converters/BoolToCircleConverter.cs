﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Shapes;

namespace GrafTheftAuto.Converters
{
  public class BoolToCircleConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      Brush fill = Brushes.DimGray;
      bool? nullable = value as bool?;
      if (nullable != null)
      {
        fill = nullable.Value ? Brushes.DarkGreen : Brushes.OrangeRed;
      }
      else if (value is bool b)
      {
        fill = nullable.Value ? Brushes.DarkGreen : Brushes.OrangeRed;
      }

      return new Ellipse()
      {
        Width = 14,
        Height = 14,
        Fill = fill,
        StrokeThickness = 1,
        Stroke = Brushes.LightGray,
        Opacity = 0.8
      };
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
