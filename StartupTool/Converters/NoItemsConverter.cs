﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace GrafTheftAuto.Converters
{
  public class NoItemsConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is IEnumerable<string> infos && infos.Count() > 0)
      {
        if (targetType.IsAssignableFrom(typeof(bool)))
        {
          return true;
        }

        return infos;
      }
      else
      {
        if (targetType.IsAssignableFrom(typeof(bool)))
        {
          return false;
        }

        return new List<string>()
        {
          "None found"
        };
      }
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      // Not required
      throw new NotImplementedException();
    }
  }
}
