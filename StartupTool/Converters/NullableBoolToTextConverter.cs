﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace GrafTheftAuto.Converters
{
  public class NullableBoolToTextConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      bool? bval = value as bool?;
      switch (bval)
      {
        case null:
          return "Unknown";
        case true:
          return "Yes";
        default:
          return "No";
      }
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      // Not required
      throw new NotImplementedException();
    }
  }
}
