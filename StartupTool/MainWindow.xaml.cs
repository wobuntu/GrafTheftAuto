﻿using System.IO;
using System.Linq;
using System.Windows;
using System.Threading;
using System.Diagnostics;
using System.Windows.Input;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;

using Core;
using Core.ComponentDiscovery;
using System.Net;

namespace GrafTheftAuto
{
  public partial class MainWindow : Window, INotifyPropertyChanged
  {
    public MainWindow()
    {
      InitializeComponent();
      
      var items = ComponentInfo.TryDeserialize("ComponentInfos.json");
      if (items != null)
      {
        this.StartableItems = new ObservableCollection<ComponentInfo>(items.OrderBy(x=>x.Name));
      }

      // Setup of commands
      this.CheckRunningLocallyCommand = new ActionCommand(this.doCheckRunningLocally, this.canCheckRunningLocally);
      this.CheckRunningInNetworkCommand = new ActionCommand(this.doCheckRunningInNetwork, this.canCheckRunningInNetwork);
      this.StartLocalInstanceCommand = new ActionCommand(this.doStartNewLocalInstance, this.canStartNewLocalInstance);
      this.KillProcessCommand = new ActionCommand(this.doKillProcess, this.canKillProcess);
      this.BringProcessToFrontCommand = new ActionCommand(this.doBringProcessToFront, this.canKillProcess); // we can use the same check as for the above command

      this.DataContext = this;
      updateStates();
    }

    #region Properties for binding
    public ObservableCollection<ComponentInfo> StartableItems { get; set; }

    private ComponentInfo selectedItem = null;
    public ComponentInfo SelectedItem
    {
      get => selectedItem;
      set
      {
        selectedItem = value;

        if (this.canCheckRunningLocally(null))
        {
          this.doCheckRunningLocally(null);
        }
        
        OnPropertyChanged();
        SelectedIndexFoundInNetwork = 0;
      }
    }

    private int selectedIndexFoundInNetwork = 0;
    public int SelectedIndexFoundInNetwork
    {
      get => selectedIndexFoundInNetwork;
      set
      {
        selectedIndexFoundInNetwork = value;
        OnPropertyChanged();
      }
    }

    bool isCheckingForNetworkInstances = false;
    public bool IsCheckingForNetworkInstances
    {
      get => isCheckingForNetworkInstances;
      set
      {
        isCheckingForNetworkInstances = value;
        OnPropertyChanged();
      }
    }

    string status = "";
    public string Status
    {
      get => status;
      set
      {
        status = value;
        OnPropertyChanged();
      }
    }

    private IPAddress localIPAddress;
    public IPAddress LocalIpAddress
    {
      get => localIPAddress;
      private set
      {
        localIPAddress = value;
        OnPropertyChanged();
      }
    }

    #endregion


    void updateStates()
    {
      Task.Run(async () =>
      {
        while (true)
        {
          Status = "Refreshing States...";
          foreach (var item in this.StartableItems)
          {
            updateRunningLocally(item);
            this.LocalIpAddress = Core.Net.Utils.GetLocalIPAddress();
            await updateRunningInNetwork(item);
          }
          await Task.Delay(500);
          Status = "Ready";

          await Task.Delay(4000);
        }
      });
    }

    void updateRunningLocally(ComponentInfo info)
    {
      if (info != null)
      {
        if (!string.IsNullOrEmpty(info.ProcessName))
        {
          Process[] processes = Process.GetProcessesByName(info.ProcessName);
          info.RunningLocally = processes.Length > 0;
        }
        else
        {
          info.RunningLocally = null;
        }
      }
    }

    Task updateRunningInNetwork(ComponentInfo info)
    {
      return Task.Run(() =>
      {
        if (selectedItem != null && !string.IsNullOrEmpty(selectedItem.NetworkServiceName) && !isCheckingForNetworkInstances)
        {
          var item = info;
          if (!Config.NetworkDiscovery.Ports.ContainsKey(item.NetworkServiceName))
          {
            return;
          }

          IsCheckingForNetworkInstances = true;

          var foundEntries = DiscoveryService.Find(
            item.NetworkServiceName,
            Config.NetworkDiscovery.Ports[item.NetworkServiceName],
            Config.NetworkDiscovery.ReceiveTimeout);

          var list = new ObservableCollection<string>();
          foreach (var entry in foundEntries)
          {
            list.Add(entry.Address.ToString());
          }
          item.FoundNetworkInstances = list;

          IsCheckingForNetworkInstances = false;
        }
      });
    }

    #region Commands
    public ICommand CheckRunningLocallyCommand { get; set; }
    public ICommand CheckRunningInNetworkCommand { get; set; }
    public ICommand StartLocalInstanceCommand { get; set; }
    public ICommand KillProcessCommand { get; set; }
    public ICommand BringProcessToFrontCommand { get; set; }

    bool canCheckRunningLocally(object o)
    {
      return selectedItem != null && !string.IsNullOrEmpty(selectedItem.ProcessName);
    }
    void doCheckRunningLocally(object o)
    {
      Process[] processes = Process.GetProcessesByName(selectedItem.ProcessName);
      SelectedItem.RunningLocally = processes.Length > 0;
    }

    bool canCheckRunningInNetwork(object o)
    {
      return selectedItem != null && !string.IsNullOrEmpty(selectedItem.NetworkServiceName) && !IsCheckingForNetworkInstances;
    }
    void doCheckRunningInNetwork(object o)
    {
      Task.Run(() =>
      {
        IsCheckingForNetworkInstances = true;
        var item = SelectedItem;
        var foundEntries = DiscoveryService.Find(
          item.NetworkServiceName,
          Config.NetworkDiscovery.Ports[item.NetworkServiceName],
          Config.NetworkDiscovery.ReceiveTimeout);

        var list = new ObservableCollection<string>();
        foreach (var entry in foundEntries)
        {
          list.Add(entry.Address.ToString());
        }
        item.FoundNetworkInstances = list;

        IsCheckingForNetworkInstances = false;

        
        SelectedIndexFoundInNetwork = 0;
      });
    }
    
    bool canStartNewLocalInstance(object o)
    {
      var item = selectedItem;
      bool preconditionsSatisfied =
        item != null
        && !string.IsNullOrEmpty(item.Location)
        && File.Exists(item.Location);

      if (!preconditionsSatisfied)
      {
        return false;
      }

      if (canCheckRunningLocally(null))
      {
        doCheckRunningLocally(null);
      }
      else
      {
        return false;
      }

      if (item.RunningLocally == false)
      {
        return true;
      }

      return false;
    }
    void doStartNewLocalInstance(object o)
    {
      var ending = System.IO.Path.GetExtension(selectedItem.Location).ToLower();
      if (ending == ".exe" || ending == ".bat")
      {
        ProcessStartInfo psi = new ProcessStartInfo(selectedItem.Location);
        psi.WorkingDirectory = System.IO.Path.GetDirectoryName(selectedItem.Location);
        psi.CreateNoWindow = false;
        psi.UseShellExecute = false;

        Process.Start(psi);
      }
      else
      {
        Process.Start(selectedItem.Location);
      }

      Thread.Sleep(100);
      doCheckRunningLocally(null);

      Task.Delay(1500).ContinueWith((t)=>Core.Utils.Windows.BringProcessToFront(System.Diagnostics.Process.GetCurrentProcess()));
    }

    bool canKillProcess(object o)
    {
      if (!canCheckRunningLocally(null))
      {
        return false;
      }

      doCheckRunningLocally(null);
      if (selectedItem.RunningLocally == true)
      {
        return true;
      }

      return false;
    }

    void doKillProcess(object o)
    {
      var processes = Process.GetProcesses().Where(pr => pr.ProcessName == selectedItem.ProcessName);
      foreach (var process in processes)
      {
        try
        {
          process.Kill();
          Thread.Sleep(100);
          doCheckRunningLocally(null);
        }
        catch { }
      }
    }

    void doBringProcessToFront(object o)
    {
      var processes = Process.GetProcesses().Where(pr => pr.ProcessName == selectedItem.ProcessName);
      foreach (var process in processes)
      {
        Core.Utils.Windows.BringProcessToFront(process);
      }
    }
    #endregion


    #region INotifyPropertyChanged related
    public event PropertyChangedEventHandler PropertyChanged;
    
    protected void OnPropertyChanged([CallerMemberName]string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
    #endregion

  }
}
