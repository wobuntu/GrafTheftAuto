﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace GrafTheftAuto
{
  public class ComponentInfo : INotifyPropertyChanged
  {
    private string name;
    public string Name
    {
      get => name;
      set
      {
        name = value;
        OnPropertyChanged();
      }
    }

    private string processName;
    public string ProcessName
    {
      get => processName;
      set
      {
        processName = value;
        OnPropertyChanged();
      }
    }

    private string networkServiceName;
    public string NetworkServiceName
    {
      get => networkServiceName;
      set
      {
        networkServiceName = value;
        OnPropertyChanged();
      }
    }

    private string description;
    public string Description
    {
      get => description;
      set
      {
        description = value;
        OnPropertyChanged();
      }
    }

    private string location;
    public string Location
    {
      get => location;
      set
      {
        location = value;
        OnPropertyChanged();
      }
    }

    private bool? runningLocally = null;
    [JsonIgnore]
    public bool? RunningLocally
    {
      get => runningLocally;
      set
      {
        runningLocally = value;
        OnPropertyChanged();
      }
    }

    [JsonIgnore]
    public bool? RunningInNetwork
    {
      get
      {
        if (string.IsNullOrEmpty(this.NetworkServiceName))
        {
          return null;
        }

        return this.foundNetworkInstances != null && this.foundNetworkInstances.Count > 0;
      }
    }

    private ObservableCollection<string> foundNetworkInstances = null;
    [JsonIgnore]
    public ObservableCollection<string> FoundNetworkInstances
    {
      get => foundNetworkInstances;
      set
      {
        foundNetworkInstances = value;
        OnPropertyChanged();
        OnPropertyChanged(nameof(RunningInNetwork));
      }
    }


    #region Serialization / Deserialization
    public static List<ComponentInfo> TryDeserialize(string filepath)
    {
      try
      {
        return JsonConvert.DeserializeObject<List<ComponentInfo>>(File.ReadAllText(filepath));
      }
      catch
      {
        return null;
      }
    }

    public static bool TrySerialize(string filepath, List<ComponentInfo> instances)
    {
      try
      {
        string result = JsonConvert.SerializeObject(instances);
        File.WriteAllText(filepath, result);
        return true;
      }
      catch
      {
        return false;
      }
    }
    #endregion


    #region INotifyPropertyChanged related
    public event PropertyChangedEventHandler PropertyChanged;

    protected void OnPropertyChanged([CallerMemberName]string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
    #endregion
  }
}
