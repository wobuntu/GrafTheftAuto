﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using Core.Classes;
using Core.Interfaces;
using Core.Logging;

namespace TrafficLightService
{
  internal class TrafficLightService : ITrafficLightService
  {
    private Timer timer = null;
    object _lock = new object();

    private ConcurrentDictionary<Guid, Dictionary<int, TrafficLightState>> GroupStates { get; set; }

    public TrafficLightService()
    {
      GroupStates = new ConcurrentDictionary<Guid, Dictionary<int, TrafficLightState>>();
    }

    #region Interface implementation
    private int interval = 5000;
    public int Interval
    {
      get => interval;
      set
      {
        if (value < 1000)
        {
          value = 1000;
        }

        if (timer == null)
        {
          timer = new Timer();
          timer.Elapsed += Timer_Elapsed;
          timer.AutoReset = true;
        }

        timer.Stop();
        timer.Interval = interval = value;
        timer.Start();
      }
    }

    public Guid Register(IEnumerable<ITrafficLight> lights)
    {
      Guid registrationGuid = Guid.NewGuid();
      Feed.Report($"New registration for traffic lights received. Simulation ID={registrationGuid}");
      
      // Alternate the traffic light state according to the key (e.g. 0 = Red, 1 = Green, 2 = Red, ...)
      var intervalIds = lights
        .Select((x) => x.IntervalId)
        .Distinct()
        .ToDictionary(x=>x, x => x % 2 == 0 ? TrafficLightState.Red : TrafficLightState.Green);

      // Initial interval, may be altered later on
      // This will also initialize the timer if not yet done
      if (timer == null)
      {
        Interval = 5000;
      }

      lock (_lock)
      {
        GroupStates[registrationGuid] = intervalIds;
      }

      // The guid for which outside components may request their traffic lights
      return registrationGuid;
    }

    public IDictionary<int, TrafficLightState> GetStates(Guid simulationId)
    {
      if (GroupStates.ContainsKey(simulationId))
      {
        Feed.Report($"States requested for simulation ID {simulationId}, returning values.");
        return GroupStates[simulationId];
      }

      // Invalid simulation ID, return default value
      Feed.Report($"Invalid request for states received: Simulation ID={simulationId}. Returning the contents of the first found dictionary.",
        LogLvl.Warn);
      return new Dictionary<int, TrafficLightState>();
    }

    public void PauseSimulation()
    {
      if (timer == null)
      {
        Feed.Report($"Pausing of the traffic light service requested, but no traffic lights are registered.", LogLvl.Warn);
        return;
      }

      Feed.Report("Pausing of the traffic light service requested, thus the timer was stopped.");
      timer.Stop();
    }

    public void ResumeSimulation()
    {
      if (timer == null)
      {
        Feed.Report($"Resuming of the traffic light service requested, but no traffic lights are registered.", LogLvl.Warn);
        return;
      }

      Feed.Report("Resuming of the traffic light service requested, thus the timer was stopped.");
      timer.Start();
    }
    #endregion


    private void Timer_Elapsed(object sender, ElapsedEventArgs e)
    {
      Feed.Report("Changing traffic light states...", LogLvl.Debug);
      
      lock(_lock)
      {
        var simulationGuids = GroupStates.Keys.ToList();
        foreach (var simguid in simulationGuids)
        {
          var simuStatesDict = GroupStates[simguid];
          var simuIntervalIds = simuStatesDict.Keys.ToList();

          foreach (var simuIntervalId in simuIntervalIds)
          {
            var oldState = simuStatesDict[simuIntervalId];
            simuStatesDict[simuIntervalId]
              = (oldState == TrafficLightState.Green
              ? TrafficLightState.Red
              : TrafficLightState.Green);
          } // foreach
        } // foreach
      } // lock
    } // Timer_Elapsed

  }
}