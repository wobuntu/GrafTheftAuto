﻿using System;
using System.Threading;
using Core;
using Core.ComponentDiscovery;
using Core.Interfaces;
using Core.Logging;
using Core.Net.RemoteInvocation;
using Core.Net.Websockets;

namespace TrafficLightService
{
  internal class TrafficLightServiceStartup
  {
    private static void Main(string[] args)
    {
      const string componentName = nameof(ITrafficLightService);
      Console.Title = componentName;
      var component = new TrafficLightService();
      var port = Config.NetworkDiscovery.Ports[componentName];
      var serverUri = $"http://+:80/GrafTheftAuto/{componentName}/";

      // Register for logging outputs
      Feed.Subscribe(new ConsoleFeedPrinter(), Config.FeedOptions.DefaultLogLvl);
      //Feed.Subscribe(new NLogFeedPrinter(true), LogLvl.All);

      Console.WriteLine($"{componentName} starting up...");

      try
      {
        // Register the component to be found in the network
        var discovery = DiscoveryRegistration.Create(componentName, port);
        Console.WriteLine($"{componentName} was registered for network discovery on port {port}.");

        // Register the RMI handler, so that other foreign components can call methods from this component
        var rmiHandler = new RemoteInvocationHandler(new Server(serverUri));
        rmiHandler.RegisterLocalTarget(componentName, component);
        Console.WriteLine($"{componentName} was registered as an RMI server on {serverUri}");
      }
      catch (Exception ex)
      {
        Console.WriteLine($"An exception occurred: {ex}");
        Console.WriteLine("Press any key to exit");
        Console.ReadLine();
        return;
      }

      Console.WriteLine($"Setup done. Press enter to quit.");
      Console.ReadLine();
      Thread.Sleep(Timeout.Infinite);
     }
  }
}