﻿using System;
using Core.Classes;
using Core.Interfaces;

namespace AutoAI.Vehicles
{
  class Bus : Vehicle
  {
    public Bus(VehiclePool pool, Tuple<ILaneEx, ILaneEx> startEndLaneTuple) : base(pool, startEndLaneTuple)
    {
      CurrentLane = startEndLaneTuple.Item1;
      PositionOnLane = 0;
      LastVelocity = 0;
      lastUpdated = DateTime.Now;
    }

    public override double MaxVelocity => 80;
    internal override double TimeFrom0to100inSeconds => 6;
    internal override double TimeFrom100to0inSeconds => 3.5;
    public override VehicelType VehicleType => VehicelType.Bus;
    public override double Length => 20;
    public override double Width => 5;
  }
}