﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Interfaces;
using Core.Classes;
using System.Windows;
using Newtonsoft.Json;

namespace AutoAI.Vehicles
{
  public abstract class Vehicle : IVehicle
  {
    /// <summary>
    /// Private default constructor for serialization.
    /// </summary>
    [JsonConstructor]
    private Vehicle() { }

    public Vehicle(VehiclePool vehiclePool, Tuple<ILaneEx, ILaneEx> startEndLaneTuple)
    {
      this.CurrentLane = startEndLaneTuple?.Item1;
      this.Destination = startEndLaneTuple?.Item2;
      this.PositionOnGrid = (CurrentLane?.Start.X??0, CurrentLane?.Start.Y??0);

      //this.Route = NavigationService.Contracts.FindRoute(startEndLaneTuple.Item1, startEndLaneTuple.Item2);
      this.PositionOnLane = 0;
      this.Velocity = 0;
      this.LastVelocity = 0;

      this.Id = Guid.NewGuid();
      this.lastUpdated = DateTime.Now;

      this.ContainingVehiclePool = vehiclePool;
    }

    ~Vehicle()
    {
    }

    protected VehiclePool ContainingVehiclePool = null;

    public abstract VehicelType VehicleType { get; }

    public double Velocity { get; internal set; }
    internal double LastVelocity;
    public abstract double MaxVelocity { get; }
    public double Delta { get; internal set; }
    public Guid Id { get; internal set; }
    public abstract double Length { get; }
    public abstract double Width { get; }
    public (double, double) Direction { get; internal set; }
    public double RelativePosition { get; internal set; }
    public ILaneEx CurrentLane { get; internal set; }
    public double PositionOnLane { get; internal set; }
    public ILaneEx Destination { get; internal set; }

    public (double, double) PositionOnGrid { get; set; }

    public double Angle { get; internal set; }

    public double DecisionBoundary { get; set; } = 0.5;

    public IEnumerable<IEnumerable<ILaneEx>> Route { get; internal set; }

    internal DateTime lastUpdated;
    internal Vector normalVecOnLane;
    internal double internalAngle;

    // Speed stats of se racing car
    [JsonIgnore]
    internal abstract double TimeFrom0to100inSeconds { get; }
    [JsonIgnore]
    internal abstract double TimeFrom100to0inSeconds { get; }

    public double Acceleration => this.MaxVelocity / (TimeFrom0to100inSeconds * 1000);
    public double Deceleration => this.MaxVelocity / (TimeFrom100to0inSeconds * 1000);

    public void Move(DateTime currentTime)
    {
      SpeedAdjustment(currentTime);
      AjustCurrentLaneInMovingDirection();
      AjustLaneInHorizontalDirection(currentTime);
      Positioning(currentTime);
      SetAngle();

      lastUpdated = currentTime;
    }

    private void SetAngle()
    {
      Point start = CurrentLane.Start;
      Point end = CurrentLane.End;

      double xDiff = end.X - start.X;
      double yDiff = end.Y - start.Y;
      Angle = Math.Atan2(yDiff, xDiff) * 180.0 / Math.PI + 90;
      return;
      if (start.X < end.X && start.Y == end.Y)
      {
        Angle = internalAngle + 90;
      }
      else if (start.X > end.X && start.Y == end.Y)
      {
        Angle = internalAngle + 270;
      }
      else if (start.X == end.X && start.Y > end.Y)
      {
        Angle = internalAngle;
      }
      else if (start.X == end.X && start.Y < end.Y)
      {
        Angle = internalAngle + 180;
      }
      else if (start.X > end.X && start.Y < end.Y)
      {
        Angle = internalAngle + 225;
      }
      else if (start.X > end.X && start.Y > end.Y)
      {
        Angle = internalAngle + 315;
      }
      else if (start.X < end.X && start.Y < end.Y)
      {
        Angle = internalAngle + 135;
      }
      else if (start.X < end.X && start.Y > end.Y)
      {
        Angle = internalAngle + 45;
      }

    }

    public void Update(IVehicleAiUpdate vehicleAiUpdateObject)
    {
      Console.WriteLine($"{Id.ToString()} got an update.");
    }


    internal void Accelerate(DateTime now)
    {
      Velocity += (now - lastUpdated).TotalMilliseconds * Acceleration;
      if(Velocity > MaxVelocity)
      {
        Velocity = MaxVelocity;
      }
    }
    internal void Decelerate(DateTime now)
    {
      Velocity -= (now - lastUpdated).TotalMilliseconds * Deceleration;
      if(Velocity < 0)
      {
        Velocity = 0;
      }
    }

    internal void Positioning(DateTime now)
    {
      var start = CurrentLane.Start;
      var end = CurrentLane.End;
     
      var laneAsVec = (new Vector(CurrentLane.End.X, CurrentLane.End.Y) - new Vector(CurrentLane.Start.X, CurrentLane.Start.Y));
      normalVecOnLane = laneAsVec / laneAsVec.Length;

      Double t = (now - lastUpdated).TotalMilliseconds;
      // Distance traveled since last update
      // s = vi*t + 1/2 * a * t²
      Double distTraveled = ((LastVelocity / 3.6)) * t + 0.5 * Delta * (t * t);
      // 450 is the massstab
      PositionOnLane += (Math.Abs(distTraveled) / 450);

      var length = getLenghtOfLane(CurrentLane);

      if (PositionOnLane > length)
      {
        PositionOnLane = length;
      }

      //normalVecOnLane.X = Math.Abs(normalVecOnLane.X);
      //normalVecOnLane.Y = Math.Abs(normalVecOnLane.Y);

      Vector newPos = new Vector(CurrentLane.Start.X, CurrentLane.Start.Y) + normalVecOnLane * PositionOnLane;
      internalAngle = Math.Atan2(newPos.Y - PositionOnGrid.Item1, newPos.X - PositionOnGrid.Item2);

      PositionOnGrid = (newPos.X, newPos.Y);

    }

    internal void SpeedAdjustment(DateTime now)
    {
      LastVelocity = Velocity;
      IEnumerable<IVehicle> vehiclesInSegment = ContainingVehiclePool.getVehiclesForSegment(this.CurrentLane);

      // Selects all Vehicles in the same lane which head into the same direction, defined by its internalAngle.
      IEnumerable<IVehicle> vehiclesInSameDirection = vehiclesInSegment
          .Where(x => x.CurrentLane == this.CurrentLane)
          .Where(x => x.PositionOnLane > this.PositionOnLane)
          .Where(x => ((Vehicle)x).internalAngle >= (this.internalAngle - 10) || ((Vehicle)x).internalAngle <= (this.internalAngle + 10))
          .Where(x => x.Id != this.Id);
      
      if(ContainingVehiclePool.TrafficLights != null)
      {
        if (checkIfTrafficLightIsRedOnLane())
        {
          Decelerate(DateTime.Now);
          return;
        }
      }

      if (vehiclesInSameDirection.Count() == 0)
      {
        if (Velocity < MaxVelocity)
        {
          Accelerate(now);
        }
        return;
      }
      if(Velocity == 0 && !checkIfTrafficLightIsRedOnLane())
      {
        Accelerate(now);
        return;
      }

      List<KeyValuePair<double, IVehicle>> distancesToVehicles = new List<KeyValuePair<double, IVehicle>>();

      foreach (IVehicle v in vehiclesInSameDirection)
      {
        Vector vector = (new Vector(v.PositionOnGrid.Item1, v.PositionOnGrid.Item2)
            - new Vector(this.PositionOnGrid.Item1, this.PositionOnGrid.Item2));

        distancesToVehicles.Add(new KeyValuePair<double, IVehicle>(vector.Length, v));
      }

      var itemWithLeastDistance = distancesToVehicles.Where((x) => x.Key == distancesToVehicles.Min(y => y.Key)).FirstOrDefault().Value;
      double refSpeed = itemWithLeastDistance.Velocity;

      if (ContainingVehiclePool.TrafficLights != null)
      {
        if (checkIfTrafficLightIsRedOnLane())
        {
          Decelerate(now);
        }
        else
        {
          if (distancesToVehicles.Min(x => x.Key) < 8 * this.Length)
          {
            // Decelerate if vehicle is too close to vehicle infront of it
            Decelerate(now);
          }
          else
          {
            // Decelerate if the vehicle infront is slower the this.vehicle
            if (Velocity > refSpeed)
            {
              Decelerate(now);
            }
            else
            {
              Accelerate(now);
            }
          }
        }
      }
      else
      {
        if (distancesToVehicles.Min(x => x.Key) < 8 * this.Length)
        {
          // Decelerate if vehicle is too close to vehicle infront of it
          Decelerate(now);
        }
        else
        {
          // Decelerate if the vehicle infront is slower the this.vehicle
          if (Velocity > refSpeed)
          {
            Decelerate(now);
          }
          else
          {
            Accelerate(now);
          }
        }
      }


      Delta = Velocity - LastVelocity;
    }
    private bool checkIfTrafficLightIsRedOnLane()
    {
      List<TrafficLight> trafficlight = new List<TrafficLight>();

      foreach(var t in ContainingVehiclePool.TrafficLights)
      {
        var pos = t.Positions
          .Where(x => x.X == CurrentLane.End.X && x.Y == CurrentLane.End.Y);

        if(pos.Count() != 0)
        {
          trafficlight.Add(t);
        }
      }
      

      if(trafficlight == null)
      {
        return false;
      }

      if (trafficlight.Count() == 0)
      {
        return false;
      }
      else
      {
        var light = trafficlight.FirstOrDefault();
        if (light == null)
        {
          return false;
        }

        var state = light.State;
        // Decelrate a little before the red light.
        // in this case 40% before the lane ends.
        if (state == TrafficLightState.Red)
        {
          return true;
        }
        else
        {
          // Red light is on but still a little away
          // no need to break now
          return false;
        }
      }
    }

    internal void AjustLaneInHorizontalDirection(DateTime dateTime)
    {
      if (CurrentLane.RightLane == null && CurrentLane.LeftLane == null)
      {
        return;
      }
      if (!canChooseLaneFreely(this.CurrentLane))
      {
        ILaneEx mergingLane = getCorrectLaneForMerging(this.CurrentLane, this.Route);
        switchLanes(mergingLane);
        return;
      }
      Random random = new Random();
      int randomNumber = random.Next(0, 100);

      if (randomNumber > 40)
      {
        return;
      }
      ILaneEx laneToSwicthTo = null;
      double maxMean = 0;
      double meanSpeedCurrentLane = 0f;

      try
      {
        meanSpeedCurrentLane = ContainingVehiclePool.getVehiclesForLane(this.CurrentLane).Average(x => x.Velocity) 
          * CurrentLane.Weight;
      }
      catch
      {
        meanSpeedCurrentLane = MaxVelocity * CurrentLane.Weight;
      }


      double boundary = meanSpeedCurrentLane * (1 + this.DecisionBoundary);
      foreach (var kvp in getMeanSpeedsOfNeighbourLanes(this.CurrentLane))
      {
        if (kvp.Value > boundary && kvp.Value > maxMean)
        {
          maxMean = kvp.Value;
          laneToSwicthTo = kvp.Key;
        }
      }

      if (laneToSwicthTo == null)
      {
        // Get the hella out of this function
        // if no other lane is significant faster then the
        // current one.
        return;
      }
      if (checkIfLaneIsFree(laneToSwicthTo))
      {
        switchLanes(laneToSwicthTo);
      }
    }

    private ILaneEx getCorrectLaneForMerging(ILaneEx currentLane, IEnumerable<IEnumerable<ILaneEx>> route)
    {
      if (route == null || !route.Any())
      {
        Random rnd = new Random();
        return currentLane.NextLanes.OrderBy(x => rnd.Next()).FirstOrDefault();
      }
      int index = (route as List<List<ILaneEx>>).FindIndex(a => a.Contains(currentLane));

      // get lanes where we want to be the lane after
      int lanesAheadIndex = index + 2;
      IEnumerable<ILaneEx> lanesAhead = route.ElementAt(lanesAheadIndex);

      if (currentLane.NextLanes.FirstOrDefault()?.NextLanes == lanesAhead)
      {
        // We are already on the right lane for merging
        return currentLane;
      }

      ILaneEx correctLaneForMerging = null;

      if (currentLane.RightLane != null)
      {
        correctLaneForMerging = checkLaneToTheRight(currentLane.RightLane, lanesAhead);
      }

      if (correctLaneForMerging == null && currentLane.LeftLane != null)
      {
        correctLaneForMerging = checkLaneToTheLeft(currentLane.LeftLane, lanesAhead);
      }

      if (correctLaneForMerging == null)
      {
        throw new Exception("FATAL ERROR: Cant find correct lane to merge to");
      }
      return correctLaneForMerging;
    }

    private ILaneEx checkLaneToTheRight(ILaneEx laneToTheRight, IEnumerable<ILaneEx> lanesAhead)
    {
      ILaneEx choosenLane = null;

      if (laneToTheRight.NextLanes.FirstOrDefault()?.NextLanes == lanesAhead)
      {
        return laneToTheRight;
      }
      if (laneToTheRight.RightLane != null)
      {
        choosenLane = checkLaneToTheRight(laneToTheRight.RightLane, lanesAhead);
      }
      return choosenLane;
    }

    private ILaneEx checkLaneToTheLeft(ILaneEx laneToTheLeft, IEnumerable<ILaneEx> lanesAhead)
    {
      ILaneEx choosenLane = null;

      if (laneToTheLeft.NextLanes.FirstOrDefault()?.NextLanes == lanesAhead)
      {
        return laneToTheLeft;
      }
      if (laneToTheLeft.LeftLane != null)
      {
        choosenLane = checkLaneToTheLeft(laneToTheLeft.LeftLane, lanesAhead);
      }
      return choosenLane;
    }

    private bool canChooseLaneFreely(ILaneEx currentLane)
    {
      if (currentLane.IsPartOfCrossing)
      {
        return false;
      }
      if (currentLane.NextLanes.Count() != 0)
      {
        if (currentLane.NextLanes.FirstOrDefault()?.IsPartOfCrossing ?? false)
        {
          return false;
        }

        //if(currentLane.NextLanes.SingleOrDefault().NextLanes.Count() != 0)
        //{
        //    // Please dont judge me for this if clause
        //    // it checks if the lane after is on a crossing
        //    // car should rather change lane then look for the fastest in this case
        //    if (currentLane.NextLanes.SingleOrDefault().NextLanes.SingleOrDefault().IsPartOfCrossing)
        //    {
        //        return false;
        //    }
        //}

      }
      //no crossing ahead
      return true;
    }

    private void switchLanes(ILaneEx newLane, bool resetPosOnLane = false)
    {
      //Maybe make transistion smoother
      CurrentLane = newLane;
      if (resetPosOnLane)
      {
        PositionOnLane = 0;
      }
    }

    private bool checkIfLaneIsFree(ILaneEx lane)
    {
      // Checks the mirror if some cars are behind us on the lane we want to merge to.
      // It trys to leave some space to the cars behind
      var listOfCarsInMirror = ContainingVehiclePool.getVehiclesForLane(lane)
          .Where(x => x.PositionOnLane <= this.PositionOnLane)
          .Where(x => x.PositionOnLane >= (this.PositionOnLane - (this.Length * 2)));
      return (listOfCarsInMirror.Count() == 0) ? true : false;
    }


    private Dictionary<ILaneEx, double> getMeanSpeedsOfNeighbourLanes(ILaneEx lane)
    {
      var dict = new Dictionary<ILaneEx, double>();
      if(lane.LeftLane != null)
      {
        try
        {
          dict.Add(
            lane.LeftLane, (ContainingVehiclePool.getVehiclesForLane(lane.LeftLane).Average(x => x.Velocity)
            * lane.LeftLane.Weight));
        }
        catch
        {
          dict.Add(
            lane.LeftLane, MaxVelocity * lane.LeftLane.Weight
            );
        }
      }
      if(lane.RightLane != null)
      {
        try
        {
          dict.Add(
            lane.RightLane, (ContainingVehiclePool.getVehiclesForLane(lane.RightLane).Average(x => x.Velocity)
            * lane.RightLane.Weight));
        }
        catch
        {
          dict.Add(
            lane.RightLane, MaxVelocity * lane.RightLane.Weight
            );
        }
      }
      return dict;
    }

    internal void AjustCurrentLaneInMovingDirection()
    {
      if (PositionOnLane < getLenghtOfLane(CurrentLane))
      {
        // Still on track nothing to do
        return;
      }

      if (PositionOnLane >= getLenghtOfLane(CurrentLane))
      {
        //Vehicle is at the end
        if (CurrentLane.NextLanes.Count == 0)
        {
          GoodByeVehicle(this);
        }
        // If the lane has just one successor stay on the lane
        if (CurrentLane.NextLanes.Count == 1)
        {
          switchLanes(CurrentLane.NextLanes.FirstOrDefault(), true);
        }
        // Further merging for follwóing the route is taken into consideration.
        if (CurrentLane.NextLanes.Count > 1)
        {
          switchLanes(getCorrectLaneForMerging(CurrentLane.NextLanes.ElementAt(0), Route), true);
        }
      }
    }


    private double getLenghtOfLane(ILaneEx lane)
    {
      Vector vec = new Vector(lane.End.X, lane.End.Y) - new Vector(lane.Start.X, lane.Start.Y);
      return vec.Length;
    }

    internal void GoodByeVehicle(IVehicle vehicle)
    {
      ContainingVehiclePool.RemoveVehicle(vehicle);
    }

  }
}
