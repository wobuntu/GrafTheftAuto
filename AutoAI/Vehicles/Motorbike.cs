﻿using System;
using Core.Classes;
using Core.Interfaces;
using Newtonsoft.Json;

namespace AutoAI.Vehicles
{
  public class Motorbike : Vehicle
  {
    public Motorbike(VehiclePool pool, Tuple<ILaneEx, ILaneEx> startEndLaneTuple) : base(pool, startEndLaneTuple)
    {
      CurrentLane = startEndLaneTuple.Item1;
      PositionOnLane = 0;
      LastVelocity = 0;
      lastUpdated = DateTime.Now;
    }

    public override double MaxVelocity => 100;
    internal override double TimeFrom0to100inSeconds => 6;
    internal override double TimeFrom100to0inSeconds => 2.5;
    public override double Length => 60;
    public override double Width => 4;
    public override VehicelType VehicleType => VehicelType.Motorbike;
  }
}
