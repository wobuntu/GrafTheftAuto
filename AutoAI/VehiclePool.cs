﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Interfaces;
using Core.Logging;
using Core.Classes;
using AutoAI.Vehicles;
using System.Net;
using System.Threading;
using Core;

namespace AutoAI
{
  public class VehiclePool : IVehiclePool
  {
    public VehiclePool()
    {
      Feed.Report("Note: Waiting for a simulation instance to register itself as the controller of this program AFTER the setup is done.", LogLvl.InfoHighlighted);
    }

    public ISimulation stub;
    object _vehicleLock = new object();
    object _trafficLightLock = new object();

    #region Helper methods
    private bool _updateTrafficLightsIsExecuting = false;
    private void updateTrafficLights()
    {
      lock (_trafficLightLock)
      {
        if (!_updateTrafficLightsIsExecuting)
        {
          _updateTrafficLightsIsExecuting = true;
        }
        else
        {
          return;
        }
      }

      new Thread(() =>
      {
        while (true)
        {
          if (simulationPaused)
          {
            Thread.Sleep(200);
            continue;
          }

          try
          {
            // Get the traffic light states
            var states = stub.GetCurrentTrafficLightStates();

            // Update the traffic lights
            foreach (var light in this.TrafficLights)
            {
              if (states.ContainsKey(light.IntervalId))
              {
                light.State = states[light.IntervalId];
              }
              else
              {
                light.State = TrafficLightState.Error;
              }
            }
          }
          catch
          {
            foreach (var light in this.TrafficLights)
            {
              light.State = TrafficLightState.Error;
            }
          }

          Thread.Sleep(1000);
        }
      }).Start();
    }
    
    private bool _simuIsExecuting = false;
    public void simulate()
    {
      lock (_vehicleLock)
      {
        if (!_simuIsExecuting)
        {
          _simuIsExecuting = true;
        }
        else
        {
          return;
        }
      }

      new Thread(() =>
      {
        while (true)
        {
          if (simulationPaused)
          {
            Thread.Sleep(200);
            continue;
          }

          var diffSinceLastUpdate = (DateTime.Now - StartInterval).TotalMilliseconds;
          if (diffSinceLastUpdate > _vehiclesInitInerval && CurrentVehicles.Count < _vehiclesMaximum)
          {
            var vehicleTypes = (VehicelType[])Enum.GetValues(typeof(VehicelType));
            var typeForCreation = vehicleTypes[rnd.Next(0, vehicleTypes.Length - 1)];

            // For now just adding cars.
            AddVehicleToPool(typeForCreation);
            StartInterval = DateTime.Now;
          }

          lock (_vehicleLock)
          {
            for (int i = CurrentVehicles.Count - 1; i >= 0; i--)
            {
              if (CurrentVehicles[i] != null)
              {
                CurrentVehicles[i].Move(DateTime.Now);
              }
            }
          }

          Thread.Sleep(5);
        }
      }).Start();
    }
    #endregion


    #region private variables
    private static uint _vehiclesMaximum = 20;
    private static uint _vehiclesInitInerval = 1000;
    private static uint _trafficLightUpdateIntervall = 3000;

    public DateTime StartInterval;
    private Random rnd = new Random();
    public Map Map;

    private bool simulationPaused = false;
    public List<TrafficLight> TrafficLights;
    #endregion


    #region public variables
    public List<IVehicle> CurrentVehicles { get; private set; } = new List<IVehicle>();

    public List<VehicleWrapperDto> CurrentVehiclesAsDto
    {
      get
      {
        var dtos = new List<VehicleWrapperDto>();
        lock (_vehicleLock)
        {
          foreach (var v in CurrentVehicles)
          {
            dtos.Add(new VehicleWrapperDto()
            {
              Delta = v.Delta,
              Width = v.Width,
              Length = v.Length,
              Rotation = v.Angle,
              Velocity = v.Velocity,
              PositionOnGrid = new System.Windows.Point(v.PositionOnGrid.Item1, v.PositionOnGrid.Item2),
              VehicleType = v.VehicleType,
              Id = v.Id
            });
          }
        }

        return dtos;
      }
    }
    #endregion


    #region public methods
    private IPAddress existingIp = null;
    public bool TryRegisterSimulation(string ipString)
    {
      if (!IPAddress.TryParse(ipString, out var ip))
      {
        Feed.Report($"Invalid IP, registration of simulation failed.", LogLvl.Error);
        return false;
      }

      if (this.existingIp != null && !this.existingIp.Equals(ip))
      {
        Feed.Report($"Invalid simulation registration attempt was made for simulation instance on {existingIp}: The simulation IP was already assigned by another ip.", LogLvl.Warn);
        return false;
      }
      else if (this.existingIp != null && this.existingIp.Equals(ip))
      {
        return true;
      }

      this.existingIp = ip;

      StubHelper<ISimulation> stubHelper = null;
      while (stubHelper == null || this.stub == null || this.Map == null)
      {
        stubHelper = StubHelper<ISimulation>.TrySetup(ip);
        this.stub = stubHelper?.TryGetStub();
        this.Map = this.stub?.GetCurrentMap();
        if (this.Map == null)
        {
          Feed.Report("Could not fetch the map from the simulation, trying again after a delay of one second.", LogLvl.Warn);
          Task.Delay(1000);
          continue;
        }

        Feed.Report($"A simulation instance on {ipString} was registered.", LogLvl.InfoHighlighted);

        this.StartInterval = DateTime.Now;
        lock (_trafficLightLock)
        {
          this.TrafficLights = this.Map.GetAllTrafficLights();
        }
      }

      updateTrafficLights();
      simulate();

      return true;
    }

    public void PauseSimulation()
    {
      Feed.Report($"PAUSING SIMULATION", LogLvl.InfoHighlighted);
      simulationPaused = true;
    }

    public void ResumeSimulation()
    {
      Feed.Report($"RESUMING SIMULATION", LogLvl.InfoHighlighted);
      StartInterval = DateTime.Now;
      simulationPaused = false;
    }

    public void UpdateVehicle(IVehicleAiUpdate aiUpdateObject)
    {
      // TODO: Keine Referenzen auf diese Methode? Noch benötigt?
      IVehicle vehicle = null;
      lock (_vehicleLock)
      {
        vehicle = CurrentVehicles.Where(v => v.Id == aiUpdateObject.VehicleId).FirstOrDefault();
      }

      vehicle?.Update(aiUpdateObject);
    }


    public void SetVehicleMaximum(uint vehicleMaximum)
    {
      _vehiclesMaximum = vehicleMaximum;
    }

    public uint GetVehicleMaximum() => _vehiclesMaximum;

    public void SetVehicleInitInterval(uint initInterval)
    {
      _vehiclesInitInerval = initInterval;
    }

    public uint GetVehicleInitInterval() => _vehiclesInitInerval;
    #endregion


    #region internal and private methods
    DateTime lastTimeAddedVehicle = DateTime.MinValue;
    private void AddVehicleToPool(VehicelType vType)
    {
      var diff = (DateTime.Now - lastTimeAddedVehicle).TotalMilliseconds;
      if (diff < Config.UI.MillisecondsUntilAdd)
      {
        return;
      }

      lastTimeAddedVehicle = DateTime.Now;

      IVehicle v = null;
      if (Map == null)
      {
        Feed.Report("Map is null, cant create any vehicle.", LogLvl.Warn);
        return;
      }

      switch (vType)
      {
        case VehicelType.Car:
          v = new Car(this, getStartAndEndPoint(Map.Tiles));
          break;
        case VehicelType.Truck:
          v = new Truck(this, getStartAndEndPoint(Map.Tiles));
          break;
        case VehicelType.Bus:
          v = new Bus(this, getStartAndEndPoint(Map.Tiles));
          break;
        case VehicelType.Motorbike:
          v = new Motorbike(this, getStartAndEndPoint(Map.Tiles));
          break;
        case VehicelType.Tractor:
          v = new Tractor(this, getStartAndEndPoint(Map.Tiles));
          break;
        default:
          Feed.Report($"Vehicle of type '{vType}' could not be created.", LogLvl.Warn);
          return;
      }

      lock (_vehicleLock)
      {
        CurrentVehicles.Add(v);
        Feed.Report($"Vehicle of type '{vType}' added.");
      }
    }


    internal void RemoveVehicle(IVehicle vehicle)
    {
      if (vehicle == null)
      {
        return;
      }

      lock (_vehicleLock)
      {
        if (CurrentVehicles.Contains(vehicle))
        {
          Feed.Report($"Vehicle of type '{vehicle.VehicleType}' added.");
          CurrentVehicles.Remove(vehicle);
        }
      }
    }


    private Tuple<ILaneEx, ILaneEx> getStartAndEndPoint(IEnumerable<IEnumerable<ITile>> lists)
    {
      // TODO: Methode bereits in Map enthalten
      List<ILaneEx> possibleStartPoints = new List<ILaneEx>();
      List<ILaneEx> possibleEndPoints = new List<ILaneEx>();

      List<ILaneEx> allLanes = new List<ILaneEx>();

      foreach (var list in lists)
      {
        foreach (var tile in list)
        {
          if (tile.LanesBackward != null)
          {
            foreach (var lane in tile.LanesBackward)
            {
              allLanes.Add(lane);
            }
          }
          if (tile.LanesForward != null)
          {
            foreach (var lane in tile.LanesForward)
            {
              allLanes.Add(lane);
            }
          }
        }
      }

      possibleEndPoints = allLanes.Where(x => x.NextLanes.Count == 0).ToList();

      foreach (var lane in allLanes)
      {
        bool hasPrevious = false;
        foreach (var laneToCompare in allLanes)
        {
          if (hasPrevious)
          {
            break;
          }
          if (laneToCompare.End == lane.Start && laneToCompare != lane)
          {
            hasPrevious = true;
          }
        }
        if (!hasPrevious)
        {
          possibleStartPoints.Add(lane);
        }
        hasPrevious = true;
      }

      int start = rnd.Next(possibleStartPoints.Count);
      int end = rnd.Next(possibleEndPoints.Count);

      return new Tuple<ILaneEx, ILaneEx>(possibleStartPoints.ElementAt(start), possibleEndPoints.ElementAt(end));
    }

    internal IEnumerable<IVehicle> getVehiclesForSegment(ILaneEx lane)
    {
      List<IVehicle> result = new List<IVehicle>();
      result = getVehicelesForLeftRecursive(result, lane);
      result = getVehicelesForRightRecursive(result, lane);
      return result;
    }

    private List<IVehicle> getVehicelesForLeftRecursive(List<IVehicle> vehicles, ILaneEx lane)
    {
      foreach (var v in CurrentVehicles.Where(x => x.CurrentLane == lane))
      {
        vehicles.Add(v);
      }
      if (lane.LeftLane != null)
      {
        vehicles = getVehicelesForLeftRecursive(vehicles, lane.LeftLane);
      }
      return vehicles;
    }

    private List<IVehicle> getVehicelesForRightRecursive(List<IVehicle> vehicles, ILaneEx lane)
    {
      foreach (var v in CurrentVehicles.Where(x => x.CurrentLane == lane))
      {
        vehicles.Add(v);
      }
      if (lane.RightLane != null)
      {
        vehicles = getVehicelesForRightRecursive(vehicles, lane.RightLane);
      }
      return vehicles;
    }

    internal IEnumerable<IVehicle> getVehiclesForLane(ILaneEx lane)
    {
      IEnumerable<IVehicle> result = new List<IVehicle>();
      result = CurrentVehicles.Where(x => x.CurrentLane == lane);
      return result;
    }

  }
  #endregion
}
