﻿using Core.Classes;
using Core.ComponentDiscovery;
using Core.Interfaces;
using Core.Logging;
using Core.Net.RemoteInvocation;
using Core.Net.Websockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoAI
{
  internal class VehicleAIStartup
  {
    static IVehiclePool component;

    private static void Main(string[] args)
    {
      // Register for logging outputs
      Feed.Subscribe(new ConsoleFeedPrinter(), LogLvl.Error | LogLvl.Warn | LogLvl.Info );
      //Feed.Subscribe(new NLogFeedPrinter(true), LogLvl.Error);

      const string componentName = nameof(IVehiclePool);
      Console.Title = componentName;
      component = new VehiclePool();
      UInt16 port = Core.Config.NetworkDiscovery.Ports[componentName];
      string serverUri = $"http://+:80/GrafTheftAuto/{componentName}/";

      Feed.Report($"{componentName} starting up...");

      try
      {
        // Register the component to be found in the network
        var discovery = DiscoveryRegistration.Create(componentName, port);
        Feed.Report($"{componentName} was registered for network discovery on port {port}.");

        // Register the RMI handler, so that other foreign components can call methods from this one
        var rmiHandler = new RemoteInvocationHandler(new Server(serverUri));
        rmiHandler.RegisterLocalTarget(componentName, component);
        Feed.Report($"{componentName} was registered as RMI server on {serverUri}");
      }
      catch (Exception ex)
      {
        Feed.Report($"An exception occurred: {ex}", LogLvl.Error);
        Console.WriteLine("Press any key to exit");
        Console.ReadLine();
        return;
      }

      Feed.Report($"Setup done. Press Ctrl+C to quit.", LogLvl.InfoHighlighted);
      while (true)
      {
        Task.Delay(200).Wait();
      }
    }
  }
}
