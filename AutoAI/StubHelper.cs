﻿using Core;
using Core.ComponentDiscovery;
using Core.Logging;
using Core.Net.RemoteInvocation;
using Core.Net.Stubs;
using Core.Net.Websockets;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Net;

namespace AutoAI
{
  public class StubHelper<T>
  {
    private StubHelper() { }

    static StubHelper<T> typedInstance;
    static object _lock = new object();

    StubRemoteInvocationBehaviorProvider _behavior = null;
    T _stub = default(T);
    string _url = null;

    public T TryGetStub()
    {
      try
      {
        lock(_lock)
        {
          return setupStub();
        }
      }
      catch
      {
        return default(T);
      }
    }

    public static StubHelper<T> TrySetup(IPAddress ip)
    {
      var type = typeof(T);
      var newUrl = $"ws://{ip}:80/GrafTheftAuto/{type.Name}/";
      if (typedInstance != null && newUrl == typedInstance._url)
      {
        return typedInstance;
      }

      Feed.Report($"Searching for {type.Name}...");
      var foundEntries = DiscoveryService.Find(type.Name, Config.NetworkDiscovery.Ports[type.Name], Config.NetworkDiscovery.ReceiveTimeout);
      var foundNum = foundEntries.Count();

      if (foundNum == 0)
      {
        Feed.Report("* Couldn't find any matches.");
        return null;
      }
      else
      {
        Feed.Report($"* Found the following instances of {type.Name}:");
        int i = 0;
        foreach (var entry in foundEntries)
        {
          Feed.Report($"  {i++}: {entry.Address}");
        }

        var found = foundEntries.Where((x) => x.Address.Equals(ip)).FirstOrDefault();
        if (found == null)
        {
          Feed.Report($"* None of the found instances is located on the desired address {ip}.");
          return null;
        }
      }

      typedInstance = new StubHelper<T>()
      {
        _url = newUrl
      };

      return typedInstance;
    }

    private T setupStub()
    {
      Type type = typeof(T);
      if (!(_stub is T stub))
      {
        // Only compile the stub once
        var behavior = new StubRemoteInvocationBehaviorProvider(null);
        _behavior = behavior;

        Feed.Report($"Compiling stub for {type.Name}...");
        _stub = StubBuilder.CompileStubFor<T>(behavior);
        Feed.Report($"Compilation of {type.Name}-stub successful.");
      }

      // Try to start the client for this stub
      if (_behavior.TransportClient == null)
      {
        Feed.Report($"Trying to setup a client for {type.Name} on {_url}...");
        try
        {
          _behavior.TransportClient = new Client(_url);
          Feed.Report($"Client for {type.Name}-stub successfully initialized.");
        }
        catch (Exception ex)
        {
          Feed.Report($"Error while initializing client for {type.Name}-stub.", ex, LogLvl.Error);
          return default(T);
        }
      }

      return _stub;
    }
  }
}
