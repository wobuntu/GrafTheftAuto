﻿using Core.Classes;
using Core.Interfaces;
using Core.Logging;
using Core.Net.RemoteInvocation;
using Core.Net.Stubs;
using Core.Net.Websockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace Simulation
{
  public class Simulation : ISimulation
  {
    public Simulation(string mapProviderUri,
      string trafficLightServiceUri,
      string vehicleAiServiceUri)
    {
      this.urls[typeof(IMapProviderService)] = mapProviderUri;
      this.urls[typeof(ITrafficLightService)] = trafficLightServiceUri;
      this.urls[typeof(IVehiclePool)] = vehicleAiServiceUri;
    }


    #region Private variables
    Guid trafficLightRegistration = Guid.Empty;
    Map lastMap = null;

    private Dictionary<Type, StubRemoteInvocationBehaviorProvider> behaviors
      = new Dictionary<Type, StubRemoteInvocationBehaviorProvider>();

    private Dictionary<Type, string> urls = new Dictionary<Type, string>();

    private Dictionary<Type, object> stubs = new Dictionary<Type, object>();
    #endregion


    #region Connection helpers
    private T setupStub<T>()
    {
      Type type = typeof(T);
      if (!stubs.ContainsKey(type) || !(stubs[type] is T stub))
      {
        // Only compile the stub once
        var behavior = new StubRemoteInvocationBehaviorProvider(null);
        behaviors[type] = behavior;

        Feed.Report($"Compiling stub for {type.Name}...");
        stubs[type] = StubBuilder.CompileStubFor<T>(behavior);
        Feed.Report($"Compilation of {type.Name}-stub successful.");
      }

      // Try to start the client for this stub
      if (behaviors[type].TransportClient == null)
      {
        Feed.Report($"Trying to setup a client for {type.Name} on {urls[type]}...");
        try
        {
          behaviors[type].TransportClient = new Client(urls[type]);
          Feed.Report($"Client for {type.Name}-stub successfully initialized.");
        }
        catch (Exception ex)
        {
          Feed.Report($"Error while initializing client for {type.Name}-stub.", ex, LogLvl.Error);
          throw;
        }
      }

      return (T)stubs[type];
    }
    #endregion


    #region Interface implementation
    public Map GetCurrentMap()
    {
      try
      {
        Feed.Report("Incoming request for receiving the current map received.");
        lastMap = setupStub<IMapProviderService>().GetMap();
        Feed.Report("Returning map.");
        return lastMap;
      }
      catch
      {
        return null;
      }
    }

    public List<TrafficLight> GetCurrentTrafficLights()
    {
      try
      {
        if (lastMap == null)
        {
          lastMap = setupStub<IMapProviderService>().GetMap();
        }

        Feed.Report("Incoming request for receiving the current traffic lights received.");
        var lights = lastMap?.GetAllTrafficLights();
        Feed.Report("Returning current traffic lighs.");
        return lights;
      }
      catch
      {
        return null;
      }
    }

    public IDictionary<int, TrafficLightState> GetCurrentTrafficLightStates()
    {
      try
      {
        if (lastMap == null)
        {
          lastMap = GetCurrentMap();
        }

        var stub = setupStub<ITrafficLightService>();
        if (trafficLightRegistration == Guid.Empty)
        {
          trafficLightRegistration = stub.Register(lastMap.GetAllTrafficLights());
        }

        // Check if the service was disconnected in the meantime
        // (if an empty dictionary is returned)
        var states = stub.GetStates(trafficLightRegistration);
        if (states != null && states.Count == 0)
        {
          var lastLights = lastMap.GetAllTrafficLights();
          if (lastLights != null && lastLights.Count == 0)
          {
            // everything is fine, no more lights than zero expected
            Feed.Report("Returning traffic light states.");
            return states;
          }

          // if this line is reached, then the traffic light service was down in the meantime
          // and requires us to register the lights again
          trafficLightRegistration = stub.Register(lastLights);
          states = stub.GetStates(trafficLightRegistration);
          Feed.Report("Returning traffic light states.");
          return states;
        }
        else if (states == null)
        {
          // Traffic light service may be down
          if (lastMap == null)
          {
            Feed.Report($"{nameof(ITrafficLightService)} may be down, returning empty values.", LogLvl.Warn);
            return new Dictionary<int, TrafficLightState>();
          }

          states = lastMap.GetAllTrafficLights()?.Select(x => x.IntervalId).Distinct().ToDictionary(x => x, x => TrafficLightState.Error);
          if (states == null)
          {
            Feed.Report($"{nameof(ITrafficLightService)} may be down, returning empty values.", LogLvl.Warn);
            return new Dictionary<int, TrafficLightState>();
          }
        }

        Feed.Report("Returning traffic light states.");
        return states;
      }
      catch
      {
        trafficLightRegistration = Guid.Empty;

        // Traffic light service may be down
        if (lastMap == null)
        {
          Feed.Report($"{nameof(ITrafficLightService)} may be down, returning empty values.", LogLvl.Warn);
          return new Dictionary<int, TrafficLightState>();
        }

        var states = lastMap.GetAllTrafficLights()?.Select(x => x.IntervalId).Distinct().ToDictionary(x => x, x => TrafficLightState.Error);
        if (states == null)
        {
          Feed.Report($"{nameof(ITrafficLightService)} may be down, returning empty values.", LogLvl.Warn);
          return new Dictionary<int, TrafficLightState>();
        }

        Feed.Report($"{nameof(ITrafficLightService)} may be down, returning error states for traffic lights.", LogLvl.Warn);
        return states;
      }
    }

    // TODO: Remove field if vehicle stuff works
    public List<VehicleWrapperDto> GetVehicles()
    {
      List<VehicleWrapperDto> vehicleList = new List<VehicleWrapperDto>();
      try
      {
        var stub = setupStub<IVehiclePool>();
        var ip = Core.Net.Utils.GetLocalIPAddress();
        if (ip == null)
        {
          Feed.Report($"Cannot connect to the {nameof(IVehiclePool)} instance, since the local ip for registration is unknown.", LogLvl.Error);
          return new List<VehicleWrapperDto>();
        }
        if (!stub.TryRegisterSimulation(ip.ToString()))
        {
          Feed.Report($"Registering the simulation on the {nameof(IVehiclePool)} failed, it might be down.", LogLvl.Warn);
        }
        
        stub = setupStub<IVehiclePool>();
        return stub.CurrentVehiclesAsDto;
      }
      catch
      {
        Feed.Report($"{nameof(IVehiclePool)} may be down, returning an empty vehicle list.", LogLvl.Warn);
        return new List<VehicleWrapperDto>();
      }
    }

    public bool PauseSimulation()
    {
      string nextComponent = nameof(IVehiclePool);
      try
      {
        Feed.Report("Incoming request for pausing the simulation received.");
        setupStub<IVehiclePool>().PauseSimulation();
        Feed.Report($"Paused {nameof(IVehiclePool)}.");

        nextComponent = nameof(ITrafficLightService);
        setupStub<ITrafficLightService>().PauseSimulation();
        Feed.Report($"Paused {nameof(ITrafficLightService)}.");

        return true;
      }
      catch
      {
        Feed.Report($"Pausing the simulation failed for component {nextComponent}", LogLvl.Warn);
        return false;
      }
    }

    public bool ResumeSimulation()
    {
      string nextComponent = nameof(IVehiclePool);
      try
      {
        Feed.Report("Incoming request for resuming the simulation received.");
        setupStub<IVehiclePool>().ResumeSimulation();
        Feed.Report($"Resumed {nameof(IVehiclePool)}.");

        nextComponent = nameof(ITrafficLightService);
        setupStub<ITrafficLightService>().ResumeSimulation();
        Feed.Report($"Resumed {nameof(ITrafficLightService)}.");

        return true;
      }
      catch
      {
        Feed.Report($"Resuming the simulation failed for component {nextComponent}", LogLvl.Warn);
        return false;
      }
    }

    public void SetMaximumVehicles(uint maxVehicles)
    {
      try
      {
        Feed.Report("Incoming request for setting the maximum amount of vehicles received.");
        setupStub<IVehiclePool>().SetVehicleMaximum(maxVehicles); // TODO: Error check in IVehiclePool
        Feed.Report("Value for max amount of vehicles set.");
      }
      catch
      {
        Feed.Report($"Failed to set the maximum amount of vehicles.", LogLvl.Warn);
      }
    }

    public uint GetMaximumVehicles()
    {
      try
      {
        Feed.Report("Incoming request for getting the maximum amount of vehicles received.");
        var val = setupStub<IVehiclePool>().GetVehicleMaximum(); // TODO: Error check in IVehiclePool
        Feed.Report("Value for max amount of vehicles fetched: " + val);
        return val;
      }
      catch
      {
        Feed.Report($"Failed to get the interval of traffic lights.", LogLvl.Warn);
        return 0;
      }
    }

    public void SetInitIntervalForVehicles(uint initInterval)
    {
      try
      {
        Feed.Report("Incoming request for setting the init interval of new vehicles received.");
        setupStub<IVehiclePool>().SetVehicleInitInterval(initInterval);// TODO: Error check in IVehiclePool
        Feed.Report("Value for init interval of vehicles set.");
      }
      catch
      {
        Feed.Report($"Failed to set the init interval for new vehicles.", LogLvl.Warn);
      }
    }

    public uint GetInitIntervalForVehicles()
    {
      try
      {
        Feed.Report("Incoming request for getting the init interval of new vehicles received.");
        var val = setupStub<IVehiclePool>().GetVehicleInitInterval();// TODO: Error check in IVehiclePool
        Feed.Report("Value for init interval of vehicles fetched: " + val);
        return val;
      }
      catch
      {
        Feed.Report($"Failed to set the init interval for new vehicles.", LogLvl.Warn);
        return 0;
      }
    }

    public void SetTrafficLightInterval(int interval)
    {
      try
      {
        Feed.Report("Incoming request for setting the interval of traffic lights received.");
        setupStub<ITrafficLightService>().Interval = interval;// TODO: Error check in TrafficLightService
        Feed.Report("Value for init interval of traffic lights set.");
      }
      catch
      {
        Feed.Report($"Failed to set the interval of traffic lights.", LogLvl.Warn);
      }
    }

    public int GetTrafficLightInterval()
    {
      try
      {
        Feed.Report("Incoming request for getting the interval of traffic lights received.");
        var val = setupStub<ITrafficLightService>().Interval;// TODO: Error check in TrafficLightService
        Feed.Report("Value for init interval of traffic lights fetched: " + val);
        return val;
      }
      catch
      {
        Feed.Report($"Failed to set the interval of traffic lights.", LogLvl.Warn);
        return 0;
      }
    }
    #endregion
  }
}
