﻿using Core;
using Core.ComponentDiscovery;
using Core.Interfaces;
using Core.Logging;
using Core.Net.RemoteInvocation;
using Core.Net.Websockets;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Simulation
{
  class SimulationStartup
  {
    static readonly Dictionary<Type, string> requiredComponents = new Dictionary<Type, string>()
    {
      { typeof(IMapProviderService), null },
      { typeof(ITrafficLightService), null },
      { typeof(IVehiclePool), null }
    };

    static void Main(string[] args)
    {
      const string componentName = nameof(ISimulation);
      Console.Title = componentName;
      UInt16 port = Core.Config.NetworkDiscovery.Ports[componentName];
      string serverUri = $"http://+:80/GrafTheftAuto/{componentName}/";

      // Register for logging outputs
      Feed.Subscribe(new ConsoleFeedPrinter(), Config.FeedOptions.DefaultLogLvl);
      //Feed.Subscribe(new NLogFeedPrinter(true), LogLvl.All);

      Feed.Report($"{componentName} starting up...");

      try
      {
        // Before we actually start the server, lets check the prerequisits:
        bool exit = false;
        while (!exit)
        {
          if (setupSimulation())
          {
            break;
          }

          var input = "-";
          while (input.ToLower() != "y" && input.ToLower() != "n" && !string.IsNullOrEmpty(input))
          {
            Feed.Report("Setup failed, try searching again? [Y/n]");
            input = Console.ReadLine();
          }

          if (input == "n")
          {
            return;
          }
        }

        var server = new Server(serverUri);
        var component = new Simulation(
          requiredComponents[typeof(IMapProviderService)],
          requiredComponents[typeof(ITrafficLightService)],
          requiredComponents[typeof(IVehiclePool)]);

        // Register the component to be found in the network
        var discovery = Core.ComponentDiscovery.DiscoveryRegistration.Create(componentName, port);
        Feed.Report($"{componentName} was registered for network discovery on port {port}.");

        // Register the RMI handler, so that other foreign components can call methods from this component
        RemoteInvocationHandler rmiHandler = new RemoteInvocationHandler(new Server(serverUri));
        rmiHandler.RegisterLocalTarget(nameof(ISimulation), component);
        Feed.Report($"{componentName} was registered as an RMI server on {serverUri}");
      }
      catch (Exception ex)
      {
        Feed.Report($"An exception occurred: {ex}", LogLvl.Error);
        Console.WriteLine("Press any key to exit");
        Console.ReadLine();
        return;
      }

      Feed.Report($"Setup done. Press enter to quit.", LogLvl.InfoHighlighted);
      Console.ReadLine();
    }


    static bool setupSimulation()
    {
      Feed.Report("Searching for required components in the network...");
      foreach (var componentType in requiredComponents.Keys.ToArray())
      {
        Feed.Report($"Searching for {componentType.Name}...");
        var foundEntries = DiscoveryService.Find(componentType.Name, Config.NetworkDiscovery.Ports[componentType.Name], Config.NetworkDiscovery.ReceiveTimeout);
        var foundNum = foundEntries.Count();

        if (foundNum == 0)
        {
          Feed.Report("* Couldn't find any matches.");
          return false;
        }
        else if (foundNum == 1)
        {
          Feed.Report($"* Found one match, which will be used: {foundEntries.First().Address}");
          requiredComponents[componentType]
            = $"ws://{foundEntries.First().Address}:80/GrafTheftAuto/{componentType.Name}/";
        }
        else
        {
          Feed.Report($"* Found more than one matches, which one shall be used? [Enter for 0]");
          int i = 0;
          foreach (var entry in foundEntries)
          {
            Feed.Report($"  {i++}: {entry.Address}");
          }
          while (requiredComponents[componentType] == null)
          {
            var input = Console.ReadLine();
            int selection = 0;
            if ((input == String.Empty || int.TryParse(input, out selection)) && selection < i)
            {
              Feed.Report($"* Using {foundEntries.Skip(selection).First().Address}");
              requiredComponents[componentType]
                = $"ws://{foundEntries.Skip(selection).First().Address}:80/GrafTheftAuto/{componentType.Name}/";
            }
            else
            {
              Feed.Report($"  Invalid selection, please try again.", LogLvl.Error);
            }
          }
        }
      }

      return true;
    }
  }
}
